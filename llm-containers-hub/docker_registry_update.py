import subprocess
import logging
import os
from dotenv import load_dotenv
import yaml


# Load YAML configuration
def load_config(config_file="models_config.yml"):
    with open(config_file, "r") as file:
        return yaml.safe_load(file)


load_dotenv()

logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)
logger = logging.getLogger(__name__)


def docker_login(registry_url, username, password):
    """Log in to Docker registry."""
    try:
        subprocess.run(
            ["docker", "login", registry_url, "-u", username, "-p", password],
            check=True,
            text=True,
        )
        logger.info(f"Successfully logged in to {registry_url}")
    except subprocess.CalledProcessError as e:
        logger.error(f"Failed to log in to Docker registry: {e.stderr}")


def docker_push_image(tag):
    """Push a Docker image to the registry."""
    try:
        subprocess.run(["docker", "push", tag], check=True, text=True)
        logger.info(f"Pushed image with tag '{tag}'")
    except subprocess.CalledProcessError as e:
        logger.error(f"Failed to push Docker image: {e.stderr}")


def process_images(models, registry_url):
    """Tag and push Docker images."""
    for platform, model_list in models.items():
        for model in model_list:
            image_name = f"{registry_url}/{model['tag']}"
            docker_push_image(image_name)


if __name__ == "__main__":
    config = load_config()
    registry_url = config["docker_registry"]["url"]
    username = os.getenv(config["docker_registry"]["username_env"])
    password = os.getenv(config["docker_registry"]["password_env"])

    if username is None or password is None:
        logger.error("Docker username or password environment variables not set.")
        exit(1)

    models = config["models"]

    docker_login(registry_url, username, password)
    process_images(models, registry_url)
