import os
import shutil
import subprocess
import logging
import yaml
from jinja2 import Environment, FileSystemLoader
from dotenv import load_dotenv

load_dotenv()


# Load YAML configuration
def load_config(config_file="models_config.yml"):
    with open(config_file, "r") as file:
        return yaml.safe_load(file)


logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)
logger = logging.getLogger(__name__)

# Create a Jinja2 environment and load the templates
env = Environment(loader=FileSystemLoader("templates"))


def create_file_from_template(template_name, context, output_path):
    """Render a template and write the result to a file."""
    try:
        template = env.get_template(template_name)
        content = template.render(context)
        with open(output_path, "w") as f:
            f.write(content)
        print(f"Written to {output_path}")
    except Exception as e:
        print(f"Error generating {template_name}: {e}")


def create_files(model_name, temp_dir, modality, model_platform):
    """Create files from templates in the specified directory."""

    context = {
        "model_name": model_name,
        "modality": modality if modality else "",
        "model_platform": model_platform,
    }

    for root, dirs, files in os.walk("templates"):
        for file_name in files:
            if file_name.endswith(".j2"):
                relative_path = os.path.relpath(
                    os.path.join(root, file_name), "templates"
                )
                output_path = os.path.join(
                    temp_dir, relative_path[:-3]
                )  # Remove .j2 extension
                os.makedirs(os.path.dirname(output_path), exist_ok=True)
                create_file_from_template(relative_path, context, output_path)


def build_docker_image(tag, build_path):
    """Build a Docker image with the given tag."""
    try:
        env_vars = {
            "GIT_USERNAME": os.getenv("GIT_USERNAME"),
            "GIT_PASSWORD": os.getenv("GIT_PASSWORD"),
        }
        command = ["docker", "build", "-t", tag]

        if env_vars:
            for key, value in env_vars.items():
                command.extend(["--build-arg", f"{key}={value}"])

        command.append(build_path)

        build_output = subprocess.run(
            command,
            check=True,
            capture_output=True,
            text=True,
        )
        logger.info(f"Docker build '{build_output}'")
        logger.info(f"Docker image '{tag}' built successfully.")
        return True
    except subprocess.CalledProcessError as e:
        logger.error(f"Error during Docker build: {e.stderr}")
        return False


def list_docker_images(tags):
    """List Docker images matching the provided tags."""
    if not tags:
        logger.info("No Docker images to list.")
        return

    try:
        result = subprocess.run(
            [
                "docker",
                "images",
                "--format",
                "{{.Repository}}:{{.Tag}} {{.ID}} {{.CreatedAt}} {{.Size}}",
            ],
            capture_output=True,
            text=True,
            check=True,
        )
        images = result.stdout.splitlines()

        if not images:
            logger.info("No Docker images found.")
            return

        logger.info("Listing Docker images:")
        for tag in tags:
            matched_images = [image for image in images if tag in image]
            if matched_images:
                for image in matched_images:
                    logger.info(f"Image with tag '{tag}': {image}")
            else:
                logger.info(f"No Docker images found with tag '{tag}'")
    except subprocess.CalledProcessError as e:
        logger.error(f"Error occurred while listing Docker images: {e.stderr}")


def clean_up(temp_root):
    """Remove the temporary root directory."""
    try:
        shutil.rmtree(temp_root)
        logger.info(f"Removed temporary directory {temp_root}")
    except OSError as e:
        logger.error(f"Error removing {temp_root}: {e}")


def build_and_list_containers(models_config, registry_url):
    """Build Docker images for a list of models and list them."""
    temp_root = "LLM_folder"
    os.makedirs(temp_root, exist_ok=True)
    image_tags = []

    all_models = []
    for platform, models in models_config.items():
        for model in models:
            if platform == "Individual-OpenAI":
                model["modality"] = None
            model["model_platform"] = platform
            all_models.append(model)

    for model in all_models:
        model_tag = f"{registry_url}/{model['tag']}"
        model_name = model["model_name"]
        modality = model.get("modality", "")
        model_platform = model.get("model_platform", "GenAI-LLM-Hosting")
        temp_dir = os.path.join(temp_root, model_tag.split("/")[-1])
        os.makedirs(temp_dir, exist_ok=True)

        create_files(model_name, temp_dir, modality, model_platform)

        if build_docker_image(model_tag, temp_dir):
            image_tags.append(model_tag)

        shutil.rmtree(temp_dir)

    clean_up(temp_root)
    list_docker_images(image_tags)


if __name__ == "__main__":
    config = load_config()
    registry_url = config["docker_registry"]["url"]
    models = config["models"]

    build_and_list_containers(models, registry_url)
