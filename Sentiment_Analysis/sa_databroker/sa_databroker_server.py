import grpc
from concurrent import futures
import threading

# import the generated classes :
import model_pb2
import model_pb2_grpc
from app import app_run, get_query
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
port = 8061


# create a class to define the server functions, derived from
class DatabrokerServicer(model_pb2_grpc.DatabrokerServicer):

    def __init__(self):
        super().__init__()
        self.send_data=True
        # call the function retrieving metadata on first generation of the container.

    def sadatabroker(self, request, context):
        # this simple example without streaming sends only one data record, then the pipeline must be run again
        logger.debug("Connecting to databroker")
        response = model_pb2.Text(query=get_query())
        logger.debug(response)
        if not self.send_data:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details("all data has been processed")

        self.send_data= not self.send_data
        return response


    # Getting meta data of the given dataset.
    def log_dataset_metadata(self, request, context):
        logger.debug("Model is trained on Keras imbd DB.")
        logger.debug("Number of entries: 25,000.")


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    model_pb2_grpc.add_DatabrokerServicer_to_server(DatabrokerServicer(), server)
    server.add_insecure_port('[::]:{}'.format(port))
    logger.debug("Start server")
    server.start()
    logger.debug("Start databroker UI")
    threading.Thread(target=app_run()).start()
    server.wait_for_termination()
    logger.debug("Threads ended")


if __name__ == '__main__':
    logging.basicConfig()
    serve()
