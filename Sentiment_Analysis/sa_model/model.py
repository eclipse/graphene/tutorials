from keras.models import load_model
from keras.datasets import imdb
from keras.preprocessing import sequence

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from keras.callbacks import ModelCheckpoint

import logging

class SentimentAnalysis():
    def __init__(self):
        self.model_saving_path = "model.h5" 
        self.vocab_size = 20000  # Only consider the top 20k words
        self.maxlen = 200  # Only consider the first 200 words of each movie review
        self.skip_words = 10 #skip the top N most frequently occurring words (which may not be informative).

        self.batch_size = 32
        self.epochs = 3

        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.DEBUG)
        self.logger.addHandler(logging.StreamHandler())
        

    def predict(self,input):
        self.logger.debug("Prediciting input: {}".format(input))
        model = load_model(self.model_saving_path)
        d = imdb.get_word_index()
        words = input.split()
        review = []
        for word in words:
            if word not in d:
                review.append(2)
            else:
                review.append(d[word] + 3)

        review = sequence.pad_sequences([review],
                                truncating='post', padding='pre', maxlen=self.maxlen)
        
        prediction = model.predict(review)
        
        self.logger.debug("Prediciting score: {}".format(prediction[0][0]))
        return prediction[0][0]


    def calculate_metrics(self):
        model = load_model(self.model_saving_path)
        _, (x_test, y_test) = imdb.load_data(num_words=self.vocab_size)
        x_test = sequence.pad_sequences(x_test, maxlen=self.maxlen)
        score = model.evaluate(x_test, y_test, verbose=0)
        score_dic = {
            "loss": score[0],
            "accuracy": score[1]
        }
        return score_dic
    

    def _token_and_position_embedding(self, x, maxlen, vocab_size, embed_dim):
        # Create token embedding layer
        token_emb = layers.Embedding(input_dim=vocab_size, output_dim=embed_dim)

        # Create position embedding layer
        pos_emb = layers.Embedding(input_dim=maxlen, output_dim=embed_dim)

        # Get the sequence length
        seq_len = tf.shape(x)[-1]

        # Generate positional indices
        positions = tf.range(start=0, limit=seq_len, delta=1)

        # Get position embeddings
        positions_embeddings = pos_emb(positions)

        # Get token embeddings
        token_embeddings = token_emb(x)

        # Combine token and position embeddings
        embeddings = token_embeddings + positions_embeddings

        return embeddings
    

    def _transformer_block(self, inputs, embed_dim, num_heads, ff_dim, rate=0.1, training=True):
        # Create multi-head attention layer
        attn_layer = layers.MultiHeadAttention(num_heads=num_heads, key_dim=embed_dim)

        # Create feed-forward network
        ffn = keras.Sequential(
            [layers.Dense(ff_dim, activation="relu"), layers.Dense(embed_dim)]
        )

        # Create layer normalization layers
        layernorm1 = layers.LayerNormalization(epsilon=1e-6)
        layernorm2 = layers.LayerNormalization(epsilon=1e-6)

        # Create dropout layers
        dropout1 = layers.Dropout(rate)
        dropout2 = layers.Dropout(rate)

        # Attention block
        attn_output = attn_layer(inputs, inputs)
        attn_output = dropout1(attn_output, training=training)
        out1 = layernorm1(inputs + attn_output)

        # Feed-forward block
        ffn_output = ffn(out1)
        ffn_output = dropout2(ffn_output, training=training)
        return layernorm2(out1 + ffn_output)


    def train(self):
        (x_train, y_train), (x_val, y_val) = keras.datasets.imdb.load_data(num_words=self.vocab_size, skip_top=self.skip_words)
        self.logger.debug("Training sequencest: {}".format(len(x_train)))
        self.logger.debug("Validation sequences: {}".format(len(x_val)))
        x_train = sequence.pad_sequences(x_train, maxlen=self.maxlen)
        x_val = sequence.pad_sequences(x_val, maxlen=self.maxlen)

        # nn from https://keras.io/examples/nlp/text_classification_with_transformer/
        embed_dim = 32  # Embedding size for each token
        num_heads = 2  # Number of attention heads
        ff_dim = 32  # Hidden layer size in feed forward network inside transformer

        inputs = layers.Input(shape=(self.maxlen,))
        x = self._token_and_position_embedding(inputs, self.maxlen, self.vocab_size, embed_dim)
        x = self._transformer_block(x, embed_dim, num_heads, ff_dim)
        x = layers.GlobalAveragePooling1D()(x)
        x = layers.Dropout(0.1)(x)
        x = layers.Dense(20, activation="relu")(x)
        x = layers.Dropout(0.1)(x)
        outputs = layers.Dense(1, activation='sigmoid')(x)

        model = keras.Model(inputs=inputs, outputs=outputs)

        model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['acc'])

        checkpoint = ModelCheckpoint(filepath=self.model_saving_path, monitor='val_acc', save_best_only=True, mode='max')
        history = model.fit(x_train, y_train, batch_size=self.batch_size, epochs=self.epochs, validation_data=(x_val, y_val),callbacks=[checkpoint])

        self.logger.debug("Saved model to disk")
        self.logger.debug("Accuracy in training: {}".format(history.history['acc']))
        self.logger.debug("loss in training: {}".format(history.history['loss']))
