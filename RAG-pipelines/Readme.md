# RAG
Retrieval-augmented generation (RAG) is a technique for enhancing the accuracy and reliability of generative AI models with facts fetched from external sources. It is crucial for ensuring the quality, accuracy, and relevance of the generated output.


<img src="RAG-workflow.png" width="500" />

Retrieval-Augmented Generation (RAG) combines two main components:
    
1. Retrieval Component: This part involves searching a large corpus of documents or knowledge base to find relevant information related to a given query.

2. Generation Component: This part involves generating a response or text based on the information retrieved by the first component.

Currently in our DEV AI-Builder we have two published pipelines:

1. RAG-Pipeline-TN 
2. RAG-Pipeline-SN ( Script will be updated soon )


More RAG-pipelines will be added to this tutorial folder. This tutorial is currently in the developmental stages and may undergo frequent changes. Contributions, suggestions, and feedback are welcome to help improve this project.

