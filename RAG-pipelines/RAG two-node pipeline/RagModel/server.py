from concurrent import futures
import grpc
import openai
import ragmodel_pb2
import ragmodel_pb2_grpc
import os
from app import app_run
import logging
import threading

import requests


from langchain_core.documents import Document
from langchain_community.vectorstores import FAISS
from langchain_community.embeddings import HuggingFaceEmbeddings

from langchain.prompts import PromptTemplate
from transformers import pipeline
from langchain_core.output_parsers import StrOutputParser
from langchain_core.runnables import RunnablePassthrough
from langchain_openai import ChatOpenAI

from langchain_community.document_loaders import PyPDFLoader
from langchain_text_splitters import RecursiveCharacterTextSplitter


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

openai.organization  = os.getenv("OPENAI_ORGANIZATION")
openai.api_key  = os.getenv("OPENAI_API_KEY")

class Rag(ragmodel_pb2_grpc.RagServicer):

    def __init__(self) -> None:
        super().__init__()


    def get_pdf_filename(self, url, response):
    # Try to get the filename from the URL
        filename = os.path.basename(url)
        # If the filename from URL does not have a proper extension, try content-disposition header
        if not filename.lower().endswith('.pdf'):
            content_disposition = response.headers.get('content-disposition')
            if content_disposition:
                filename = content_disposition.split('filename=')[-1].strip('"')
        return filename
       

    def get_rag_model(self,request,context):
        
        ''' Grounding LLM by creating embeddings of the pdf document using pdf document available in the URL and access the embeddings for relevant information using retreiver. 
        Finally passing relevant info + user's query into a LLM'''

        logger.info('start of pdf processing')
    
        user_query = request.user_query
        pdf_url = request.usecase_data        

        # try:

        response = requests.get(pdf_url)
        if response.status_code == 200:
            # Check the content type to ensure it's a PDF
            if 'application/pdf' in response.headers.get('Content-Type', ''):
                local_pdf_path = self.get_pdf_filename(pdf_url, response)
                with open(local_pdf_path, 'wb') as f:
                    f.write(response.content)
                print(f'Downloaded {pdf_url} to {local_pdf_path}')
            else:
                print("The URL does not point to a PDF file.")
        else:
            print(f'Failed to download PDF: {response.status_code}')


        loader = PyPDFLoader(local_pdf_path)
        raw_document = loader.load()
        data_splitter = RecursiveCharacterTextSplitter(chunk_size=1000, chunk_overlap=10)
        data = data_splitter.split_documents(raw_document)

        pdf_db = FAISS.from_documents(data, HuggingFaceEmbeddings(model_name="BAAI/bge-base-en-v1.5"))
        retriever = pdf_db.as_retriever(search_type="similarity", search_kwargs={"k": 4})

        
        # except Exception as e:
        #     """ Documents extracted from already existing vector database """

        #     print('The following exception has occurred {}'.format(e))

            
        """ Generator """
        logger.info("Rag generator ...")

        prompt_template = """
        <|start_header_id|>user<|end_header_id|>
        You are an assistant for answering questions about EU-AI act.
        You are given the extracted parts of a long document and a question. Provide a conversational answer.
        If you don't know the answer, just say "I do not know." Don't make up an answer.
        Question: {question}
        Context: {context}<|eot_id|><|start_header_id|>assistant<|end_header_id|>
        """

        prompt = PromptTemplate(
            input_variables=["context", "question"],
            template=prompt_template,
        )

        def format_docs(docs):
            return "\n\n".join(doc.page_content for doc in docs)

        llm_openai = ChatOpenAI(model="gpt-4")

        rag_chain_openai = (
            {"context": retriever | format_docs, "question": RunnablePassthrough()}
            | prompt
            | llm_openai
            | StrOutputParser()
        )

        
        grounded_output=rag_chain_openai.invoke(user_query)

        response = ragmodel_pb2.GroundedOutput(grounded_output=str(grounded_output))

        with open("results.txt", mode="a+") as f:
            # for e0, e1, e2, e3, e4, e5 in result:
            f.write("|" + str(user_query) + "|" + str(grounded_output) + "\n")
            f.close()

        logger.info(f'response: {response}')

        return response


def serve(port):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    ragmodel_pb2_grpc.add_RagServicer_to_server(Rag(), server)
    server.add_insecure_port("[::]:{}".format(port))
    print("Starting server. Listening on port : " + str(port))
    server.start()
    threading.Thread(target=app_run()).start()
    server.wait_for_termination()

if __name__ == "__main__":
    logging.basicConfig()
    open('results.txt', 'w').close()
    port = 8061
    serve(port)
   

