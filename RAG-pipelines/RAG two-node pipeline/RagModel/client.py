import grpc
from timeit import default_timer as timer
import logging

# import the generated classes

import ragmodel_pb2_grpc
import ragmodel_pb2

#port = 8061
port = 20021



def run():
    print("Calling GLLM_Stub..")
    with grpc.insecure_channel('localhost:{}'.format(port)) as channel:
        stub = ragmodel_pb2_grpc.RagStub(channel)
        
        request = ragmodel_pb2.UserInputs(
                    organization_id="org-XSbDbl5S9lfwrFTncFOBVOMx",
                    api_key="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",
                    user_query="What does the notion of ‘emotion recognition system’ mean? ",
                    #user_query = "what is a mutator ? ",
                    usecase_data = "https://www.europarl.europa.eu/doceo/document/TA-9-2024-0138-FNL-COR01_EN.pdf"
                    #usecase_data = "https://arxiv.org/pdf/2403.12171"
        )
        keys_response = stub.get_keys_data(request)
        response_grounding = stub.get_rag_model(request)

    print("Greeter client received: ")
    print(keys_response)
    print(response_grounding)


if __name__ == '__main__':
    logging.basicConfig()
    run()

