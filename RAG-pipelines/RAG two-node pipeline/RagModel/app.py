from flask import Flask, render_template, request, flash
import grpc
import ragmodel_pb2
import ragmodel_pb2_grpc
import os
import logging


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
app = Flask(__name__)


@app.route("/", methods=['GET', 'POST'])
def index():
    results = []
    with open("results.txt", mode="r") as f:
        content = ""
        for line in f.readlines():
            content += line.strip('')  # Concatenate lines with hyphen
            

        # Split the content into two items based on '|'
        results = content.split('|')
        results = [item.strip('') for item in results]  # Remove trailing hyphens
        
    try:
        LLM_prompt=results[-2]
        grounded_output=results[-1]
    except:
        LLM_prompt=results[:-1]
        grounded_output=results[-1]


    return render_template('index.html', LLM_prompt=LLM_prompt, grounded_output=grounded_output)


def app_run():
    app.secret_key = 'rag' 
    app.run(host="0.0.0.0", port=8062, debug = True)


