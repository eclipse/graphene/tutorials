# RAG
Retrieval-augmented generation (RAG) is a technique for enhancing the accuracy and reliability of generative AI models with facts fetched from external sources. It is crucial for ensuring the quality, accuracy, and relevance of the generated output.

PLease refer to the issue for more details on the usecase: https://gitlab.eclipse.org/eclipse/graphene/eclipse-graphene/-/issues/17

![RAG Two-Node Pipeline](two-node-pipeline.png)

# Docker Containers
Here we have 3 docker containers:
 1. RAG Databroker
    The databroker is responsible for acquiring data ( user's query and pdf url ) from the user and pass it into the docker container - rag model..  
 2. RAG Model
    The model downloads the pdf using the pdf url and then process the pdf to extract page content and metadata. This is then stored in a vector store as embeddings. The retriever retrieves relevant information from the embedding store and pass it into the rag pipeline. 
    
Finally the rag pipeline with the LLM and retrieved content gives an output which is expected to be accurate, reliable and relevant.


