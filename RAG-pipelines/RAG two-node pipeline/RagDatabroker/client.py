import grpc
from timeit import default_timer as timer
import logging

# import the generated classes

import rag_databroker_pb2_grpc
import rag_databroker_pb2

port = 20021


def run():
    print("Calling Rag_Stub..")
  
    with grpc.insecure_channel('localhost:{}'.format(port)) as channel:
        stub = rag_databroker_pb2_grpc.DatabrokerStub(channel)
        ui_request = rag_databroker_pb2.Empty()
        response = stub.ragdatabroker(ui_request)
    
    print("Greeter client received: ")
    print(response)


if __name__ == '__main__':
    logging.basicConfig()
    run()
