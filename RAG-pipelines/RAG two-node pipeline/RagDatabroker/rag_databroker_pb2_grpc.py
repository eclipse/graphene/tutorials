# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
"""Client and server classes corresponding to protobuf-defined services."""
import grpc

import rag_databroker_pb2 as rag__databroker__pb2


class DatabrokerStub(object):
    """Missing associated documentation comment in .proto file."""

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.ragdatabroker = channel.unary_unary(
                '/Databroker/ragdatabroker',
                request_serializer=rag__databroker__pb2.Empty.SerializeToString,
                response_deserializer=rag__databroker__pb2.UserInputs.FromString,
                )


class DatabrokerServicer(object):
    """Missing associated documentation comment in .proto file."""

    def ragdatabroker(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_DatabrokerServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'ragdatabroker': grpc.unary_unary_rpc_method_handler(
                    servicer.ragdatabroker,
                    request_deserializer=rag__databroker__pb2.Empty.FromString,
                    response_serializer=rag__databroker__pb2.UserInputs.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'Databroker', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))


 # This class is part of an EXPERIMENTAL API.
class Databroker(object):
    """Missing associated documentation comment in .proto file."""

    @staticmethod
    def ragdatabroker(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/Databroker/ragdatabroker',
            rag__databroker__pb2.Empty.SerializeToString,
            rag__databroker__pb2.UserInputs.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)
