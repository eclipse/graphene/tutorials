import grpc
import rag_pb2
import rag_pb2_grpc


def run():
    # Dummy implementation
    with grpc.insecure_channel("localhost:8061") as channel:
        stub = rag_pb2_grpc.RAGServiceStub(channel)
        responses = stub.instruct_llm_query(rag_pb2.Empty())
        for response in responses:
            print(f"Received LLM Query: {response.input.user}")
            print(f"Question: {response.qa.question}")


if __name__ == "__main__":
    run()
