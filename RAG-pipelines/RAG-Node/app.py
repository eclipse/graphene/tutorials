from flask import Flask, request, jsonify, render_template, send_file
import os
from rag_utils import process_pdf, start_df, update_df, faiss_db
import pandas as pd
from sklearn.cluster import KMeans
import seaborn as sns
import matplotlib.pyplot as plt
import plotly.express as px
from io import BytesIO
from scipy.spatial.distance import cdist, euclidean
import json
from datetime import datetime
import logging
from queue import Queue





app = Flask(__name__)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

user_question = []
user_answer = []
readme_ratings = {}
ratings_list = []
metrics_history = []
chat_history = []
faithfulness = None
relevancy= None
local_directory = os.getenv("SHARED_FOLDER_PATH")

# Queues to hold metrics and results for SSE
metrics_queue = Queue()
results_queue = Queue()
question_plot=""
answer_plot=""

def save_uploaded_file(file):
    file_path = os.path.join("/tmp", file.filename)
    with open(file_path, "wb") as f:
        f.write(file.read())
    return file_path


@app.route("/")
def index():

    return render_template("index.html")

# Endpoint to serve the latest results
@app.route('/results', methods=['GET'])
def get_results():
    global question_plot
    global answer_plot
    # Read the results from the JSON file and return them
    while not results_queue.empty():
        data = results_queue.get()
        question_plot = data["Question"]
        answer_plot = data["Answer"]
        chat_history.append(data)   
    
    return jsonify(chat_history)

# Endpoint to serve the latest results
@app.route('/metrics', methods=['GET'])
def get_metrics():
    global faithfulness
    global relevancy
    # Read the results from the JSON file and return them
    while not metrics_queue.empty():  
        data = metrics_queue.get()
        faithfulness = data["Faithfulness"]
        relevancy = data["AnswerRelevancy"]   
        metrics_history.append(data)           
    
    return jsonify(metrics_history)


@app.route("/handle_request", methods=["POST"])
def handle_request():
    action = request.args.get("action")

    # Upload the PDF
    if action == "upload":
        file = request.files.get("file")
        if not file:
            return jsonify({"response": "No file provided"}), 400

        chunk_size = int(request.form.get("chunk_size", 500))
        chunk_overlap = int(request.form.get("chunk_overlap", 50))

        file_path = save_uploaded_file(file)
        return jsonify(
            {
                "response": f"File uploaded successfully as {file_path}. Ready to process."
            }
        )

    # Other PDF parameters such as chunk size and chunk overlap and create embeddings
    elif action == "process":
        file_name = request.form.get("file_name")
        if not file_name:
            return jsonify({"response": "No file specified for processing"}), 400

        chunk_size = int(request.form.get("chunk_size", 500))
        chunk_overlap = int(request.form.get("chunk_overlap", 50))

        file_path = os.path.join("/tmp", file_name)
        if not os.path.exists(file_path):
            return jsonify({"response": f"File {file_name} does not exist"}), 400

        response = process_pdf(
            file_path, chunk_size, chunk_overlap
        )  # Create embeddings here
        return jsonify({"response": response})

    # If user would like to upload existing embeddings, it can be done here
    elif action == "upload_index":
        file = request.files.get("file")
        if not file:
            return jsonify({"response": "No file provided"}), 400

        response = "Should be implemented"
        return jsonify({"response": response})
    
    #History of Q&A
    elif action == "chat":

        request_data = request.get_json()
        question = request_data.get("question", "")
        
        if not question:
            return jsonify({"response": "No question provided"}), 400
        
        # Append the new question
        user_question.clear()
        user_question.append(question)
        
        return jsonify({"response": "Question recieved"})
        

def get_user_question():
    # Send the user question to the server for answer retrieval
    return user_question


# Plotting begins here.............

@app.route("/cluster_plot")
def cluster_plot():
    n_clusters = 10
    df = start_df()
    df = update_df(df)
    X = df[["UMAP_x", "UMAP_y"]]
    kmeans = KMeans(n_clusters=n_clusters, random_state=42)
    df["cluster"] = kmeans.fit_predict(X)

    # Create an interactive scatter plot with Plotly
    fig = px.scatter(
        df,
        x="UMAP_x",
        y="UMAP_y",
        color="cluster",
        title="Clusters based on UMAP embeddings",
        hover_data=["cluster"],
        color_continuous_scale=px.colors.sequential.Viridis,
        size_max=10
    )

    fig.update_traces(marker=dict(size=10, opacity=0.8))
    fig.update_layout(
        legend_title_text='Cluster',
        xaxis_title='UMAP_x',
        yaxis_title='UMAP_y',
        template='plotly_white'
    )

    # Return the plot data as JSON
    return fig.to_json()

#Interactive 2D projection of embedding chunks is visualized using Plotly, highlighting chunks most relevant to the user query
@app.route("/embedding_plot")
def plot_embeddings():
    global question_plot
    global answer_plot
    question = str(question_plot)
    answer = str(answer_plot)
    df = start_df()
    saved_db, retriever, embeddings = faiss_db()

    print(f"Question and answer from check file {question},{answer}")

    # Embed the question and answer directly
    question_embedding = embeddings.embed_query(question)
    answer_embedding = embeddings.embed_query(answer)   


    # Append question and answer to df
    df = pd.concat([
        df,
        pd.DataFrame({
            "id": ["question", "answer"],
            "metadata": ["Question", "Answer"],
            "document": [question, answer],
            "embedding": [question_embedding, answer_embedding]
        })
    ], ignore_index=True)

    df = update_df(df)

    question_coords = df[df["id"] == "question"][["UMAP_x", "UMAP_y"]]
    answer_coords = df[df["id"] == "answer"][["UMAP_x", "UMAP_y"]]

    if not question_coords.empty and not answer_coords.empty:
        question_point = question_coords[["UMAP_x", "UMAP_y"]].values[0]
        answer_point = answer_coords[["UMAP_x", "UMAP_y"]].values[0]
    else:
        return jsonify({"error": "Could not find question or answer in the data."}), 400

    print(f"question and answer points: {question_point}, {answer_point}")

    # Calculate distances
    df["distance_to_question"] = cdist(df[["UMAP_x", "UMAP_y"]], [question_point])
    df["distance_to_answer"] = cdist(df[["UMAP_x", "UMAP_y"]], [answer_point])

    max_distance = 0.5
    df["is_relevant"] = (df["distance_to_question"] < max_distance) | (df["distance_to_answer"] < max_distance)
    
    # Add hover text for relevant points
    df["hover_text"] = df["document"].fillna("No content available").apply(lambda x: "<br>".join(x.split("\n")))

    # Create the Plotly figure with all points
    fig = px.scatter(
        df,
        x="UMAP_x",
        y="UMAP_y",
        color=df["is_relevant"].map({True: "Relevant", False: "All Points"}),
        hover_name="id",
        hover_data={"hover_text": True},
        title="UMAP Projection of Documents",
        labels={"UMAP_x": "UMAP Dimension 1", "UMAP_y": "UMAP Dimension 2"},
        color_discrete_map={"Relevant": "orange", "All Points": "grey"},     
    )

    # Enable legend and customize it
    fig.update_traces(showlegend=True)

    # Add Question and Answer markers separately with custom symbols
    fig.add_scatter(
        x=[question_point[0]],
        y=[question_point[1]],
        mode="markers",
        marker=dict(color="red", symbol="star", size=12),
        name="Question",
        showlegend=True,
    )

    fig.add_scatter(
        x=[answer_point[0]],
        y=[answer_point[1]],
        mode="markers",
        marker=dict(color="blue", symbol="cross", size=12),
        name="Answer",
        showlegend=True,
    )

    # Calculate and annotate distance
    distance = euclidean(question_point, answer_point)
    mid_x, mid_y = (question_point[0] + answer_point[0]) / 2, (question_point[1] + answer_point[1]) / 2
    fig.add_annotation(
        x=mid_x,
        y=mid_y,
        text=f"Distance: {distance:.2f}",
        showarrow=True,
        arrowhead=2,
        ax=0,
        ay=-40,
        font=dict(size=12, color="black"),
        bgcolor="white",
        borderpad=4,
    )


    # Update legend layout
    fig.update_layout(   
       legend=dict(
        title="Legend",
        orientation="h",
        yanchor="bottom",
        y=0.95,  # Position legend above the plot
        xanchor="center",
        x=0.5
    ),
        autosize=True,     
        width=600,
        height=600,
    )

    return fig.to_json()


@app.route('/rate_readme', methods=['POST'])
def rate_readme():
    global faithfulness
    global relevancy
    faithfulness_score = float(faithfulness)
    relevancy_score = float(relevancy)
    
    try:
        data = request.json
        rating = data['rating']
        feedback = data.get('feedback', '')  # Get the feedback string, default to empty string if not provided
        readme_ratings.setdefault(local_directory, []).append({'rating': rating, 'feedback': feedback, 'faithfulness' : faithfulness_score, 'answer_Relevancy' : relevancy_score})
        logger.info(readme_ratings)
        timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        try:
            ratings_filename = os.path.join(local_directory, "ratings.json")
            rating_dict={"Rating": rating, "Feedback": feedback, "Faithfulness": faithfulness_score, "Answer_Relevancy": relevancy_score, "Timestamp": timestamp}
            ratings_list.append(rating_dict)
            
            with open(ratings_filename, 'w') as file:                
                json.dump(ratings_list, file, indent=4)
                logger.info(f"Rating: {rating}, Feedback: {feedback}, Faithfulness: {faithfulness_score}, Answer_Relevancy: {relevancy_score}, Timestamp: {timestamp}\n")
                
        except Exception as e:
            logger.exception("An error occurred")
        return jsonify({'success': True})
    except Exception as e:
        logger.info("Exception:", e)
        return jsonify({'success': False, 'error': str(e)})

def app_run():
    app.run(host="0.0.0.0", port=8062, debug=False)