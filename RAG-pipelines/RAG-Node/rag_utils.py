import os
import umap
import numpy as np

from langchain.vectorstores import FAISS
from langchain.embeddings.huggingface import HuggingFaceEmbeddings
from langchain.document_loaders import PyPDFLoader
from langchain.text_splitter import RecursiveCharacterTextSplitter
import pandas as pd

# FAISS folder path
local_directory = os.getenv("SHARED_FOLDER_PATH")
faiss_folder = os.path.join(local_directory, "faiss_index")


def process_pdf(file_name, chunk_size=500, chunk_overlap=50):
    # Load and process the PDF document
    loader = PyPDFLoader(file_name)
    raw_document = loader.load()

    # Split the document into chunks
    splitter = RecursiveCharacterTextSplitter(
        chunk_size=chunk_size, chunk_overlap=chunk_overlap
    )
    chunks = splitter.split_documents(raw_document)

    # Create and save FAISS index
    embedding_model = HuggingFaceEmbeddings(model_name="BAAI/bge-base-en-v1.5")
    faiss_db = FAISS.from_documents(chunks, embedding=embedding_model)
    faiss_db.save_local(faiss_folder)
    return "FAISS index processed and saved successfully"


def faiss_db():
    # Retriever contents can be obtained here
    embeddings = HuggingFaceEmbeddings(model_name="BAAI/bge-base-en-v1.5")
    try:
        saved_db = FAISS.load_local(
            faiss_folder, embeddings, allow_dangerous_deserialization=True
        )
        retriever = saved_db.as_retriever(
            search_type="similarity", search_kwargs={"k": 4}
        )
        print(retriever)
        print(type(retriever))
        return saved_db, retriever, embeddings
    except Exception as e:
        print(f"Error loading FAISS database: {e}")
        return None


# Start creating the dataframe for visualization
def start_df():
    saved_db, retriever, embeddings = faiss_db()
    vs = saved_db.__dict__.get("docstore")
    index_list = saved_db.__dict__.get("index_to_docstore_id").values()
    doc_cnt = saved_db.index.ntotal

    embeddings_vec = saved_db.index.reconstruct_n()
    doc_list = list()
    for i, docid in enumerate(index_list):
        a_doc = vs.search(docid)
        doc_list.append(
            [docid, a_doc.metadata.get("source"), a_doc.page_content, embeddings_vec[i]]
        )
    df = pd.DataFrame(doc_list, columns=["id", "metadata", "document", "embedding"])
    return df


def update_df(df):
    embeddings = np.vstack(df["embedding"].values)
    umap_model = umap.UMAP(n_neighbors=15, min_dist=0.1, metric="cosine")
    umap_embeddings = umap_model.fit_transform(embeddings)

    df["UMAP_x"] = umap_embeddings[:, 0]
    df["UMAP_y"] = umap_embeddings[:, 1]
    return df
