### Current pipeline architecture:

The refactored RAG pipeline (single-node) is designed to handle the connection to the unified LLM interface.

![alt text](image.png)

Key features

- Bi-directional streaming is enabled
- Loading and ingestion - dual possibility (PDF Upload and FAISS embeddings)
- Save workspace for future use


This pipeline is currently in developmental stages and will undergo frequent changes. Contributions, suggestions, and feedback are welcome to help improve this project.

Please refer to the following ticket to better understand the pipeline structure, eclipse/graphene/tutorials#45.
