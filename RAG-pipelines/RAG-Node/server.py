import grpc
from concurrent import futures
import threading
import time

import rag_pb2
import rag_pb2_grpc
from app import app_run, get_user_question
import json
import os
import uuid
import ast
from app import metrics_queue, results_queue  # Import the queues from app.py

from datasets import Dataset 
from ragas.metrics import faithfulness, answer_relevancy
from ragas import evaluate
from threading import Thread

os.environ["OPENAI_API_KEY"]  = os.getenv("OPENAI_API_KEY")
os.environ['OPENAI_ORGANIZATION']   =os.getenv("OPENAI_ORGANIZATION")
local_directory = os.getenv("SHARED_FOLDER_PATH")



class RAGService(rag_pb2_grpc.RAGServiceServicer):
    def __init__(self):
        super().__init__()
        self.responses = {}
        self.current_question = None  # Store the last processed question

    def instruct_llm_query(self, request, context):
        while True:
            params = get_user_question()
            if not params:
                time.sleep(0.5)
                continue

            self.new_question = params[0]
            if self.new_question == self.current_question:
                time.sleep(0.5)
                continue

            self.current_question = self.new_question

            question_id = str(uuid.uuid4())
            input_text = f"You asked: {self.new_question}"

            Pinput = rag_pb2.PromptInput(
                system="System instruction here",
                user=input_text,
                context="",
                prompt=input_text,
            )

            QAinput = rag_pb2.UserQuestion(question=self.new_question)
            llm_query = rag_pb2.LLMQuery(
                id=rag_pb2.ConvoID(q_id=question_id), input=Pinput, qa=QAinput,
                app_type=rag_pb2.AppType.RAG
            )
            print(llm_query)
            yield llm_query

            while question_id not in self.responses:
                time.sleep(0.1)

    def retrieve_llm_response(self, request_iterator, context):
        
        for answer in request_iterator:
            question_id = answer.id.q_id
            print(f"Received LLMAnswer for ID {question_id}")

            # Extract fields from the `fields` map
            rag_ans, relevant_context = self.extract_rag_fields(answer.fields)
            results_queue.put({"Question": self.new_question, "Answer": rag_ans})

            faithfulness, relevancy = self.calculate_metrics(relevant_context, rag_ans) 
            metrics_queue.put({"Faithfulness": str(round(faithfulness,3)), "AnswerRelevancy": str(round(relevancy,3))})
            print(f'faithfulness, relevancy: {faithfulness}, {relevancy}')
            

            # Log extracted fields
            print(f"Extracted rag_ans: {rag_ans}")
            print(f"Extracted relevant_context: {relevant_context}")
            

            # Store the response in the class's responses dictionary
            self.responses[question_id] = {
                "rag_ans": rag_ans,
                "relevant_context": relevant_context,
            }


        return rag_pb2.Status(message="All answers received successfully.")
    
    def extract_rag_fields(self, fields_map):
        rag_ans = ""
        relevant_context = ""

        if "data" in fields_map:
            try:
                # Parse the data field as JSON
                data = ast.literal_eval(fields_map["data"])
                rag_ans = data.get("rag_ans", "")
                relevant_context = data.get("relevant_context", "")
            except json.JSONDecodeError as e:
                print("Error parsing data field:", e)
        else:
            print("Data field not found in fields.")

        return rag_ans, relevant_context
    
    def calculate_metrics(self, relevant_info, rag_output):
  
        data_samples = {
            'question': [str(self.new_question)],
            'answer': [str(rag_output)],
            'contexts' : [[str(relevant_info)]]
        }

        dataset = Dataset.from_dict(data_samples)
        faithfulness_score = evaluate(dataset,metrics=[faithfulness])
        answer_relevancy_score = evaluate(dataset,metrics=[answer_relevancy])
        
        return faithfulness_score["faithfulness"], answer_relevancy_score["answer_relevancy"]

def serve(port):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    rag_pb2_grpc.add_RAGServiceServicer_to_server(RAGService(), server)
    server.add_insecure_port(f"[::]:{port}")
    print(f"Starting server. Listening on port: {port}")
    server.start()
    threading.Thread(target=app_run).start()
    server.wait_for_termination()


if __name__ == "__main__":
    port = 8061
    serve(port)
