grpcio==1.38.0
grpcio-tools==1.38.0
grpc-interceptor
protobuf==3.16.0
multithreading

Bootstrap-Flask
Flask
Flask-WTF
flask_session
WTForms
requests
python-dotenv

langchain
langchain_community 

umap-learn
pandas
matplotlib
sentence-transformers
faiss-cpu

pypdf
numpy
seaborn
umap-learn
scikit-learn
plotly
kaleido
ragas==0.1.7
scipy
