import grpc
from concurrent import futures
import threading
import os
import json
from datetime import datetime

import user_ratings_pb2
import user_ratings_pb2_grpc

from app import app_run
from utils_calculate_metrics import process_ratings_file
import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

local_directory = os.getenv("SHARED_FOLDER_PATH")
print('The SHARED_FOLDER_PATH is {}'.format(local_directory))
file_path = os.path.join(local_directory, 'ratings.json')
#file_path = "ratings.json"


class RatingsService(user_ratings_pb2_grpc.RatingsServiceServicer):
    def __init__(self):
        self.has_metrics = True # Flag to indicate the presence of metrics in this node and print a message accordingly.
        if self.has_metrics:
            print('MetricsAvailable')

    def GetRatings(self, request, context):   # Get the ratings from the json file stored in shared folder
        llm_metrics = {}
        timestamp =""
        feedback=""
        stars = 0
        filename=""
        try:
            with open(file_path, 'r') as file:  
                ratings_list = json.load(file)

                for ratings_dict in ratings_list:

                    for key,value in ratings_dict.items():   
                        if key.lower() == "filename":
                            filename = value
                        elif key.lower() =="timestamp":
                            timestamp=value
                        elif key.lower() == "feedback" :
                            feedback=value
                        elif "star" in key.lower() or "rating" in key.lower():
                            stars = value
                        elif isinstance(value,(float,int)):
                            llm_metrics[key] = value

                    print("llm_metrics: ",llm_metrics)

                    rating = user_ratings_pb2.Rating(filename=filename, stars=stars, feedback=feedback, timestamp=timestamp, llm_metrics=llm_metrics)
                    yield rating
        except:
            rating = user_ratings_pb2.Rating(filename='', stars=0.0, feedback='', timestamp='', llm_metrics={})
            yield rating
           



    def CalculateMetrics(self, request, context):   # Perform calculation for average metrics

        if file_path:
            score_averages, average_rating, average_sentiment_score = process_ratings_file(file_path)
            
            calculated_metrics = user_ratings_pb2.CalculatedMetrics()
            calculated_metrics.type = "LLM Metrics"
            calculated_metrics.status_text = "success"
            calculated_metrics.more_is_better.overall_avg_feedback_sentiment_score = average_sentiment_score
            calculated_metrics.more_is_better.overall_avg_star_rating = average_rating
            for key,value in score_averages.items():
                calculated_metrics.more_is_better.overall_avg_metrics_rating[key]=value   

            self.get_metrics_metadata(calculated_metrics, None) 
            return calculated_metrics
        else:
            return ()

    def get_metrics_metadata(self, request, context):  # Get excution metadata
        
        final_metrics_dict = {'metrics': {}}
        now = datetime.now()
        current_time = now.strftime("%Y-%m-%d %H:%M:%S")
        final_metrics_dict['metrics']['date_time'] = current_time

        for field, value in request.ListFields():
            field_name = field.name
            if field.message_type is not None:
                nested_dict = {}
                for nested_field, nested_value in value.ListFields():
                    nested_dict[nested_field.name] = nested_value
                final_metrics_dict['metrics'][field_name] = nested_dict
            else:
                final_metrics_dict['metrics'][field_name] = value

        print(final_metrics_dict)


def serve(port):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    user_ratings_pb2_grpc.add_RatingsServiceServicer_to_server(RatingsService(), server)
    server.add_insecure_port("[::]:{}".format(port))
    print("Starting server. Listening on port : " + str(port))
    server.start()
    threading.Thread(target=app_run()).start()
    server.wait_for_termination()

if __name__ == "__main__":
    port = 8061
    serve(port) 