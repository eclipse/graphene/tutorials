import grpc
import user_ratings_pb2
import user_ratings_pb2_grpc

def run():
    channel = grpc.insecure_channel('localhost:8061')
    stub = user_ratings_pb2_grpc.RatingsServiceStub(channel)

    print("-------------- GetRatings --------------")
    for rating in stub.GetRatings(user_ratings_pb2.Empty()):
        print(rating)


    print("-------------- CalculateMetrics --------------")
    response = stub.CalculateMetrics(user_ratings_pb2.Empty())
    print("Type:", response.type)
    print("Status Text:", response.status_text)
    print("Average Star Rating:", response.more_is_better.overall_avg_star_rating)
    print("Average Feedback Sentiment Score:", response.more_is_better.overall_avg_feedback_sentiment_score)
    print("Average Feedback Metrics Score:", response.more_is_better.overall_avg_metrics_rating)
    print("----------------------------------------------")

if __name__ == "__main__":
    run()
