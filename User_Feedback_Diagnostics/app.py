from flask import Flask, render_template
import grpc
import plotly.graph_objects as go
from utils_calculate_metrics import calculate_sentiment_score
from user_ratings_pb2 import Empty
from user_ratings_pb2_grpc import RatingsServiceStub
import numpy as np

app = Flask(__name__)
app.secret_key = "user_ratings"


def get_ratings():
    try:    
        with grpc.insecure_channel('localhost:8061') as channel:        
            stub = RatingsServiceStub(channel)        
            for rating in stub.GetRatings(Empty()):
                yield rating.filename, rating.stars, rating.feedback, rating.timestamp, rating.llm_metrics
    except:
        yield ('', 0.0, '', '', {})
        

def get_calculated_metrics():
    try:
        with grpc.insecure_channel('localhost:8061') as channel:
            stub = RatingsServiceStub(channel)
            response = stub.CalculateMetrics(Empty())
            return response
    except:
        return ()

@app.route('/')
def index():
    calculated_metrics = get_calculated_metrics()
    ratings = list(get_ratings()) # [(5,"gllm",timstamp,"good",{faithfulnes:0.98, relevancy:1.0),()..]
    average_ratings = {}
    filenames, stars, feedbacks, _, llm_metrics = zip(*ratings) #unpack into separate variables eg: stars would be (5, 4)
    ratings_by_filename = {}
    sentiment_scores = []
    llm_metric_ratings ={}

    for filename, star, feedback, llm_metric in zip(filenames, stars, feedbacks, llm_metrics): #iterate through each tuple
        if filename not in ratings_by_filename:
            ratings_by_filename[filename] = {} 
            ratings_by_filename[filename]["star"] = []
            ratings_by_filename[filename]["llm_metric"] = []
           
        ratings_by_filename[filename]["star"].append(star)
        ratings_by_filename[filename]["llm_metric"].append(llm_metric)
        sentiment_score = calculate_sentiment_score(feedback)
        print(f"For the feedback, {feedback} we have the following sentiment_score - {sentiment_score}")
        sentiment_scores.append(sentiment_score)

    print("rating by filename: ", ratings_by_filename)
        
    for filename, metrics in ratings_by_filename.items(): # average rating calculation for plotting the graph
        if filename not in average_ratings:
            average_ratings[filename] = {}
        for key,value in metrics.items():
            if "star" in key:
                if value:
                    average_ratings[filename]["star"] = round(sum(value) / len(value) , 2)
                else:
                    average_ratings[filename]["star"] = 0.0
            else:
                for each_dict in value:
                    print("each_dict: ",each_dict)
                    if bool(each_dict):
                        for llm_key,llm_value in each_dict.items():
                            if llm_key not in llm_metric_ratings:
                                llm_metric_ratings[llm_key]=[]
                            llm_metric_ratings[llm_key].append(llm_value)
                if bool(llm_metric_ratings):
                    print("llm_metrics_ratings: ", llm_metric_ratings)
                    for metric_key,metric_value in llm_metric_ratings.items():
                        average_ratings[filename][metric_key] = round(sum(metric_value) / len(metric_value), 2)

    print("average_ratings for graph: ", average_ratings)


    # Plotting the graph for star and other metrics
    fig = go.Figure()
    
    for filename, avg_rating in average_ratings.items():
        for key,value in avg_rating.items():
            fig.add_trace(go.Bar(x=[key], y=[value], name=filename))


    fig.update_layout(title='Average Ratings for LLM-Usecase',
                      xaxis=dict(title='LLM-Usecase'),
                      yaxis=dict(title='Average Ratings / Scores'),
                      showlegend=False)

    # Plotting the graph for sentiment score
    sentiment_fig = go.Figure()
    print("sentiment_scores: ", sentiment_scores)
    # Histogram trace with customizations
    sentiment_fig.add_trace(go.Histogram(x=sentiment_scores,
                                        marker_color='skyblue', 
                                        opacity=0.65,  
                                        xbins=dict(size=0.1),  
                                        histnorm='probability',  
                                        name='Sentiment Score'))

    sentiment_fig.add_trace(go.Scatter(x=[np.mean(sentiment_scores)]*2, 
                                        y=[0, 0.15],  
                                        mode='lines',
                                        line=dict(color='red', width=2),
                                        name='Mean Sentiment Score'))


    sentiment_fig.update_layout(title='Distribution of User Feedbacks (Sentiment Scores)',
                                xaxis=dict(title='Sentiment Score'),
                                yaxis=dict(title='Probability Density'),
                                showlegend=True) 


    plot_div1 = fig.to_html(full_html=False)
    plot_div2 = sentiment_fig.to_html(full_html=False)


    return render_template('index.html', plot_div1=plot_div1, plot_div2=plot_div2, calculated_metrics=calculated_metrics)


def app_run():
    app.run(host="0.0.0.0", port=8062, debug=False)