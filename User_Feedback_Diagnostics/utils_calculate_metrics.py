from textblob import TextBlob
import json
from dateutil import parser


def calculate_sentiment_score(feedback_text):
    return TextBlob(feedback_text).sentiment.polarity

def parse_rating_info(line):
    # filename, rating_value, feedback_text, timestamp = [info.split(": ")[1] for info in line.strip().split(",")]
    rating_value, feedback_text, faithfulness_score, relevancy_score, timestamp = [info.split(": ")[1] for info in line.strip().split(",")]
    return round(float(rating_value),2), feedback_text, round(float(faithfulness_score), 2), round(float(relevancy_score),2), timestamp

def process_ratings_file(file_path):
    with open(file_path, 'r') as file:  
        ratings_list = json.load(file)       

    print("ratings list: ",ratings_list)
    
    feedback_texts = []
    sentiment_scores = []
    total_sentiment_score = 0.0
    total_rating = 0
    total_score={}
    num_score={}
    num_ratings = 0

    for ratings_dict in ratings_list:
        num_ratings+=1
          
        for key,value in ratings_dict.items():
            if isinstance(value,str) and "filename" not in key.lower():
                    try:
                        parser.parse(value)
                    except:
                        feedback_texts.append(value)
                        print("feedback: ",value)
                        sentiment_score = calculate_sentiment_score(value)
                        print("sentiment score: ",sentiment_score)
                        total_sentiment_score += sentiment_score
                        print("total sentiment score: ",total_sentiment_score)

            elif "star" in key.lower() or "rating" in key.lower():
                total_rating += value
            elif isinstance(value,( int, float)) :
                if key not in total_score:
                    total_score[key] = 0
                    num_score[key] = 0

                total_score[key] += value
                num_score[key] +=1
            
            
    print("numratings:",num_ratings)

    score_averages = {key: (round(total_score[key] / num_score[key],2)) 
                            for key in total_score} # keys are ratings, faithfulness, relevancy
    average_sentiment_score = round(total_sentiment_score / num_ratings, 2) if num_ratings > 0 else 0
    average_rating = round(total_rating / num_ratings , 2) if num_ratings > 0 else 0
        
    return score_averages, average_rating, average_sentiment_score