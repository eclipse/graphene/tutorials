# User-Feedback-Diagnostics
The User-Feedback-Diagnostics for LLM pipleine module manages the following operations: Calculating the overall star ratings, LLM evaluating metric scores, overall sentiment score for the feedback it received. A sample plot of the average ratings for each pipeline run is displayed here for the user, measuring the effectiveness of the output from the LLM.

![UserDiagnostics](UserDiagnostics.png)

Guidelines to use the node in an LLM pipeline :

1. The container that delivers the data for metric calculation in a pipeline needs to write the data into a json file and store it in a shared folder. Example: Grounding LLM pipleine and ReadMe-pipeline

2. The node accepts the following parameters for metrics calculation:        

    Filename : optional
    Timestamp: optional
    Ratings: optional
    LLM_Metrics : optional -> accepts n number of metrics in int, float

    Here a user has the option to either provide the data for these parameters or not. Based on the provided parameters the node will be able to handle the calculation and display average ratings.

The user ratings are on a scale of 1 to 5 to gauge user satisfaction with the generated output or the LLM's effectiveness. The faithfulness and answer relevancy score ranges between 1.0 to 0.0 where higher the score indicates good efficiency. This is automatically calculated by the modal.

Overall, the User-Feedback-Diagnostics module is vital in continuously improving the LLM pipeline that generates the grounded output by providing valuable insights from user ratings and feedback.



