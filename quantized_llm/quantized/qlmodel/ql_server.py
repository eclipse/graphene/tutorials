import time
import grpc
import model_pb2
import model_pb2_grpc
from concurrent import futures 
import threading
import json
import queue

from gradioui import serve_gradio
import config

from logger import Logger
logger = Logger(__name__)

port = 8061

class QLModel(model_pb2_grpc.QLModelServicer):

    def __init__(self):
        logger.info("server started")
        #logger.info('MetricsAvailable')
        self.input_queue = queue.Queue(maxsize=1)

    def get_metrics_metadata(self, request, context):
        ...
        # try:
        #     now = datetime.datetime.now()
        #     current_time = now.strftime("%Y-%m-%d %H:%M:%S")
        #     self.detrOd.load_custom_model()
        #     metric_back = self.detrOd.get_metrics(threshold=config.detection_threshold)
        #     if not metric_back["coco_metric"]:
        #         return model_pb2.Empty() 
            
        #     final_metrics_dict = {
        #         'metrics': {
        #             "type": "coco-metrics/v1",
        #             "date_time": current_time,
        #             "status_text": "success",
        #             ""
        #             "more_is_better": {
                        
        #                 "AP": round(metric_back["coco_metric"]["map"].item(),6) ,
        #                 "AP_50": round(metric_back["coco_metric"]["map_50"].item(),6),
        #                 "AP_75": round(metric_back["coco_metric"]["map_75"].item(),6),
        #                 "APsmall": round(metric_back["coco_metric"]["map_small"].item(),6),
        #                 "APmedium": round(metric_back["coco_metric"]["map_medium"].item(),6),
        #                 "APlarge": round(metric_back["coco_metric"]["map_large"].item(),6),
        #                 "mar_1": round(metric_back["coco_metric"]["mar_1"].item(),6),
        #             },
        #             "less_is_better": {},
        #             "meta_data": metric_back["meta_data"]
        #         }
        #     }
        #     logger.info(final_metrics_dict)

        # except Exception as e:
        #     logger.error("Error while getting metrics")
        #     logger.error(e, exc_info=True)
        #     context.set_code(grpc.StatusCode.NOT_FOUND)
        
    def run_request2(self, request_iterator, context):
        try:
            for request in request_iterator:
                # enqueue request in queue
                while True:
                    try:
                        # with timeout to permit detection of killed caller
                        self.input_queue.put(request, timeout=1.0)
                    except queue.Full:
                        if not context.is_active():
                            logger.info("RPC inactive - leaving callAnswersetSolver")
                            break
                    else:
                        break

                # leave if orchestrator disconnected
                if not context.is_active():
                    logger.info("RPC inactive - leaving callAnswersetSolver")
                    break
        except Exception as e:
            logger.info("exception in receiveAnswersetSolverResult: %s\n%s", e)

        return model_pb2.Empty()
    
    def run_request(self, request, context):
        try:
            logger.info("Prompt request recieved")
            prompt = request.prompt
            logger.info("Resposne generated")
            json_resp = json.dumps("generated output by me")

            self.input_queue.put({"prompt":prompt})
            response = model_pb2.ModelResp(response=json_resp)
            return response
        except Exception as e:
            logger.error("Error while running inference")
            logger.error(e, exc_info=True)
            context.set_code(grpc.StatusCode.NOT_FOUND)


def serve():
    grpcserver = grpc.server(
    futures.ThreadPoolExecutor(max_workers=10),
        options=(
            ('grpc.keepalive_time_ms', 10000), # send each 10 seconds
            ('grpc.keepalive_timeout_ms', 3000), # 3 second = timeout
            ('grpc.keepalive_permit_without_calls', True), # allow ping without RPC calls
            ('grpc.http2.max_pings_without_data', 0), # allow unlimited pings without data
            ('grpc.http2.min_time_between_pings_ms', 5000), # allow pings every 10 seconds
            ('grpc.http2.min_ping_interval_without_data_ms', 5000), # allow pings without data every 5 seconds
        )
    )
    model_pb2_grpc.add_QLModelServicer_to_server(QLModel(), grpcserver)
    grpcserver.add_insecure_port('[::]:{}'.format(port))
    grpcserver.start()
    logger.info("Start gradio web-ui")
    #threading.Thread(target=app_run()).start()
    threading.Thread(target=serve_gradio()).start()
    grpcserver.wait_for_termination()

if __name__ == '__main__':
    serve()
    while True:
        time.sleep(1)
