

#FROM pytorch/pytorch:2.1.0-cuda11.8-cudnn8-runtime
FROM nvidia/cuda:11.7.1-devel-ubuntu22.04

# We need to set the host to 0.0.0.0 to allow outside access
ENV HOST 0.0.0.0

RUN apt-get update && apt-get upgrade -y

RUN apt-get install -y git build-essential \
    python3 python3-pip gcc wget \
    ocl-icd-opencl-dev opencl-headers clinfo \
    libclblast-dev libopenblas-dev
RUN mkdir -p /etc/OpenCL/vendors && echo "libnvidia-opencl.so.1" > /etc/OpenCL/vendors/nvidia.icd

# setting build related env vars
ENV CUDA_DOCKER_ARCH=all
ENV LLAMA_CUBLAS=1

# Install depencencies
RUN python3 -m pip install --upgrade pip pytest cmake scikit-build setuptools fastapi uvicorn sse-starlette pydantic-settings starlette-context

# Install llama-cpp-python (build with cuda)
RUN CMAKE_ARGS="-DLLAMA_CUBLAS=on" pip install llama-cpp-python

ENV PYTHONUNBUFFERED=1

ENV SHARED_FOLDER_PATH "/test"
ENV PORT 8062

WORKDIR ${SHARED_FOLDER_PATH}
WORKDIR /llm

COPY license-1.0.0.json config.py __init__.py logger.py model.proto app.py ql_server.py requirements.txt ./
COPY static ./static
COPY templates ./templates

RUN python3 -m pip install -r requirements.txt

#ENV MODEL_NAME "mistral-7b-instruct-v0.1.Q4_K_M.gguf"
ENV MODEL_NAME "mistral-7b-instruct-v0.1.Q2_K.gguf"
ENV MODEL_DL_URL "https://huggingface.co/TheBloke/Mistral-7B-Instruct-v0.1-GGUF/resolve/main/mistral-7b-instruct-v0.1.Q2_K.gguf"

#COPY mistral-7b-instruct-v0.1.Q2_K.gguf ${SHARED_FOLDER_PATH}

ENTRYPOINT [ "python3","ql_server.py" ]

