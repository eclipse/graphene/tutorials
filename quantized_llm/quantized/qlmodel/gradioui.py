import gradio as gr
import requests
from llm import LLMModel

from logger import logging

port = 8061

llm = LLMModel()

with gr.Blocks() as gradio_ui:
    chatbot = gr.Chatbot()
    msg = gr.Textbox()
    clear = gr.Button("Clear")
    #submit_btn = gr.Button("submit")

    def send_request_to_localllm(data):
        resp = llm.chat(data)
        return resp
    
    def send_chat_request(data):
        url = ('[::]:{}'.format(port))
        endpoint = url + "/chat"
        #endpoint = "http://127.0.0.1:8062" + "/chat"
        try:
            r = requests.post(url = endpoint, json = data)
            resp_json = r.json()
            return resp_json
        except Exception as e:
            logging.error("Error calling llm endpoint.")
            logging.error(e)
            #raise gr.Error("Connection to server is failing!")
        
        return "yoooo"


    def convert_hitory_lama(history):
        history_dic_list = []

        # Add system promt. What should the chatbot do.
        system_message = "You are a friendly chatbot."
        new_prompt= {
            "role": "system",
            "content":system_message
        }
        history_dic_list.append(new_prompt)

        # add history to the new message if anything exists
        if history:
            for item in history:
                userpropmpt = {
                    "role": "user",
                    "content":item[0]
                }   
                systemanswer = {
                    "role": "system",
                    "content":item[1]
                }
                history_dic_list.append(userpropmpt)
                history_dic_list.append(systemanswer)
        
        return history_dic_list


    def bot(message,history):
        
        history_dic_list = convert_hitory_lama(history)
        # add the current prompt
        new_prompt= {
            "role": "user",
            "content":message
        } 
        history_dic_list.append(new_prompt)
        resp = send_request_to_localllm(history_dic_list)
        #resp = send_chat_request(history_dic_list)
        return "",history + [[message, resp]]
        ##TODO: demove live above and use below
        if resp:
            return "",history + [[message, resp]]
        else:
            return "",history

    msg.submit(bot, [msg, chatbot], [msg, chatbot], queue=False)
    clear.click(lambda: None, None, chatbot, queue=False)


def serve_gradio():
    gradio_ui.queue()
    gradio_ui.launch(server_port=8062, server_name="0.0.0.0")

# serve()