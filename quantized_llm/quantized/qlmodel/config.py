import os

def create_not_existing_dirs(list_of_paths):
     for dir in list_of_paths:
        if not os.path.exists(dir):
            # If not, create the folder
            os.makedirs(dir)
            print(f"Folder '{dir}' created successfully.")


SHARED_FOLDER = os.getenv("SHARED_FOLDER_PATH", "./dev/local_shared_folder/")  

port = int(os.getenv("PORT", "8062"))
default_model_name = os.getenv("DEFAULT_MODEL_NAME", "mistral-7b-instruct-v0.1.Q2_K.gguf")
default_model_path = os.getenv("DEFAULT_MODEL_PATH", "./dev")

create_not_existing_dirs([SHARED_FOLDER])