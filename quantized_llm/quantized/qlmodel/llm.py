import json

from llama_cpp.llama import Llama, LlamaGrammar
import requests
import os
import json
import copy

from logger import Logger
import config
logger = Logger(__name__)


class LLMModel:

    def __init__(self):
        self.path_to_parameter = os.path.join(config.SHARED_FOLDER, "parameter.json")
        self.default_parameters = {
            #"model_dl_url": config.default_model_dl_url,
            "model_name": config.default_model_name,
            "temp": 0.7,
            "top_p": 0.95,
            "top_k": 50,
            "n_ctx": 4096, # The max sequence length to use - note that longer sequence lengths require much more resources
            "echo":False,
            #n_threads:n_threads,            # The number of CPU threads to use, tailor to your system and the resulting performance
            # n_gpu_layers:n_gpu_layers   # The number of layers to offload to GPU, if you have GPU acceleration available
        }
        self.parameters = None
        self.model = None
        self.parameters_changed= True
        
        # save the first parameter dictionary
        self.save_parameter(self.default_parameters)
        # Load default model
        self.get_parameters()
        self.get_model()

    def get_model(self):
        if self.parameters_changed or self.model is None:
            # check if the default model is being used.
            if self.parameters["model_name"] == config.default_model_name:
                logger.debug("Default model is being used.")
                modelpath = os.path.join(config.default_model_path,self.parameters["model_name"])
            else:
                logger.debug("Model uploaded by user is being used.")
                modelpath = os.path.join(config.SHARED_FOLDER,self.parameters["model_name"])
            logger.info("Loading new model.")
            try:
                self.model = Llama(
                        model_path=modelpath,
                        temp=self.parameters["temp"],
                        top_p=self.parameters["top_p"],
                        top_k=self.parameters["top_k"],
                        echo=self.parameters["echo"],
                        stop = ["Q", "\n"],
                    )
            except ValueError:
                logger.error("Model path is empty: {}".format(modelpath))
        else:
            logger.debug("Model is already loaded")

        return modelpath
        

    def save_parameter(self, para):
        with open(self.path_to_parameter, mode="w") as f:
            json.dump(para, f)
            f.close()


    def _check_if_parameter_changed(self,new_paramters):
        if self.parameters:
            for key, item in self.parameters.items():
                if item != new_paramters[key]:
                    self.parameters_changed = True
                    return
            self.parameters_changed = False
        else:
            self.parameters_changed = True


    def get_parameters(self):
        try:
            # Reading JSON data from the file
            with open(self.path_to_parameter, mode="r") as file:
                data_read = json.load(file)
                self._check_if_parameter_changed(data_read)
                self.parameters = data_read
        except FileNotFoundError:
            logger.info(f"File not found: {self.path_to_parameter}. Please restart the container")
        except json.JSONDecodeError as e:
            logger.error(f"Error decoding JSON: {e}")



    def chat(self, prompt_dic):
        self.get_parameters()
        if self.parameters_changed or not self.model:
            modelpath = self.get_model()
            if not self.model:
                raise FileNotFoundError("Model could not get loaded. Please check if the model exists under: {}".format(modelpath))
        
        #data = request.get_json()

        resp = self.model.create_chat_completion(
            messages = prompt_dic
        )
        resp_return = resp["choices"][0]
        logger.info("finishing reason: " + resp_return["finish_reason"])
        logger.info("chat request finished")
        return resp_return["message"]["content"]




# def app_run():
#     app.secret_key = "qlmodel"
#     bootstrap = Bootstrap(app)
#     app.run(host="0.0.0.0", port=config.port)

# if __name__ == '__main__':
    
#     llm.chat([
#     {
#         "role": "system",
#         "content": "You are a story writing assistant."
#     },
#     {
#         "role": "user",
#         "content": "hi how are you."
#     }])