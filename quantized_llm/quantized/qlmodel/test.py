#!/usr/bin/env python3

import logging
import grpc
import sys
import json

import model_pb2
import model_pb2_grpc

logging.basicConfig(level=logging.INFO)

def main():
   # !!! here it is hardcoded to use the external port that ./helper.py assigns to the docker container !!!
   # !!! if you run without docker, you need to use port 8061 !!!
   channel = grpc.insecure_channel('localhost:8061')
   stub = model_pb2_grpc.QLModelStub(channel)

   # test evaluateSudokuDesign(...): convert a partial Sudoku field into an ASP Program
   evaluationjob = model_pb2.PromptConfig()
   # dummy field
   evaluationjob.prompt = "yooo"

   solverjob = stub.run_request(evaluationjob)
   logging.info("got solverjob response '%s'", solverjob)

   # test processEvaluationResult(...): convert a set of answer sets into a design evaluation result
   # TODO

main()
