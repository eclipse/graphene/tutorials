# Quantized LLM Model
This node can be used to use various quantized llms with .gguf format. The models should be put in the shared directory via jupyter lab and the path should be given to the llm.

# Changing the code

## Step 1: Apply changes

Apply your changes into the source code.

## Step 2: Generate gRPC classes for Python:

Open the terminal, change the directory to be in the same folder that the proto file is
in.
To generate the gRPC classes we have to install the needed libraries first:

* Install gRPC :
```cmd
python -m pip install grpcio
```

* To install gRPC tools, run:
```commandline
python -m pip install grpcio-tools googleapis-common-protos
```

* Now, run this command:
```commandline
python -m grpc_tools.protoc -I. --python_out=. --grpc_python_out=. model.proto
```
This command used model.proto file to generate the needed stubs to create the
client/server.
The files generated will be as follows:

model_pb2.py — contains message classes

* model_pb2.Features for the input features
* model_pb2.Prediction for the prediction review

model_pb2_grpc.py — contains server and client classes

* model_pb2_grpc.PredictServicer will be used by the server
* model_pb2_grpc.PredictStub the client will use it

Tested in python 3.10.13


## Step 3: Push to docker

Currenlty our container registery is being used to host the docker images:

### CPU Model
docker build -t cicd.ai4eu-dev.eu:7444/tutorials/quantized_llm/cpu_model:latest -f Dockerfile.cpu .
docker run cicd.ai4eu-dev.eu:7444/tutorials/quantized_llm/cpu_model:latest
docker push cicd.ai4eu-dev.eu:7444/tutorials/quantized_llm/cpu_model:latest

### GPU Model
docker build -t cicd.ai4eu-dev.eu:7444/tutorials/quantized_llm/gpu_model:latest -f Dockerfile.gpu .
docker run cicd.ai4eu-dev.eu:7444/tutorials/quantized_llm/gpu_model:latest
docker push cicd.ai4eu-dev.eu:7444/tutorials/quantized_llm/gpu_model:latest

Any registery can be used to host the images.