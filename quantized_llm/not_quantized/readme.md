## Step 3: Push to docker

Currenlty our container registery is being used to host the docker images:

docker build -t cicd.ai4eu-dev.eu:7444/tutorials/quantized_llm/llm_hf:latest .
docker run -p 8062:8062 cicd.ai4eu-dev.eu:7444/tutorials/quantized_llm/llm_hf:latest
docker push cicd.ai4eu-dev.eu:7444/tutorials/quantized_llm/llm_hf:latest

Any registery can be used to host the images.