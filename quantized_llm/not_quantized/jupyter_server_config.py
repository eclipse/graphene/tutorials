# Copyright (c) Jupyter Development Team.
# Distributed under the terms of the Modified BSD License.
# mypy: ignore-errors
import os
import stat
import subprocess
from pathlib import Path

from jupyter_server.auth import passwd

from jupyter_core.paths import jupyter_data_dir

c = get_config()  # noqa: F821
c.ServerApp.ip = "0.0.0.0"
c.ServerApp.open_browser = False
c.ServerApp.allow_root = True
c.ServerApp.allow_origin = "*"
c.ServerApp.port = int(os.getenv('PORT', 8888)) 
c.ServerApp.allow_remote_access=True

c.ServerApp.password = passwd(os.getenv("JUPYTER_PASSWORD", "test"))

c.FileContentsManager.delete_to_trash = True