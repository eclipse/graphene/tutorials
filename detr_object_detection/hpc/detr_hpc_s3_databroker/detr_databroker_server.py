import grpc
import model_pb2
import model_pb2_grpc
from concurrent import futures 
import threading
import os

from logger import Logger
logger = Logger(__name__)


class DetrDataBrokerServicer(model_pb2_grpc.DetrDatabroker):

    def __init__(self,config):
        self.config = config
        logger.info("server started")

    def send_training_signal(self, request, context):
        logger.info("Preparing training signal to be sent to the model.")
        out_message = model_pb2.TrainingConfig()
        
        training_status = "starting"
        out_message.config=training_status
        logger.info("Sending training signal to the model.")
        return out_message


def serve_grpc(config,port):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    model_pb2_grpc.add_DetrDatabrokerServicer_to_server(DetrDataBrokerServicer(config), server)
    server.add_insecure_port('[::]:{}'.format(port))
    server.start()
    return server
