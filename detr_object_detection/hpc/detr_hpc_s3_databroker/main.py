
import os 
from config import Config
import detr_databroker_server
from app import web_app
import argparse
import threading

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('-gp','--grpc-port', type=int , help='grpc port',required=True)
    parser.add_argument('-hp','--http-port', type=int , help='http port',required=True) 
    parser.add_argument('-f','--folder', type=str , help='Folder path',required=True) 

    args = parser.parse_args()

    # run_inference(args.input, args.output)

    prod_flag = os.getenv("PRODUCTION", False)
    config = Config(prod_flag, args.folder)
    grpc_server = detr_databroker_server.serve_grpc(config, args.grpc_port)
    threading.Thread(target=web_app, args=(config, args.http_port)).start()
    grpc_server.wait_for_termination()