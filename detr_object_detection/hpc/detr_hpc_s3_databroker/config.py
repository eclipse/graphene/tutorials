import os

class Config:
    def __init__(self, PROD_FLAG, SHARED_FOLDER):
        self.PROD_FLAG = PROD_FLAG
        self.SHARED_FOLDER = SHARED_FOLDER

        # get path to the models saving location
        if PROD_FLAG:
            self.custom_model_path = os.path.join(SHARED_FOLDER, os.environ['CUSTOM_MODEL_DIR'])
            self.dataset_path = os.path.join(SHARED_FOLDER, os.environ['DATASET_DIR'])
            self.input_path = os.path.join(SHARED_FOLDER, "inputimage")
            self.output_path = os.path.join(SHARED_FOLDER, "outputimage")
            self.status_folder = os.path.join(SHARED_FOLDER, "status")
            self.annotation_file_name = os.getenv("ANNOTATION_FILE_NAME")
            
        else:
            self.custom_model_path = "./dev/custom_model/"
            self.dataset_path = "./dev/dataset"
            self.input_path = "./dev/inputimage"
            self.output_path = "./dev/outputimage"
            self.status_folder = "./dev/status"
            self.annotation_file_name = "_annotations.coco.json"

        self.create_not_existing_dirs([SHARED_FOLDER,self.custom_model_path,self.dataset_path,self.input_path,self.output_path,self.status_folder])


    def create_not_existing_dirs(self, list_of_paths):
        for dir in list_of_paths:
            if not os.path.exists(dir):
                # If not, create the folder
                os.makedirs(dir)
                print(f"Folder '{dir}' created successfully.")
            

