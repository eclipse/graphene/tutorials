from flask import Flask,abort,render_template,request,redirect,url_for, Blueprint
#from flask_bootstrap import Bootstrap5
from flask_bootstrap import Bootstrap
from flask_wtf import FlaskForm
from wtforms.validators import DataRequired, ValidationError
from wtforms.fields import StringField, IntegerField, FloatField, SubmitField
import os
from logger import Logger
import zipfile
import s3_func
import shutil

logger = Logger(__name__)
config = None
app = Flask(__name__)


class S3_dataset_config(FlaskForm):
    data_endpoint_url = StringField('Endpoint URL', validators=[DataRequired()])
    data_s3_bucket = StringField('S3 Bucket', validators=[DataRequired()])
    data_s3_key = StringField('S3 Key', validators=[DataRequired()])
    data_access_key = StringField('S3 Access Key', validators=[DataRequired()])
    data_secret_key = StringField('S3 Secret Key', validators=[DataRequired()])
    data_save = SubmitField('Download from S3', render_kw={"onclick": "loading()"})

class S3_model_config(FlaskForm):
    endpoint_url = StringField('Endpoint URL', validators=[DataRequired()])
    s3_bucket = StringField('S3 Bucket', validators=[DataRequired()])
    s3_key = StringField('S3 Key', validators=[DataRequired()])
    access_key = StringField('S3 Access Key', validators=[DataRequired()])
    secret_key = StringField('S3 Secret Key', validators=[DataRequired()])
    save = SubmitField('Download from S3', render_kw={"onclick": "loading()"})


@app.route('/',methods = ['GET', 'POST'])
def view_home():
    annotation_file_name = config.annotation_file_name

    return render_template('index.html',annotation_file_name=annotation_file_name)


@app.route('/train',methods = ['GET', 'POST'])
def view_train():

    s3_dataset_config = S3_dataset_config()
    s3_model_config = S3_model_config()

    if request.method == 'POST':
        if s3_dataset_config.data_save.data and s3_dataset_config.validate_on_submit():
            endpoint_url = s3_dataset_config.data_endpoint_url.data
            s3_bucket = s3_dataset_config.data_s3_bucket.data
            s3_key = s3_dataset_config.data_s3_key.data
            access_key = s3_dataset_config.data_access_key.data
            secret_key = s3_dataset_config.data_secret_key.data
            clear_dir(config.dataset_path)
            s3_func.download_dir(endpoint_url,
                                    s3_bucket,
                                    s3_key,
                                    access_key,
                                    secret_key,
                                    config.dataset_path)
            
            change_new_train_flag(True)

        if s3_model_config.save.data and s3_model_config.validate_on_submit():
            endpoint_url = s3_model_config.endpoint_url.data
            s3_bucket = s3_model_config.s3_bucket.data
            s3_key = s3_model_config.s3_key.data
            access_key = s3_model_config.access_key.data
            secret_key = s3_model_config.secret_key.data
            clear_dir(config.custom_model_path)
            dl_number = s3_func.download_dir(endpoint_url,
                                    s3_bucket,
                                    s3_key,
                                    access_key,
                                    secret_key,
                                    config.custom_model_path)
            
            change_new_train_flag(True)
            
    return render_template('index_train.html',s3_dataset_config=s3_dataset_config, s3_model_config=s3_model_config)


@app.route('/inference',methods = ['GET', 'POST'])
def view_inference():
    s3_model_config = S3_model_config()

    if request.method == 'POST':
        if s3_model_config.save.data and s3_model_config.validate_on_submit():
            endpoint_url = s3_model_config.endpoint_url.data
            s3_bucket = s3_model_config.s3_bucket.data
            s3_key = s3_model_config.s3_key.data
            access_key = s3_model_config.access_key.data
            secret_key = s3_model_config.secret_key.data

            dl_number = s3_func.download_dir(endpoint_url,
                                 s3_bucket,
                                 s3_key,
                                 access_key,
                                 secret_key,
                                 config.custom_model_path)
            
            change_new_inference_flag(True)



    return render_template('index_inference.html', s3_model_config=s3_model_config)


@app.route('/uploadmodel',methods = ['POST'])
def upload_model():
    try:
        if request.files:
            logger.debug("A file being uploaded")
            clear_dir(config.custom_model_path)
            unzip_file(request.files["directory"], config.custom_model_path)
            change_new_inference_flag(True)
            change_new_train_flag(True)
        else:
            logger.debug("request was empty. returning to home")
    except Exception as e:
        logger.error("Upload failed")

    return redirect('/')


@app.route('/uploaddataset',methods = ['POST'])
def upload_dataset():
    try:
        if request.files:
            logger.debug("A file being uploaded")
            clear_dir(config.dataset_path)
            unzip_file(request.files["directory"], config.dataset_path)
            change_new_train_flag(True)
        else:
            logger.debug("request was empty. returning to home")
    except Exception as e:
        logger.error("Upload failed")

    return redirect('/')


@app.route('/uploadimage',methods = ['POST'])
def upload_image():
    try:
        if request.files:
            file = request.files['file']
            if file.filename == '':
                return "No selected file"
            
            parts = file.filename.split('.')
            # Take the first part (substring until the first dot)
            file_format = parts[-1]
            inference_image_path = f"{config.input_path}/image.{file_format}"
            with open(config.status_folder+"/image_path.txt", mode="w") as f:
                f.write(inference_image_path)
                f.close()
            file.save(inference_image_path)
            logger.info("Image was uploaded.")
            change_new_inference_flag(True)
        else:
            logger.info("request was empty. returning to home")
    except Exception as e:
        logger.error(e, exc_info=True)
        logger.error("Upload failed")
    return redirect('/')


def get_image_path():
    file_path = config.status_folder+"/image_path.txt"
    path_line = None 
    if os.path.exists(file_path):
        with open(file_path, mode="r") as f:
            for line in f.readlines():
                print(line)
                path_line = line
        f.close()
    return path_line


def change_new_inference_flag(flag:bool):
    path_file = config.status_folder+"/inference_startable.txt"
    if flag:
        print("adding inference flag")
        open(path_file, 'w').close()
    else:
        print("removing inference flag")
        if os.path.exists(path_file):
            os.remove(path_file)


def change_new_train_flag(flag:bool):
    path_file = config.status_folder+"/train_startable.txt"
    if flag:
        print("adding train flag")
        open(path_file, 'w').close()
    else:
        print("removing train flag")
        if os.path.exists(path_file):
            os.remove(path_file)


def is_training_startable():
    if os.path.exists(config.status_folder+"/train_startable.txt"):
        return True
    return False


def is_inference_startable():
    if os.path.exists(config.status_folder+"/inference_startable.txt"):
        return True
    return False


def unzip_file(file_name, path):
    """Unzips a file into a directory."""
    archive = zipfile.ZipFile(file_name, "r")
    for member in archive.infolist():
        archive.extract(member, path)
    archive.close()


def clear_dir(directory_path):
    try:
        # Remove the directory and its contents
        shutil.rmtree(directory_path)
        # Recreate the directory
        os.makedirs(directory_path)
        logger.info(f"Directory {directory_path} removed and recreated successfully.")
    except Exception as e:
        logger.error(f"An error occurred: {str(e)}")



def web_app(config_main, webui_port):
    global config
    config = config_main
    logger.info("starting webUI")
    app.secret_key = "detr"
    bootstrap = Bootstrap(app)
    app.run(host="0.0.0.0", port=webui_port)
