import os

class Config:
    def __init__(self, PROD_FLAG, SHARED_FOLDER):
        self.PROD_FLAG = PROD_FLAG
        self.SHARED_FOLDER = SHARED_FOLDER

        # get path to the models saving location
        if PROD_FLAG:
            
            self.default_model_path = os.path.join(SHARED_FOLDER, os.environ['DEFAULT_MODEL_DIR'])
            self.custom_model_path = os.path.join(SHARED_FOLDER, os.environ['CUSTOM_MODEL_DIR'])
            self.dataset_path = os.path.join(SHARED_FOLDER, os.environ['DATASET_DIR'])
            self.lightning_logs_dir =  os.path.join(SHARED_FOLDER, os.environ['TENSOR_BOARD_LOG_DIR'])
            self.input_path = os.path.join(SHARED_FOLDER, "inputimage")
            self.output_path = os.path.join(SHARED_FOLDER, "outputimage")
            self.status_folder = os.path.join(SHARED_FOLDER, "status")
            self.log_every_n_steps = int(os.environ['LOG_EVERY_N_STEP'])
            self.annotation_file_name = os.environ['ANNOTATION_FILE_NAME']
            self.max_nu_metric_images = int(os.environ['MAX_NU_METRIC_IMAGES'])
            self.detection_threshold = float(os.environ['DETECTION_THRESHOLD'])
            
            
        else:
            self.custom_model_path = "./dev/custom_model2/"
            self.log_every_n_steps = 5
            self.annotation_file_name = "_annotations.coco.json"
            self.lightning_logs_dir = "./dev/tensorboard_logs/"
            self.dataset_path = "./dev/dataset/maritimsmall"
            self.input_path = "./dev/inputimage"
            self.output_path = "./dev/outputimage"
            self.status_folder = "./dev/status"
            self.max_nu_metric_images = 2
            self.detection_threshold= 0.8

        self.TENSORBOARD_FOLDER = os.getenv(self.lightning_logs_dir, "./detr/dev/tensorboard_logs")

        self.create_not_existing_dirs([SHARED_FOLDER,self.custom_model_path,self.default_model_path,self.dataset_path,self.lightning_logs_dir,self.input_path,self.output_path,self.status_folder])


    def create_not_existing_dirs(self, list_of_paths):
        for dir in list_of_paths:
            if not os.path.exists(dir):
                # If not, create the folder
                os.makedirs(dir)
                print(f"Folder '{dir}' created successfully.")