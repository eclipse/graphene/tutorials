import grpc
from timeit import default_timer as timer
from logger import Logger

logger = Logger(__name__)

# import the generated classes
import model_pb2
import model_pb2_grpc
import os

server_port = int(os.getenv("SERVER_PORT", "8061"))
port = int(os.getenv("PORT", "8062"))
import config

def run_inference(input_path, output_path):
    logger.debug("Calling HPP_Stub..")
    addr = 'localhost:{}'.format(server_port)
    logger.debug("connecting to {}".format(addr))
    with grpc.insecure_channel(addr) as channel:
        stub = model_pb2_grpc.DetrModelServicerStub(channel)
        
        #inference
        ui_request = model_pb2.ObjectDetectionInputFile()
        ui_request.input = input_path
        ui_request.output = output_path
        response = stub.detect_objects(ui_request)

        logger.debug("Inference finished")
    
    print(response)

def start_train():
    logger.debug("Calling HPP_Stub for training")
    addr = 'localhost:{}'.format(server_port)
    logger.debug("connecting to {}".format(addr))
    with grpc.insecure_channel(addr) as channel:
        stub = model_pb2_grpc.DetrModelServicerStub(channel)
        
        ui_request = model_pb2.TrainingConfig()
        ui_request.config = "test"
        response = stub.startTraining(ui_request)


        logger.debug("Training finished")
    
    print(response)


import argparse
if __name__ == '__main__':
    # parser = argparse.ArgumentParser(description='Process some integers.')
    # parser.add_argument('-i','--input', type=str , help='Input Image',required=True) # will be accesible with args.OPT
    # parser.add_argument('-o','--output', type=str , help='Output Image',required=True) # will be accesible with args.OPT

    # args = parser.parse_args()

    # run_inference(args.input, args.output)
    start_train()
