import os
from transformers import DetrImageProcessor, DetrForObjectDetection
from od.train.detr import Detr
from od.train.dataload import DataProvider
from od.metrics.dataloader import CocoMetrics
from pytorch_lightning import Trainer
from pytorch_lightning import loggers as pl_loggers

import huggingface_hub
from torchmetrics.detection import MeanAveragePrecision

import torch
from PIL import Image
import torchvision
import torchvision.transforms.functional as F
from PIL import Image

from logger import Logger
logger = Logger(__name__)

class DetrOD():
    def __init__(self,config):
        self.config = config
        # get path to folder with scripts
        self.current_dir_path = os.path.dirname(os.path.abspath(__file__))
        #self.default_model_path = os.path.join(self.current_dir_path, "default_model")
        self.default_model_path = config.default_model_path
            
        model_path = self.default_model_path
        extractor_path = self.default_model_path
        try:
            # loading default model & extractor on the first stop always
            self.model = DetrForObjectDetection.from_pretrained(model_path)
            self.extractor = DetrImageProcessor.from_pretrained(extractor_path)
        except Exception:
            logger.info("Could not find the default model. Downloading it now")
            self.download_default_model()


    def download_default_model(self):
        checkpoint = "ciasimbaya/ObjectDetection"
        self.model = DetrForObjectDetection.from_pretrained(checkpoint)
        self.extractor = DetrImageProcessor.from_pretrained(checkpoint)
        # In HPC files cannot be saved under apptainer folders.
        self.extractor.save_pretrained(self.default_model_path)
        self.model.model.save_pretrained(self.default_model_path)


    def load_custom_model(self):
        if not os.path.exists(self.config.custom_model_path):
            logger.info("Custom model path does not exist: {}. Default model is being used".format(self.config.custom_model_path))
            return False
        
        if len(os.listdir(self.config.custom_model_path)) == 0:
            logger.info("Custom model path is empty. Default model is being used.")
            return False
        else:
            self.model = DetrForObjectDetection.from_pretrained(self.config.custom_model_path, local_files_only=self.config.PROD_FLAG)
            self.extractor = DetrImageProcessor.from_pretrained(self.config.custom_model_path, local_files_only=self.config.PROD_FLAG)
            logger.info("Custom model loaded.")
            return True
        

    def train(self, 
              lr:float=1e-4, 
              lr_backbone:float=1e-5, 
              weight_decay:float=1e-4,
              max_epoch:int=50,
              gradient_clip_val:float=0.1,
              accumulate_grad_batches:int=8,
              batch_size:int=2,
              dataloader_workers:int = 0
              ):
        logger.info("Training batch size is {}".format(batch_size))
        logger.info("Using databasee under this path for training: {}".format(self.config.dataset_path))
        logger.info("Trained model will be saved in this path: {}".format(self.config.custom_model_path))

        
        dataprovider = DataProvider(extractor=self.extractor, 
                                    dataset_path=self.config.dataset_path, 
                                    annotation_file_name=self.config.annotation_file_name, 
                                    batch_size=batch_size,
                                    dataloader_workers=dataloader_workers)
        
        # we will use id2label function for training
        categories = dataprovider.train_dataset.coco.cats
        id2label = {k: v['name'] for k,v in categories.items()}
        label2id = {v['name']: k for k,v in categories.items()}

        model = Detr(lr=lr, 
                    lr_backbone=lr_backbone, 
                    weight_decay=weight_decay, 
                    pretrained_model_name_or_path=self.default_model_path,
                    id2label=id2label,
                    label2id = label2id,
                    dataprovider=dataprovider)
        
        tb_logger = pl_loggers.TensorBoardLogger(save_dir=self.config.lightning_logs_dir)
        # Training
        gpu_count = torch.cuda.device_count()
        gpu_trainig= False
        # checking if gpu is available
        if torch.cuda.is_available() and gpu_count > 0:
            gpu_trainig=True
            logger.info("Cuda and {} GPUs are avaiable. Using GPU for training.".format(gpu_count))
            torch.set_float32_matmul_precision('medium')
            device = torch.device("cuda")
            trainer = Trainer(devices=torch.cuda.device_count(), accelerator="gpu", 
                            max_epochs=max_epoch, 
                            gradient_clip_val=gradient_clip_val, 
                            accumulate_grad_batches=accumulate_grad_batches, 
                            log_every_n_steps=5, 
                            logger=tb_logger)
            
            model = model.to(device) 
        else:
            logger.info("Cuda and GPU are not available. Using CPU for training.")
            trainer = Trainer(devices=1, 
                              max_epochs=max_epoch, 
                              gradient_clip_val=gradient_clip_val, 
                              accumulate_grad_batches=accumulate_grad_batches, 
                              log_every_n_steps=self.config.log_every_n_steps, 
                              logger=tb_logger)

        trainer.fit(model)
        # Saving
        self.extractor.save_pretrained(self.config.custom_model_path)
        model.model.save_pretrained(self.config.custom_model_path)
        logger.info("Saved model and extractor under: {} .".format(self.config.custom_model_path))

        if gpu_trainig:
            torch.set_float32_matmul_precision('default')


    def get_metrics(self,threshold=0.7):

        logger.info("Getting metrics with current threshhold of: {}".format(threshold))
        preds = []
        target = []
        val_dir = os.path.join(self.config.dataset_path, "valid")
        test_dir = os.path.join(self.config.dataset_path, "test")
        if os.path.exists(test_dir):
            coco_metrics_path = test_dir
        else:
            coco_metrics_path = val_dir
        METRIC_DATASET = CocoMetrics(image_directory_path=coco_metrics_path,
                                     annotation_file_name=self.config.annotation_file_name)
        
        num = 0
        max_num = self.config.max_nu_metric_images if self.config.max_nu_metric_images < len(METRIC_DATASET) else len(METRIC_DATASET)
        logger.info("Metric dataset has {} images. {} images will be used for calculating the metrics".format(len(METRIC_DATASET),max_num))

        for img in METRIC_DATASET:
            while num < max_num:
                logger.info("Processing image {}".format(num))
                target.append(METRIC_DATASET[img["image_id"]]["annotations"])
                # predict image
                image = img["image"].convert("RGB")
                inputs = self.extractor(images=image, return_tensors="pt")
                outputs = self.model(**inputs)

                target_sizes = torch.tensor([image.size[::-1]])

                results = self.extractor.post_process_object_detection(outputs, target_sizes=target_sizes, threshold=threshold)[0]

                preds.append({
                    "boxes":torch.round(results["boxes"]),
                    "scores":results["scores"],
                    "labels":results["labels"],
                })
                num +=1

        # calculate metrics
        metric = MeanAveragePrecision(iou_type="bbox")
        metric.update(preds, target)
        coco_metric = metric.compute()
        logger.info("Metric calculation finished")
        meta_data = {
            "metric_dataset_size": num,
            "threshold": threshold
        }

        return {
            "meta_data" : meta_data,
            "coco_metric": coco_metric
        }


    def inference(self, input_path, saving_path, threshold=0.7):
        detected_items = []
        logger.info("Inference job recieved. Saving path: {}".format(saving_path))

        try:
            image = Image.open(input_path)
            image = image.convert("RGB") 
            logger.info("Opened image under {}.".format(image))
        except Exception as e:
            logger.error("Error while loading the image")
            logger.error(e, exc_info=True)
            return detected_items

        logger.info("Image was opened successfully.")
        inputs = self.extractor(images=image, return_tensors="pt")
        outputs = self.model(**inputs)

        target_sizes = torch.tensor([image.size[::-1]])

        results = self.extractor.post_process_object_detection(outputs, target_sizes=target_sizes, threshold=threshold)[0]

        for score, label, box in zip(results["scores"], results["labels"], results["boxes"]):
            box = [round(i, 2) for i in box.tolist()]
            label = self.model.self.config.id2label[label.item()]
            logger.info(
                    f"Detected {label} with confidence "
                    f"{round(score.item(), 3)} at location {box}"
            )
            # Convert bounding box coordinates to integers
            x_min, y_min, x_max, y_max = [int(coord) for coord in box]
            bbox = [x_min, y_min, x_max, y_max]

            detected_item = {
                "label": label,
                "bbox": bbox
            }
            detected_items.append(detected_item)
        
        if detected_items:
            logger.info("Objects detected.")
            # draw items and save an image
            draw_bboxes = []
            draw_labels = []
            for item in detected_items:
                draw_bboxes.append(item["bbox"])
                draw_labels.append(item["label"])
            
            bbox_int = torch.tensor(draw_bboxes, dtype=torch.int)

            tesnor_image = F.to_tensor(image)
            tesnor_image = torchvision.transforms.functional.convert_image_dtype(tesnor_image, dtype=torch.uint8)
            tesnor_image = torchvision.utils.draw_bounding_boxes(tesnor_image, bbox_int, labels= draw_labels)
            # Convert the image tensor back to a PIL image for saving
            image = F.to_pil_image(tesnor_image)  
        else:
            logger.info("Nothing detected. Returning empty string.")

        image.save(saving_path)
        logger.debug("Finished inference for given picture.")
        return detected_items
