import os
from od.train.coco import CocoDetection
from torch.utils.data import DataLoader

from time import time
import multiprocessing as mp

from logger import Logger
logger = Logger(__name__)


class DataProvider():
    
    def __init__(self, extractor, dataset_path, annotation_file_name, batch_size, dataloader_workers):
        self.extractor = extractor
        train_dir = os.path.join(dataset_path, "train")
        val_dir = os.path.join(dataset_path, "valid")

        self.train_dataset = CocoDetection(
            image_directory_path=train_dir, 
            image_processor=extractor, 
            annotation_file_name=annotation_file_name,
            train=True)
        self.val_dataset = CocoDetection(
            image_directory_path=val_dir, 
            image_processor=extractor,
            annotation_file_name=annotation_file_name,
            train=False)
        
        
        self.train_dataloader = DataLoader(dataset=self.train_dataset, collate_fn=self.collate_fn, batch_size=batch_size, shuffle=True,num_workers=dataloader_workers)
        self.val_dataloader = DataLoader(dataset=self.val_dataset, collate_fn=self.collate_fn, batch_size=batch_size,num_workers=dataloader_workers)

        logger.info("Number of training examples: {}".format(len(self.train_dataset)))
        logger.info("Number of validation examples: {}".format(len(self.val_dataset)))


    def collate_fn(self, batch):
        # DETR authors employ various image sizes during training, making it not possible 
        # to directly batch together images. Hence they pad the images to the biggest 
        # resolution in a given batch, and create a corresponding binary pixel_mask 
        # which indicates which pixels are real/which are padding
        pixel_values = [item[0] for item in batch]
        encoding = self.extractor.pad(pixel_values, return_tensors="pt")
        labels = [item[1] for item in batch]
        return {
            'pixel_values': encoding['pixel_values'],
            'pixel_mask': encoding['pixel_mask'],
            'labels': labels
        }
    

    def get_val_dataloader(self):
        return self.val_dataloader


    def get_train_dataloader(self):
        return self.train_dataloader


    

