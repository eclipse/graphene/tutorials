import grpc
import model_pb2
import model_pb2_grpc
from concurrent import futures 
import json
import datetime
import os
import threading

from od.detrOD import DetrOD
from config import Config
from app import get_train_parameters

from logger import Logger
logger = Logger(__name__)

class DetrModelServicer(model_pb2_grpc.DetrModelServicer):

    def __init__(self, config):
        self.config = config
        self.detrOd = DetrOD(config)
        self.start_count = 0
        logger.info("server started")
        logger.info('MetricsAvailable')
        with open(self.config.status_folder+"/results.txt", mode="w") as f:
            f.write("Server is ready.")
            f.close()


    def startTraining(self, request, context):
        try:
            logger.info('MetricsAvailable')
            response = model_pb2.TrainStatus()
            # states:
            # 0: training not started.
            # 1: being trained.
            # 2: training done.
            # -1: Error
            if self.start_count == 0:
                logger.info("Starting the training...")
                self.detrOd.load_custom_model()
                parameters = get_train_parameters()
                logger.info("Got parameters")
                logger.info(parameters)
                self.start_count = 1
                with open(self.config.status_folder+"/results.txt", mode="w") as f:
                    f.write("Training started. Please wait until its finished")
                    f.close()
                self.detrOd.train(
                    lr=parameters["lr"], 
                    lr_backbone=parameters["lr_backbone"], 
                    weight_decay=parameters["weight_decay"],
                    max_epoch=parameters["max_epoch"],
                    gradient_clip_val=parameters["gradient_clip_val"],
                    accumulate_grad_batches=parameters["accumulate_grad_batches"],
                    batch_size = parameters["batch_size"],
                    dataloader_workers = parameters["dataloader_workers"]
                )
                logger.info("Training finished...")
                with open(self.config.status_folder+"/results.txt", mode="w") as f:
                    f.write("Training finished.")
                    f.close()
                self.start_count = 2
                response.status = self.start_count
                self.get_metrics_metadata(None,None)
                return response

            elif self.start_count == 1:
                context.set_code(grpc.StatusCode.NOT_FOUND)
                message = "Training is started and not finished yet."
                context.set_details(message)
                with open(self.config.status_folder+"/results.txt", mode="w") as f:
                        f.write(message)
                        f.close()
                response.status = self.start_count
                return response

            elif self.start_count == 2:
                message = "Training finished. Download your model from sharedfolder. Please return to the home page and start the training with the new parameters."
                logger.info(message)
                with open(self.config.status_folder+"/results.txt", mode="w") as f:
                    f.write(message)
                    f.close()
                self.start_count = 0
                response.status = self.start_count
                return response
            
            else:
                message = "Something went wrong. Please check the container or contact the publisher."
                logger.info(message)
                with open(self.config.status_folder+"/results.txt", mode="w") as f:
                    f.write(message)
                    f.close()
                self.start_count = -1
                response.status = self.start_count
                return response
        except Exception as e:
            logger.error("Error while running training")
            logger.error(e, exc_info=True)
            context.set_code(grpc.StatusCode.NOT_FOUND)


    def get_metrics_metadata(self, request, context):
        try:
            now = datetime.datetime.now()
            current_time = now.strftime("%Y-%m-%d %H:%M:%S")
            self.detrOd.load_custom_model()
            metric_back = self.detrOd.get_metrics(threshold=self.config.detection_threshold)
            if not metric_back["coco_metric"]:
                return model_pb2.Empty() 
            
            final_metrics_dict = {
                'metrics': {
                    "type": "coco-metrics/v1",
                    "date_time": current_time,
                    "status_text": "success",
                    ""
                    "more_is_better": {
                        
                        "AP": round(metric_back["coco_metric"]["map"].item(),6) ,
                        "AP_50": round(metric_back["coco_metric"]["map_50"].item(),6),
                        "AP_75": round(metric_back["coco_metric"]["map_75"].item(),6),
                        "APsmall": round(metric_back["coco_metric"]["map_small"].item(),6),
                        "APmedium": round(metric_back["coco_metric"]["map_medium"].item(),6),
                        "APlarge": round(metric_back["coco_metric"]["map_large"].item(),6),
                        "mar_1": round(metric_back["coco_metric"]["mar_1"].item(),6),
                    },
                    "less_is_better": {},
                    "meta_data": metric_back["meta_data"]
                }
            }
            logger.info(final_metrics_dict)

        except Exception as e:
            logger.error("Error while getting metrics")
            logger.error(e, exc_info=True)
            context.set_code(grpc.StatusCode.NOT_FOUND)
        

    def detect_objects(self, request, context):
        try:
            logger.info("Inference request recieved")
            self.detrOd.load_custom_model()
            input_path = request.input
            saving_path = request.output
            #saving_image = os.path.join(self.config.output_path, "output.jpg")
            output_dic = self.detrOd.inference(input_path, saving_path, threshold=self.config.detection_threshold)
            logger.info("Response generated")
            json_resp = json.dumps(output_dic)

            response = model_pb2.ObjectDetectionOutputFile(path=json_resp)
            return response
        except Exception as e:
            logger.error("Error while running inference")
            logger.error(e, exc_info=True)
            context.set_code(grpc.StatusCode.NOT_FOUND)


def serve_grpc(config,port):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    model_pb2_grpc.add_DetrModelServicerServicer_to_server(DetrModelServicer(config), server)
    server.add_insecure_port('[::]:{}'.format(port))
    server.start()
    return server