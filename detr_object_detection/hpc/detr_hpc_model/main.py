from od.detrOD import DetrOD
import os 
from config import Config
import detr_server
from app import app_run
import argparse
import threading

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('-gp','--grpc-port', type=int , help='grpc port',required=True)
    parser.add_argument('-hp','--http-port', type=int , help='http port',required=True) 
    parser.add_argument('-f','--folder', type=str , help='Folder path',required=True) 

    args = parser.parse_args()

    # run_inference(args.input, args.output)

    prod_flag = os.getenv("PRODUCTION", False)
    config = Config(prod_flag, args.folder)
    grpc_server = detr_server.serve_grpc(config, args.grpc_port)
    #threading.Thread(target=app_run(config,args.http_port,args.grpc_port)).start()
    threading.Thread(target=app_run, args=(config, args.http_port, args.grpc_port)).start()
    grpc_server.wait_for_termination()