import grpc
import model_pb2
import model_pb2_grpc
from concurrent import futures 
import threading
import os
import app as webui
import config

from logger import Logger
logger = Logger(__name__)

port = 8061


class DetrDataBrokerServicer(model_pb2_grpc.DetrDatabroker):

    def __init__(self):
        logger.info("server started")

    def send_training_signal(self, request, context):
        logger.info("Preparing training signal to be sent to the model.")
        out_message = model_pb2.TrainingConfig()

        # checking if training message is already being sent
        if not webui.is_training_startable():
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details("No new data for training has been detected.")
            logger.info("Training not startable. Not sending training signal to the model.")
            return out_message
        
        training_status = "starting"
        out_message.config=training_status
        logger.info("Sending training signal to the model.")
        webui.change_new_train_flag(False)
        return out_message
        

    def send_detect_signal(self, request, context):
        logger.info("Preparing inference signal to be sent to the model.")
        image_path = webui.get_image_path()
        out_message = model_pb2.ObjectDetectionInputFile()
        
        # check if the request is already sent
        if not webui.is_inference_startable():
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details("No new data for inference has been detected.")
            logger.info("Inference not startable. Not sending inference signal to the model.")
            return out_message

        if not image_path:
            logger.error("No image to be found under the path")
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details("No image to be found under the path")
            return out_message
        
        if os.path.exists(image_path):
            out_message.path = image_path
            logger.info("Sending inference signal to the model.")
            webui.change_new_inference_flag(False)
            return out_message
        else:
            logger.error("Inference image is not saved and cannot be found. No request was sent")
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details("Inference image is not saved.")


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    model_pb2_grpc.add_DetrDatabrokerServicer_to_server(DetrDataBrokerServicer(), server)
    server.add_insecure_port('[::]:{}'.format(port))
    logger.info("Start server")
    server.start()

    logger.info("Start config web-ui")
    threading.Thread(target=webui.web_app()).start()

    server.wait_for_termination()

if __name__ == '__main__':
    serve()
