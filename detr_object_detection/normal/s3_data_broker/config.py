import os


def create_not_existing_dirs(list_of_paths):
     for dir in list_of_paths:
        if not os.path.exists(dir):
            # If not, create the folder
            os.makedirs(dir)
            print(f"Folder '{dir}' created successfully.")


# This variable will be set in deployment files of kubernetes
SHARED_FOLDER = os.getenv("SHARED_FOLDER_PATH", "./local_shared_folder")  

PROD_FLAG = os.getenv("PRODUCTION", False)

# get path to the models saving location
if PROD_FLAG:
    custom_model_path = os.path.join(SHARED_FOLDER, os.environ['CUSTOM_MODEL_DIR'])
    dataset_path = os.path.join(SHARED_FOLDER, os.environ['DATASET_DIR'])
    input_path = os.path.join(SHARED_FOLDER, "inputimage")
    output_path = os.path.join(SHARED_FOLDER, "outputimage")
    status_folder = os.path.join(SHARED_FOLDER, "status")
    annotation_file_name = os.getenv("ANNOTATION_FILE_NAME")
    
else:
    custom_model_path = "./dev/custom_model/"
    dataset_path = "./dev/dataset"
    input_path = "./dev/inputimage"
    output_path = "./dev/outputimage"
    status_folder = "./dev/status"
    annotation_file_name = "_annotations.coco.json"


create_not_existing_dirs([SHARED_FOLDER,custom_model_path,dataset_path,input_path,output_path,status_folder])
            

