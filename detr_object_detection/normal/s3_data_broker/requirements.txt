bootstrap-flask~=2.3.2
flask~=3.0.0
flask-wtf~=1.2.1

grpcio~=1.48.1
grpcio-tools~=1.48.1
googleapis-common-protos~=1.61.0

boto3~=1.28.84