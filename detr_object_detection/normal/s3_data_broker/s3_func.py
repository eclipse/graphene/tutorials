import boto3
import os

def download_dir(endpoint_url, bucket, s3_dir_prefix, access_key, secret_key, download_dir):
    print("download dir is: {}".format(download_dir))
    s3config = {
        "endpoint_url": endpoint_url,
        "aws_access_key_id": access_key,
        "aws_secret_access_key": secret_key 
    }
    s3client = boto3.client("s3", **s3config)

    paginator = s3client.get_paginator('list_objects_v2')

    dl_number = 0
    # List all items that start with the prefix
    for page in paginator.paginate(Bucket=bucket, Prefix=s3_dir_prefix):
        if page["Contents"]:
            for contents in page["Contents"]:
                file_key = contents["Key"]
                saving_rel_folder = file_key
                if file_key.startswith(s3_dir_prefix):
                    saving_rel_folder = file_key.replace(s3_dir_prefix, '.')


                download_dir_current = os.path.normpath(os.path.join(download_dir,os.path.dirname(saving_rel_folder)))
                if not os.path.exists(download_dir_current):
                    # If not, create the folder
                    os.makedirs(download_dir_current)
                save_path = os.path.join(download_dir_current,os.path.basename(saving_rel_folder))
                
                
                s3client.download_file(Filename=save_path,Bucket=bucket,Key=file_key)
                dl_number+=1

    return dl_number