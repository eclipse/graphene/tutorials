# DETR Object detection
This model uses the detr to detect objects in a picture. The model was trained on COCO dataset and is now able to detect ["person", "bicycle", "car", "motorcycle", "airplane", "bus", "train", "truck",
    "boat","traffic light", "fire hydrant", "stop sign", "parking meter", "bench", "bird", "cat",
    "dog", "horse", "sheep", "cow", "elephant", "bear", "zebra", "giraffe", "backpack",
    "umbrella", "handbag", "tie", "suitcase", "frisbee", "skis", "snowboard",
    "sports ball", "kite", "baseball bat", "baseball glove", "skateboard", "surfboard",
    "tennis racket", "bottle", "wine glass", "cup", "fork", "knife", "spoon", "bowl",
    "banana", "apple", "sandwich", "orange", "broccoli", "carrot", "hot dog", "pizza", "donut",
    "cake", "chair", "couch", "potted plant", "bed", "dining table", "toilet",
    "tv", "laptop", "mouse", "remote", "keyboard", "cell phone", "microwave", "oven",
    "toaster", "sink", "refrigerator", "book", "clock", "vase", "scissors", "teddy bear",
    "hair drier", "toothbrush"]

It is also possible to train this model on a custom dataset. More on how the dataset is structured are explained in the s3-databroker webui.

# Changing the code

## Step 1: Apply changes

Apply your changes into the source code.


## Step 2: Generate gRPC classes for Python:

Open the terminal, change the directory to be in the same folder that the proto file is
in.
To generate the gRPC classes we have to install the needed libraries first:

* Install gRPC :
```cmd
python -m pip install grpcio
```

* To install gRPC tools, run:
```commandline
python -m pip install grpcio-tools googleapis-common-protos
```

* Now, run this command:
```commandline
python -m grpc_tools.protoc -I. --python_out=. --grpc_python_out=. model.proto
```
This command used model.proto file to generate the needed stubs to create the
client/server.
The files generated will be as follows:

model_pb2.py — contains message classes

* model_pb2.Features for the input features
* model_pb2.Prediction for the prediction review

model_pb2_grpc.py — contains server and client classes

* model_pb2_grpc.PredictServicer will be used by the server
* model_pb2_grpc.PredictStub the client will use it

Tested in python 3.8


## Step 3: Push to docker
docker build -t cicd.ai4eu-dev.eu:7444/tutorials/detr_objectdetection/detr_s3_databroker:latest .
docker push cicd.ai4eu-dev.eu:7444/tutorials/detr_objectdetection/detr_s3_databroker:latest
docker run cicd.ai4eu-dev.eu:7444/tutorials/detr_objectdetection/detr_s3_databroker:latest