from flask import Flask, render_template, redirect, url_for, session, request
from werkzeug.datastructures import MultiDict
from flask_bootstrap import Bootstrap
from flask_wtf import FlaskForm
from wtforms.validators import DataRequired, ValidationError, InputRequired
from wtforms.fields import StringField, IntegerField, FloatField, SubmitField
from collections import namedtuple
import os
import json
import copy

from logger import Logger
import config
logger = Logger(__name__)

app = Flask(__name__)

default_parameters_dict = {
    "lr": 1e-4,
    "lr_backbone": 1e-5,
    "weight_decay": 1e-4,
    "max_epoch": 200,
    "gradient_clip_val": 0.1,
    "accumulate_grad_batches": 8,
    "batch_size": 2,
    "dataloader_workers": 0
}

def set_train_parameters(para):
    with open(config.status_folder+"/parameter.json", mode="w") as f:
        json.dump(para, f)
        f.close()
    #session['parameters'] = para


def get_train_parameters():
    json_file_path = config.status_folder+"/parameter.json"
    parameters = copy.deepcopy(default_parameters_dict)
    try:
        # Reading JSON data from the file
        with open(json_file_path, mode="r") as file:
            data_read = json.load(file)

        # Check if data is available
        if data_read:
            logger.info("Parameters read successfully.")
            parameters = data_read
        else:
            logger.info("Parameters were not available")

    except FileNotFoundError:
        logger.info(f"File not found: {json_file_path}. Writing a new one")
        set_train_parameters(parameters)

    except json.JSONDecodeError as e:
        logger.error(f"Error decoding JSON: {e}")

    # param = session.get('parameters')
    # if param:
    #     #parameters = json.loads(param)
    #     parameters = param
    #     logger.info("Retrieved Parameters: {}".format(parameters))
    # else:
    #     parameters = copy.deepcopy(default_parameters_dict)

    # logger.debug(f"return training parameters: {parameters}")
    return parameters


class TrainerInputForm(FlaskForm):
    LR = FloatField('Learning rate', validators=[DataRequired()], default=default_parameters_dict["lr"])
    LRBackbone = FloatField('Learning rate for backbone layers', validators=[DataRequired()], default=default_parameters_dict["lr_backbone"])
    WeightDecay = FloatField('Weight Decay', validators=[DataRequired()], default=default_parameters_dict["weight_decay"])
    MaxEpoch = IntegerField('Max Epoch', validators=[DataRequired()], default=default_parameters_dict["max_epoch"])
    GradientClipVal = FloatField('Gradient Clip Value', validators=[DataRequired()], default=default_parameters_dict["gradient_clip_val"])
    AccumulateGradBatches = IntegerField('Accumulate Grad Batches', validators=[DataRequired()], default=default_parameters_dict["accumulate_grad_batches"])
    BatchSize = IntegerField('Batch size', validators=[DataRequired()], default=default_parameters_dict["batch_size"])
    Dataloader_workers = IntegerField('Number of Dataloader workers', validators=[InputRequired("You got to enter a valid worker value!")], default=default_parameters_dict["dataloader_workers"])

    Save = SubmitField('Save Parameters')


class TrainingStatusForm(FlaskForm):
    TrainingStatus = StringField('Training status will be displayed here. Please save the parameters first before observing the status.', render_kw={'readonly': True})
    Get = SubmitField('Get Status')


@app.route('/', methods=['GET', 'POST'])
def training_input():
        
    training_vals_form = TrainerInputForm()
    trainingStatusForm = TrainingStatusForm()


    if training_vals_form.Save.data and training_vals_form.validate_on_submit():
        logger.info("Processing user inputs")
        new_para = {}
        new_para["lr"] = training_vals_form.LR.data
        new_para["lr_backbone"] = training_vals_form.LRBackbone.data
        new_para["weight_decay"] = training_vals_form.WeightDecay.data
        new_para["max_epoch"] = training_vals_form.MaxEpoch.data
        new_para["gradient_clip_val"] = training_vals_form.GradientClipVal.data
        new_para["accumulate_grad_batches"] = training_vals_form.AccumulateGradBatches.data
        new_para["batch_size"] = training_vals_form.BatchSize.data
        new_para["dataloader_workers"] = training_vals_form.Dataloader_workers.data
        
        set_train_parameters(new_para)
        logger.info(f"User inputs taken: {new_para}")
        return redirect('/')

    if trainingStatusForm.Get.data and trainingStatusForm.validate_on_submit():
        if not os.path.exists(config.status_folder+"/results.txt"):
            logger.error("Results.txt does not exist. Cannot load the status.")
        else:
            with open(config.status_folder+"/results.txt", mode="r") as f:
                for line in f.readlines():
                    print(line)
                    trainingStatusForm.TrainingStatus.data = line
            f.close()

        logger.debug(f"Current training status: {trainingStatusForm.TrainingStatus.data}")

    current_para = json.dumps(get_train_parameters(), indent=2)
    return render_template("index.html", training_var_form=training_vals_form, trainingStatusForm=trainingStatusForm, current_para=current_para)


def app_run():
    app.secret_key = "detr"
    bootstrap = Bootstrap(app)
    app.run(host="0.0.0.0", port=8062)

#app_run()