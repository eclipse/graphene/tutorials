timm~=0.9.8
pytorch-lightning~=2.1.0
transformers~=4.34.1
pycocotools~=2.0.7
scipy~=1.11.4
tensorboard~=2.15.0

grpcio~=1.48.1
grpcio-tools~=1.48.1
googleapis-common-protos~=1.61.0
protobuf~=3.20.3

bootstrap-flask~=2.3.2
flask~=3.0.0
flask-wtf~=1.2.1