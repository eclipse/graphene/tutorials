import os

def create_not_existing_dirs(list_of_paths):
     for dir in list_of_paths:
        if not os.path.exists(dir):
            # If not, create the folder
            os.makedirs(dir)
            print(f"Folder '{dir}' created successfully.")

SHARED_FOLDER = os.getenv("SHARED_FOLDER_PATH", "./dev/local_shared_folder")  

PROD_FLAG = os.getenv("PRODUCTION", False)

# get path to the models saving location
if PROD_FLAG:
    custom_model_path = os.path.join(SHARED_FOLDER, os.environ['CUSTOM_MODEL_DIR'])
    dataset_path = os.path.join(SHARED_FOLDER, os.environ['DATASET_DIR'])
    lightning_logs_dir =  os.path.join(SHARED_FOLDER, os.environ['TENSOR_BOARD_LOG_DIR'])
    input_path = os.path.join(SHARED_FOLDER, "inputimage")
    output_path = os.path.join(SHARED_FOLDER, "outputimage")
    status_folder = os.path.join(SHARED_FOLDER, "status")
    log_every_n_steps = int(os.environ['LOG_EVERY_N_STEP'])
    annotation_file_name = os.environ['ANNOTATION_FILE_NAME']
    max_nu_metric_images = int(os.environ['MAX_NU_METRIC_IMAGES'])
    detection_threshold = float(os.environ['DETECTION_THRESHOLD'])
    
    
else:
    custom_model_path = "./dev/custom_model2/"
    log_every_n_steps = 5
    annotation_file_name = "_annotations.coco.json"
    lightning_logs_dir = "./dev/tensorboard_logs/"
    dataset_path = "./dev/dataset/maritimair"
    input_path = "./dev/inputimage"
    output_path = "./dev/outputimage"
    status_folder = "./dev/status"
    max_nu_metric_images = 2
    detection_threshold= 0.8

TENSORBOARD_FOLDER = os.getenv(lightning_logs_dir, "./detr/dev/tensorboard_logs")


create_not_existing_dirs([SHARED_FOLDER,custom_model_path,dataset_path,lightning_logs_dir,input_path,output_path,status_folder])