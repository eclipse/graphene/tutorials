import grpc
from timeit import default_timer as timer
from logger import Logger

logger = Logger(__name__)

# import the generated classes
import model_pb2
import model_pb2_grpc

port = 8061
import config

def testrun():
    logger.debug("Calling HPP_Stub..")
    with grpc.insecure_channel('172.17.0.2:{}'.format(port)) as channel:
        stub = model_pb2_grpc.DetrModelServicerStub(channel)
        
        #inference
        #ui_request = model_pb2.ObjectDetectionInputFile()
        #ui_request.path = "/home/sharedFolder/image/image1.png"
        #response = stub.detect_objects(ui_request)

        # training
        ui_request = model_pb2.TrainingConfig(config="test")
        response = stub.startTraining(ui_request)

    logger.debug("Training started")
    print(response)


if __name__ == '__main__':
    testrun()
