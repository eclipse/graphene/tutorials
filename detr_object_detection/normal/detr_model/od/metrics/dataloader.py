import os
import torchvision
import torch

class CocoMetrics(torchvision.datasets.CocoDetection):
    def __init__(
        self, 
        image_directory_path: str,
        annotation_file_name: str
    ):
        annotation_file_path = os.path.join(image_directory_path, annotation_file_name)
        super(CocoMetrics, self).__init__(image_directory_path, annotation_file_path)

    def __getitem__(self, idx):
        bbox_list = []
        label_list = []
        image, annotations = super(CocoMetrics, self).__getitem__(idx)
        for item in annotations:
            x_min, y_min, x_max, y_max = [int(coord) for coord in item["bbox"]] 
            bbox = [x_min, y_min, x_min+x_max, y_min+ y_max]
            bbox_list.append(bbox)
            label_list.append(item["category_id"])
        return {
            "image": image,
            "image_id":idx,
            "annotations":{
                "boxes":torch.tensor(bbox_list),
                "labels": torch.tensor(label_list)
            }  
        }