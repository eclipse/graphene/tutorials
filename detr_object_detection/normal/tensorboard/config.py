import os

SHARED_FOLDER = os.getenv("SHARED_FOLDER_PATH", "./dev/local_shared_folder")  

PROD_FLAG = os.getenv("PRODUCTION", False)

# get path to the models saving location
if PROD_FLAG:
    lightning_logs_dir =  os.path.join(SHARED_FOLDER, os.environ['TENSOR_BOARD_LOG_DIR'])
    
else:
    lightning_logs_dir = "../detr/dev/tensorboard_logs"

TENSORBOARD_FOLDER = lightning_logs_dir