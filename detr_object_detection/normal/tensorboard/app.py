import os
import config

path_to_logs = os.path.join(config.TENSORBOARD_FOLDER, "lightning_logs")

print("starting tensorflow in the folder: {}".format(path_to_logs))


os.system('tensorboard --bind_all --port=8062 --logdir=' + path_to_logs)
