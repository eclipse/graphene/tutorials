
### Push to docker
docker build -t cicd.ai4eu-dev.eu:7444/tutorials/detr_objectdetection/detr_tensorboard:latest .
docker push cicd.ai4eu-dev.eu:7444/tutorials/detr_objectdetection/detr_tensorboard:latest
docker run cicd.ai4eu-dev.eu:7444/tutorials/detr_objectdetection/detr_tensorboard:latest