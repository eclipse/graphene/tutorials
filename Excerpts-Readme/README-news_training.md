# Table of Contents
- [Overview](#overview)
- [Steps in Client-Server Communication](#steps-in-client-server-communication)
- [Insights into UML Diagram](#insights-into-uml-diagram)
- [Project Dependencies](#project-dependencies)
- [Docker Commands](#docker-commands)
- [Generic Information](#generic-information)


## Overview
The provided code defines a Protocol Buffers (protobuf) schema for a news classification system. It includes messages for configuring training (`TrainingConfig`), reporting training status (`TrainingStatus`), and handling news text and categories (`NewsText`, `NewsCategory`). The `TrainingConfig` message specifies parameters like data filenames, epochs, batch size, and model filename. The `TrainingStatus` message indicates the type of status, with sub-messages for metrics that are better when lower (`Lessisbetter`) or higher (`Moreisbetter`). The `NewsClassifier` service offers three RPC methods: `startTraining` to initiate training, `classify` to categorize news text, and `get_metrics_metadata` to retrieve training metrics.The provided code defines a Protocol Buffers (protobuf) schema using syntax version 3. It includes two message types: `Empty`, which has no fields, and `NewsText`, which contains a single string field for text. Additionally, it defines a `DatasetFeatures` message with fields for type, dataset name, description, size, and DOI ID. The `NewsDatabroker` service includes two RPC methods: `get_next`, which returns a `NewsText` message, and `get_dataset_metadata`, which returns a `DatasetFeatures` message, both taking an `Empty` message as input.The provided code defines a Protocol Buffers (proto3) schema for a service called "Tensorboard." It includes an empty message type called "Empty" and a message type "LoggingStatus" that contains a single integer field "status." The "Tensorboard" service has a remote procedure call (RPC) named "loggging" that takes an "Empty" message as input and returns a "LoggingStatus" message.The provided code defines a Protocol Buffers (protobuf) schema using syntax version 3. It includes an empty message type called `Empty` and a message type `TrainingConfig` that specifies parameters for training a model, such as filenames for training data and labels, number of epochs, batch size, validation ratio, and model filename. Additionally, it defines a service `NewsTrainer` with a remote procedure call (RPC) method `startTraining`, which takes an `Empty` message as input and returns a `TrainingConfig` message.


## Steps in Client-Server Communication
- **Initialization**: The client (e.g., a news classification application) initializes the communication process by establishing a connection to the `NewsDatabroker` service.

- **Data Request**: The client sends a request to the `NewsDatabroker` service to fetch news text and dataset metadata using the appropriate RPC method.

- **Data Retrieval**: The `NewsDatabroker` service processes the request, retrieves the necessary news text and metadata from its database, and sends the data back to the client.

- **Training Configuration**: The client prepares a `TrainingConfig` message with the desired parameters for training the news classification model.

- **Start Training**: The client sends the `TrainingConfig` to the `NewsClassifier` service, invoking the RPC method to start the training process.

- **Training Status Monitoring**: The client periodically requests the `TrainingStatus` from the `NewsClassifier` service to monitor the progress of the training.

- **Classification Request**: Once training is complete, the client sends a `NewsText` message to the `NewsClassifier` service to classify new news articles.

- **Classification Response**: The `NewsClassifier` service processes the classification request and returns the classification results to the client.

- **Metrics Retrieval**: The client can request performance metrics from the `NewsClassifier` service to evaluate the model's effectiveness.

- **Logging**: Throughout the process, the client may log relevant information using the `Tensorboard` service for visualization and analysis of training and classification performance.

- **Completion**: The communication process concludes when the client has successfully classified news articles and retrieved all necessary metrics, or when the client decides to terminate the session.


## Insights into UML Diagram
The UML diagram contains several classes including `ClassifierInputForm`, `NewsClassifier`, `NewsDatabroker`, `Tensorboard`, and `NewsTrainer`, among others. The relationships include associations with `FlaskForm` for input forms and dependencies on `object` for various stubs and servicers. There are no explicit inheritance relationships. The presence of `Stub` and `Servicer` classes suggests the use of the Proxy design pattern.
## Project Dependencies
The project dependencies and libraries, along with their versions and purposes, are as follows:

1. **grpcio==1.38.0**: A high-performance, open-source universal RPC framework that is used for communication between services.
2. **grpcio-tools==1.38.0**: Tools for generating gRPC client and server code from protocol buffer definitions.
3. **grpc-interceptor**: A library for intercepting gRPC calls, allowing for additional processing such as logging or authentication.
4. **multithreading**: A module that allows for concurrent execution of code, useful for improving performance in I/O-bound applications.
5. **protobuf==3.16.0**: A language-agnostic binary serialization format used for defining data structures and services in gRPC.
6. **numpy**: A library for numerical computing in Python, providing support for arrays and matrices, along with a collection of mathematical functions.
7. **tensorflow**: An open-source machine learning framework used for building and training machine learning models.
8. **keras2onnx**: A library for converting Keras models to the ONNX (Open Neural Network Exchange) format.
9. **tf2onnx**: A tool for converting TensorFlow models to the ONNX format.

Flask-related dependencies:
10. **Bootstrap-Flask==1.8.0**: A Flask extension that integrates Bootstrap with Flask applications.
11. **Flask==2.0.2**: A lightweight WSGI web application framework for Python.
12. **idna==3.3**: A library for handling Internationalized Domain Names (IDN).
13. **Jinja2==3.0.3**: A templating engine for Python, used by Flask for rendering templates.
14. **MarkupSafe==2.0.1**: A library for safe handling of HTML and XML strings.
15. **python-dateutil==2.8.2**: A powerful extension to the standard datetime module, providing additional features for date and time manipulation.
16. **Werkzeug==2.0.2**: A comprehensive WSGI web application library that Flask is built on.
17. **Flask-WTF==0.14.3**: An extension for Flask that integrates WTForms, providing form handling capabilities.
18. **WTForms==2.3.3**: A flexible forms validation and rendering library for Python.

Note: The `requirements.txt` files in the `classifier`, `databroker`, and `trainer` directories could not be accessed, so any additional dependencies listed there are not included in this summary.
## Docker Commands
1. Build the image:
   ```bash
   docker build -t news_trainer /data/shared/news_training
   ```

2. Run the container:
   ```bash
   docker run --name news_trainer_container news_trainer
   ```
## Generic Information
Page: GRPC
Summary: gRPC (gRPC Remote Procedure Calls) is a cross-platform high-performance remote procedure call (RPC) framework. gRPC was initially created by Google, but is open source and is used in many organizations. Use cases range from microservices to the "last mile" of computing (mobile, web, and Internet of Things). gRPC uses HTTP/2 for transport, Protocol Buffers as the interface description language, and provides features such as authentication, bidirectional streaming and flow control, blocking or nonblocking bindings, and cancellation and timeouts. It generates cross-platform client and server bindings for many languages. Most common usage scenarios include connecting services in a microservices style architecture, or connecting mobile device clients to backend services.
gRPC's use of HTTP/2 is considered complex. It makes it impossible to implement a gRPC client in the browser, instead requiring a proxy.Page: Protocol Buffers
Summary: Protocol Buffers (Protobuf) is a free and open-source cross-platform data format used to serialize structured data. It is useful in developing programs that communicate with each other over a network or for storing data.  The method involves an interface description language that describes the structure of some data and a program that generates source code from that description for generating or parsing a stream of bytes that represents the structured data.No good Wikipedia Search Result was found

![alt text](UML_news_training.png)