# Table of Contents
- [Overview](#overview)
- [Steps in Client-Server Communication](#steps-in-client-server-communication)
- [Insights into UML Diagram](#insights-into-uml-diagram)
- [Project Dependencies](#project-dependencies)
- [Docker Commands](#docker-commands)
- [Generic Information](#generic-information)


## Overview
The provided code defines a Protocol Buffers (proto3) schema for a service called "Databroker." It includes two message types: "Features," which holds various property attributes (e.g., MSSubClass, LotArea, YearBuilt), and "DatasetFeatures," which contains metadata about datasets (e.g., type, name, description, size, DOI_ID). The service offers two remote procedure calls (RPCs): "hppdatabroker," which returns property features, and "get_dataset_metadata," which returns dataset metadata.The provided code defines a Protocol Buffers (proto3) schema for a machine learning service that predicts house sale prices. It includes a `Features` message to capture various property attributes (e.g., MSSubClass, LotArea, YearBuilt) and a `Prediction` message for the predicted sale price. Additionally, a `TrainingStatus` message is defined, and the `Predict` service includes three RPC methods: `predict_sale_price` for price prediction, `regressor_metrics` for retrieving model metrics, and `get_metrics_metadata` for accessing metadata related to the training status.


## Steps in Client-Server Communication
### Communication Process Summary for the "Databroker" Protocol Buffers Service

- **Initial Node: Data Broker**
  - Acts as the central communication hub for client-server interactions.
  - Receives requests from clients and routes them to the appropriate service methods.

- **Client Interaction**
  - The client initiates communication by sending a request to the Data Broker.
  - Requests can be for property features or dataset metadata, depending on the RPC method invoked.

- **RPC Methods Overview**
  - **hppdatabroker**
    - Purpose: To retrieve property features.
    - Client sends a request containing property identifiers or criteria.
    - Data Broker processes the request and forwards it to the relevant service handling property features.
  
  - **get_dataset_metadata**
    - Purpose: To obtain metadata about datasets.
    - Client sends a request specifying the dataset of interest.
    - Data Broker forwards the request to the dataset metadata service.

- **Machine Learning Service Interaction**
  - The Data Broker also facilitates communication with the machine learning service for predicting house sale prices.
  
  - **Predict Service RPC Methods**
    - **predict_sale_price**
      - Client sends a request with property details encapsulated in the "Features" message.
      - Data Broker forwards the request to the prediction service.
      - The prediction service processes the input and returns a "Prediction" message with the predicted sale price.
    
    - **regressor_metrics**
      - Client requests performance metrics of the regression model.
      - Data Broker forwards this request to the metrics service.
      - The metrics service responds with relevant metrics data.
    
    - **get_metrics_metadata**
      - Client requests metadata about the metrics available.
      - Data Broker forwards this request to the metrics metadata service.
      - The service returns metadata information to the client.

- **Response Handling**
  - After processing the requests, the respective services send responses back to the Data Broker.
  - The Data Broker consolidates the responses and sends them back to the client.

- **Error Handling**
  - If any errors occur during the request processing, the Data Broker captures these errors and communicates them back to the client in a structured format.

- **Conclusion**
  - The communication process involves a clear flow of requests and responses between the client, Data Broker, and various services, ensuring efficient data retrieval and processing for property features and machine learning predictions.


## Insights into UML Diagram
The UML diagram contains several classes related to house price prediction, including `HppInputForm`, `DatabrokerStub`, `DatabrokerServicer`, `PredictStub`, and `PredictServicer`. `HppInputForm` extends `FlaskForm`, indicating a specialized form for input handling. There are dependencies between service classes and their respective stubs, suggesting the use of the Stub Pattern for testing and the Service Layer Pattern for encapsulating business logic.
## Project Dependencies
Here is a detailed explanation and comprehensive list of all project dependencies and libraries, including their versions, purposes, and any relevant configurations:

1. **Bootstrap-Flask==1.5.2**
   - **Purpose**: Integrates Bootstrap with Flask to facilitate responsive web design.
   
2. **Flask==1.1.2**
   - **Purpose**: A lightweight WSGI web application framework for Python.
   
3. **Flask-SQLAlchemy==2.5.1**
   - **Purpose**: Adds SQLAlchemy support to Flask applications, simplifying database interactions.
   
4. **Flask-WTF==0.14.3**
   - **Purpose**: Integrates WTForms with Flask, providing form handling capabilities.
   
5. **google==3.0.0**
   - **Purpose**: A library for accessing Google APIs.
   
6. **googleapis-common-protos==1.53.0**
   - **Purpose**: Common protocol buffers for Google APIs.
   
7. **grpcio==1.38.0**
   - **Purpose**: A high-performance RPC framework that can run in any environment.
   
8. **grpcio-tools==1.38.0**
   - **Purpose**: Provides tools for generating gRPC code from protocol buffer definitions.
   
9. **Jinja2==2.11.3**
   - **Purpose**: A templating engine for Python, used by Flask for rendering templates.
   
10. **pandas==1.1.5**
    - **Purpose**: A data manipulation and analysis library for Python, providing data structures like DataFrames.
    
11. **protobuf==3.16.0**
    - **Purpose**: A library for serializing structured data, used with gRPC.
    
12. **PyYAML==5.4.1**
    - **Purpose**: A YAML parser and emitter for Python, useful for configuration files.
    
13. **requests==2.25.1**
    - **Purpose**: A simple HTTP library for Python, used for making API calls.
    
14. **scikit-learn==0.24.2**
    - **Purpose**: A machine learning library for Python, providing tools for data mining and data analysis.
    
15. **sklearn==0.0**
    - **Purpose**: A placeholder for scikit-learn, typically used for compatibility.
    
16. **SQLAlchemy==1.4.7**
    - **Purpose**: A SQL toolkit and Object-Relational Mapping (ORM) system for Python.
    
17. **threadpoolctl==2.2.0**
    - **Purpose**: A library for controlling the number of threads used by native libraries.
    
18. **urllib3==1.26.5**
    - **Purpose**: A powerful HTTP library for Python, used for making requests.
    
19. **Werkzeug==1.0.1**
    - **Purpose**: A comprehensive WSGI web application library, used by Flask.
    
20. **WTForms==2.3.3**
    - **Purpose**: A flexible forms validation and rendering library for Python.

This list includes all the dependencies mentioned in the question, along with their purposes. The configurations for these libraries would typically be found in the respective `requirements.txt` files or in the application code itself, but since those files could not be accessed, this summary is based solely on the provided information.
## Docker Commands
To build and run the Docker image for the House Price Prediction application, you can use the following commands:

1. **Build the Docker image:**
   ```bash
   docker build -t house_price_prediction /data/shared/House_Price_Prediction
   ```

2. **Run the Docker container:**
   ```bash
   docker run -p 8061:8061 -p 8062:8062 house_price_prediction
   ``` 

These commands will build the image using the Dockerfile in the specified directory and run the container while mapping the necessary ports.
## Generic Information
Page: GRPC
Summary: gRPC (gRPC Remote Procedure Calls) is a cross-platform high-performance remote procedure call (RPC) framework. gRPC was initially created by Google, but is open source and is used in many organizations. Use cases range from microservices to the "last mile" of computing (mobile, web, and Internet of Things). gRPC uses HTTP/2 for transport, Protocol Buffers as the interface description language, and provides features such as authentication, bidirectional streaming and flow control, blocking or nonblocking bindings, and cancellation and timeouts. It generates cross-platform client and server bindings for many languages. Most common usage scenarios include connecting services in a microservices style architecture, or connecting mobile device clients to backend services.
gRPC's use of HTTP/2 is considered complex. It makes it impossible to implement a gRPC client in the browser, instead requiring a proxy.Page: Protocol Buffers
Summary: Protocol Buffers (Protobuf) is a free and open-source cross-platform data format used to serialize structured data. It is useful in developing programs that communicate with each other over a network or for storing data.  The method involves an interface description language that describes the structure of some data and a program that generates source code from that description for generating or parsing a stream of bytes that represents the structured data.Page: Docker (software)
Summary: Docker is a set of platform as a service (PaaS) products that use OS-level virtualization to deliver software in packages called containers. 
The service has both free and premium tiers. The software that hosts the containers is called Docker Engine. It was first released in 2013 and is developed by Docker, Inc.
Docker is a tool that is used to automate the deployment of applications in lightweight containers so that applications can work efficiently in different environments in isolation.

![alt text](UML_House_Price_Prediction-1.png)