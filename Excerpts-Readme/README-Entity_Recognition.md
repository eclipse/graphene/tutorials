# Table of Contents
- [Overview](#overview)
- [Steps in Client-Server Communication](#steps-in-client-server-communication)
- [Insights into UML Diagram](#insights-into-uml-diagram)
- [Project Dependencies](#project-dependencies)
- [Docker Commands](#docker-commands)
- [Generic Information](#generic-information)


## Overview
The provided code defines a Protocol Buffers (protobuf) schema for a machine learning model service called "FlairModel." It includes messages for text input, embeddings, and training configuration, specifying parameters such as data filenames, epochs, learning rate, batch size, and model filename. The service offers two RPC methods: `startTraining`, which initiates training with a given configuration and returns the training status, and `extract_entities_from_text`, which processes text to extract entities.The provided code defines a Protocol Buffers (proto3) schema for a training configuration service. It includes an empty message, an embedding message containing a string, and a training configuration message that specifies various parameters such as file names for training, validation, and test data, a list of embeddings, epochs, learning rate, batch size, and model filename. The service, named `TrainingConfiguration`, has a single RPC method `startTraining` that takes an empty message and returns a `TrainingConfig` message.The provided code defines a Protocol Buffers (proto3) schema for a service called "Tensorboard." It includes an empty message type and a message type "LoggingStatus" that contains a single integer field for status. The service has one RPC method, "logging," which takes an "Empty" message as input and returns a "LoggingStatus" message.


## Steps in Client-Server Communication
- **Initialization**: The client (data broker) initializes the communication process by establishing a connection to the server hosting the Protocol Buffers services.

- **Service Request**: The client sends a request to the desired service (e.g., "FlairModel," "TrainingConfiguration," or "Tensorboard") using the defined RPC methods.

- **Message Preparation**: The client prepares the appropriate message format as defined in the Protocol Buffers schema, including necessary parameters (e.g., text input, training configuration).

- **Data Transmission**: The client transmits the serialized message over the network to the server.

- **Server Reception**: The server receives the incoming request and deserializes the message to interpret the client's request.

- **Processing**: The server processes the request according to the specified service method (e.g., starting training, extracting entities, logging status).

- **Response Generation**: After processing, the server generates a response message, which may include results, status updates, or confirmation of actions taken.

- **Response Transmission**: The server serializes the response message and sends it back to the client.

- **Client Reception**: The client receives the response from the server and deserializes it to access the information.

- **Finalization**: The client processes the response (e.g., updating the UI, logging results) and may initiate further requests or terminate the communication as needed.


## Insights into UML Diagram
The UML diagram contains several classes including `NerInputForm`, `FlairModelServicer`, `FlairModelStub`, `FlairModel`, `TrainingConfiguration`, `TrainingConfigurationStub`, `TrainingConfigurationServicer`, `MultiCheckboxField`, `SelectMultipleField`, `TrainerInputForm`, `TensorboardStub`, `TensorboardServicer`, and `Tensorboard`. The relationships include dashed associations indicating inheritance or extensions from `FlaskForm` for `NerInputForm` and `TrainerInputForm`, and a similar relationship between `SelectMultipleField` and `MultiCheckboxField`. There are also dashed associations from `object` to various classes, suggesting a service or stub pattern. Notably, the design patterns observed include the Service Layer pattern and the Form Object pattern.
## Project Dependencies
Here is a detailed explanation and comprehensive list of all project dependencies and libraries, including their versions, purposes, and any relevant configurations:

1. **torch==1.7.1**
   - **Purpose**: A popular deep learning framework used for building and training neural networks.

2. **transformers==3.5.1**
   - **Purpose**: A library for natural language processing (NLP) that provides pre-trained models and tools for working with transformer architectures.

3. **protobuf==3.16.0**
   - **Purpose**: A language-neutral, platform-neutral extensible mechanism for serializing structured data, often used in gRPC.

4. **flair==0.7**
   - **Purpose**: A simple NLP library that allows you to apply state-of-the-art natural language processing (NLP) models.

5. **packaging~=21.3**
   - **Purpose**: A library for dealing with Python package versions and dependencies.

6. **grpcio==1.38.0**
   - **Purpose**: A high-performance RPC framework that can run in any environment, used for communication between services.

7. **grpcio-tools==1.38.0**
   - **Purpose**: Provides tools for generating gRPC client and server code from .proto files.

8. **googleapis-common-protos==1.53.0**
   - **Purpose**: Common protocol buffers used by Google APIs.

9. **Bootstrap-Flask==1.5.2**
   - **Purpose**: A Flask extension that integrates Bootstrap for building web applications.

10. **Flask==1.1.2**
    - **Purpose**: A lightweight WSGI web application framework in Python.

11. **Flask-SQLAlchemy==2.5.1**
    - **Purpose**: An extension for Flask that adds support for SQLAlchemy, a SQL toolkit and Object-Relational Mapping (ORM) system.

12. **Flask-WTF==0.14.3**
    - **Purpose**: An extension that integrates Flask with WTForms, providing form handling capabilities.

13. **google==3.0.0**
    - **Purpose**: A library for accessing Google APIs.

14. **WTForms==3.0.1**
    - **Purpose**: A flexible forms validation and rendering library for Python.

15. **Jinja2==2.11.3**
    - **Purpose**: A templating engine for Python, used by Flask for rendering templates.

16. **markupsafe==2.0.1**
    - **Purpose**: A library for safe string handling, used by Jinja2.

17. **itsdangerous==2.0.1**
    - **Purpose**: A library for securely signing data, used by Flask for session management.

18. **werkzeug==2.0.3**
    - **Purpose**: A comprehensive WSGI web application library that Flask is built on.

19. **tensorboard==2.11.0**
    - **Purpose**: A visualization tool for TensorFlow, useful for tracking and visualizing metrics during model training.

This list includes the dependencies mentioned in the original question, along with their purposes. If there are any additional dependencies in the `requirements.txt` files that I could not access, they would need to be reviewed directly from those files for a complete overview.
## Docker Commands
To build and run the Docker image for the Entity Recognition project, you can use the following commands:

1. Build the Docker image:
   ```bash
   docker build -t entity_recognition_image /data/shared/Entity_Recognition
   ```

2. Run the Docker container:
   ```bash
   docker run -d -p 8062:8062 -e SHARED_FOLDER_PATH=/path/to/logdir entity_recognition_image
   ```

Make sure to replace `/path/to/logdir` with the actual path to your log directory.
## Generic Information
Page: GRPC
Summary: gRPC (gRPC Remote Procedure Calls) is a cross-platform high-performance remote procedure call (RPC) framework. gRPC was initially created by Google, but is open source and is used in many organizations. Use cases range from microservices to the "last mile" of computing (mobile, web, and Internet of Things). gRPC uses HTTP/2 for transport, Protocol Buffers as the interface description language, and provides features such as authentication, bidirectional streaming and flow control, blocking or nonblocking bindings, and cancellation and timeouts. It generates cross-platform client and server bindings for many languages. Most common usage scenarios include connecting services in a microservices style architecture, or connecting mobile device clients to backend services.
gRPC's use of HTTP/2 is considered complex. It makes it impossible to implement a gRPC client in the browser, instead requiring a proxy.Page: Protocol Buffers
Summary: Protocol Buffers (Protobuf) is a free and open-source cross-platform data format used to serialize structured data. It is useful in developing programs that communicate with each other over a network or for storing data.  The method involves an interface description language that describes the structure of some data and a program that generates source code from that description for generating or parsing a stream of bytes that represents the structured data.Page: Docker (software)
Summary: Docker is a set of platform as a service (PaaS) products that use OS-level virtualization to deliver software in packages called containers. 
The service has both free and premium tiers. The software that hosts the containers is called Docker Engine. It was first released in 2013 and is developed by Docker, Inc.
Docker is a tool that is used to automate the deployment of applications in lightweight containers so that applications can work efficiently in different environments in isolation.

![alt text](UML_Entity_Recognition.png)