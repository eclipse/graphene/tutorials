import grpc
from concurrent import futures
import time
import news_trainer_pb2_grpc
import news_trainer_pb2
import os
import numpy as np
from tensorflow.keras.datasets import reuters
from tensorflow.keras.utils import to_categorical

from concurrent import futures
import threading

from app import app_run, get_parameters
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


word_index = reuters.get_word_index()
sorted_word_index = sorted(word_index.items())
sorted_word_index_key, sorted_word_index_value = zip(*sorted_word_index) # Note down the maximum no. of word indices present in the dataset

def vectorize_sequences(sequences, dimension=max(sorted_word_index_value)):
    results = np.zeros((len(sequences), dimension))
    for i, sequence in enumerate(sequences):
        results[i, sequence] = 1.
    return results

port = 8061
shared_folder = os.getenv("SHARED_FOLDER_PATH")

print(f'shared_folder: {shared_folder}')
train_data_path = shared_folder+"/reuters_training_data.npz"
train_labels_path = shared_folder+"/reuters_training_labels.npz"

(train_data, train_labels), (test_data, test_labels) = reuters.load_data(num_words=max(sorted_word_index_value))
x_train = vectorize_sequences(train_data)
one_hot_train_labels = to_categorical(train_labels)
np.savez_compressed(train_data_path, x_train);
np.savez_compressed(train_labels_path, one_hot_train_labels);
logging.basicConfig()

class NewsTrainer(news_trainer_pb2_grpc.NewsTrainerServicer):

    def __init__(self):
        self.start_count = 0

    def startTraining(self, request, context):
        response = news_trainer_pb2.TrainingConfig()
        if self.start_count > 0:
            if self.start_count == 1:
                print("training already done.")
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details("training done.")
            with open(shared_folder+"/results.txt", mode="w") as f:
                    f.write("Training completed.")
                    f.close()
        else:
            print("start the training...")
            params = get_parameters()
            with open(shared_folder+"/results.txt", mode="w") as f:
                f.write("Initial state - Please return to the home page and start the training with the new parameters.")
                f.close()
            response.training_data_filename = train_data_path
            response.training_labels_filename = train_labels_path
            response.epochs = params.epochs
            response.batch_size = params.batch_size
            response.validation_ratio = params.validation_ratio
            response.model_filename = shared_folder+'/'+params.model_filename
        self.start_count += 1
        return response


server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
news_trainer_pb2_grpc.add_NewsTrainerServicer_to_server(NewsTrainer(), server)
print("Starting grpc server. Listening on port : " + str(port))
server.add_insecure_port("[::]:{}".format(port))
server.start()
print("Start ner_trainingconfig web-ui")
threading.Thread(target=app_run()).start()
server.wait_for_termination()
