from flask import Flask, render_template
from flask_bootstrap import Bootstrap
from flask_wtf import FlaskForm
from wtforms.validators import DataRequired, ValidationError
from wtforms.fields import StringField, IntegerField, FloatField, SubmitField
from collections import namedtuple
import logging
import os

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
app = Flask(__name__)
Parameters = namedtuple("Parameters", ["epochs", "batch_size", "validation_ratio", "model_filename"])
parameters = Parameters(epochs=9, batch_size=512, validation_ratio=0.1, model_filename="reuters_model")

shared_folder = os.getenv("SHARED_FOLDER_PATH")


class TrainerInputForm(FlaskForm):
    Epochs = IntegerField('Epochs', validators=[DataRequired()], default=parameters.epochs)
    BatchSize = IntegerField('BatchSize', validators=[DataRequired()], default=parameters.batch_size)
    ValidationRatio = FloatField('ValidationRatio', validators=[DataRequired()], default=parameters.validation_ratio)
    ModelFilename = StringField('ModelFilename \
                                ***Please enter the name of the model to be saved without the extension. \
                                The model in both .h5 and .onnx format will be available in the shared folder.***', validators=[DataRequired()], default=parameters.model_filename)
    Save = SubmitField('Save Parameters')


class TrainingStatusForm(FlaskForm):
    TrainingStatus = StringField('Training status will be displayed here. Please save the parameters first before observing the status.')
    Get = SubmitField('Get Status')


@app.route('/', methods=['GET', 'POST'])
def training_input():
    global parameters
    form = TrainerInputForm()
    form2 = TrainingStatusForm()

    if form.Save.data and form.validate_on_submit():
        logger.debug("Processing user inputs")
        parameters = Parameters(
            epochs=form.Epochs.data,
            batch_size=form.BatchSize.data,
            validation_ratio=form.ValidationRatio.data,
            model_filename=form.ModelFilename.data)
        logger.debug(f"User inputs taken: {parameters}")

    if form2.Get.data and form2.validate_on_submit():
        if not os.path.exists(shared_folder+"/results.txt"):
            with open(shared_folder+"/results.txt", 'w'): pass
        
        with open(shared_folder+"/results.txt", mode="r") as f:
            for line in f.readlines():
                print(line)
                form2.TrainingStatus.data = line
        f.close()

        with open(shared_folder+"/results.txt",'w') as file:pass

        logger.debug(f"Current training status: {form2.TrainingStatus.data}")

    return render_template("index.html", example_form=form, example_form_2=form2)


def get_parameters():
    logger.debug(f"return training parameters: {parameters}")
    return parameters


def app_run():
    app.secret_key = "hpp"
    bootstrap = Bootstrap(app)
    app.run(host="0.0.0.0", port=8062)
    # app.run()
