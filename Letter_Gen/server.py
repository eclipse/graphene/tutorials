import grpc
import threading
from concurrent import futures
import logging
import json
import uuid
import os
import time
import ast

import letter_gen_pb2 as pb2
import letter_gen_pb2_grpc as pb2_grpc
from app import app_run, update_pdf_content, update_user_prompts

# Set up logging
logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)
# Configure logging
logger = logging.getLogger(__name__)

local_directory = os.getenv("SHARED_FOLDER_PATH")
logging.info(f"The SHARED_FOLDER_PATH is {local_directory}")


class LetterGen(pb2_grpc.LetterGenServicer):
    def __init__(self):
        super().__init__()
        self.responses = {}
        self.current_question = None  # Store the last processed prompt

    def instruct_llm_query(self, request, context):
        while True:
            pdf_content = update_pdf_content()
            params = update_user_prompts()

            if not params:
                time.sleep(0.5)
                continue

            self.new_question = params[0]
            if self.new_question == self.current_question:
                time.sleep(0.5)
                continue

            self.current_question = self.new_question

            question_id = str(uuid.uuid4())
            input_text = f"You asked: {self.new_question}"

            Pinput = pb2.PromptInput(
                system="",
                user="",
                context=pdf_content[0],
                prompt=input_text,
            )

            llm_query = pb2.LLMQuery(
                id=pb2.ConvoID(q_id=question_id),
                input=Pinput,
                app_type=pb2.AppType.LETTER_GEN,
            )
            # logging.info(f"Generated LLMQuery : {llm_query}")
            yield llm_query

            while question_id not in self.responses:
                time.sleep(0.1)

    def retrieve_llm_response(self, request_iterator, context):
        qa_pairs = []
        history = []
        with open(os.path.join(local_directory, "results.json"), "w") as f:
            json.dump([], f, indent=4)
        for answer in request_iterator:
            question_id = answer.id.q_id
            summary = self.extract_fields(answer.fields)
            self.responses[question_id] = {
                "summary": summary,
            }

            qa_pairs.append(
                {
                    "question_id": question_id,
                    "question": self.new_question,
                    "answer": summary,
                }
            )

            history.append(
                {
                    "Question ID": question_id,
                    "Question": self.new_question,
                    "Answer": summary,
                }
            )
            with open(os.path.join(local_directory, "results.json"), "w") as f:
                json.dump(history, f, indent=4)
            logging.info(f"Saved response for Question ID: {question_id}")

        return pb2.Status(message="All answers received successfully.")

    def extract_fields(self, fields_map):
        summary = ""

        if "data" in fields_map:
            try:
                raw_data = fields_map["data"]
                logging.info(f"Raw data field: {raw_data}")

                try:
                    data = ast.literal_eval(fields_map["data"])
                    # logging.info(f"Parsed data: {data}")

                    summary = data.get("summary", "")
                    if not summary:
                        logging.warning(f"'summary' field not found in data: {data}")

                except json.JSONDecodeError as e:
                    logging.error(f"Error parsing data field: {e}")
            except Exception as e:
                logging.error(f"Unexpected error while extracting fields: {e}")
        else:
            logging.warning("Data field not found in fields.")

        return summary


def serve(port):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    pb2_grpc.add_LetterGenServicer_to_server(LetterGen(), server)
    server.add_insecure_port(f"[::]:{port}")
    logging.info(f"Starting server. Listening on port: {port}")
    server.start()
    threading.Thread(target=app_run).start()
    server.wait_for_termination()


if __name__ == "__main__":
    port = 8061
    serve(port)
