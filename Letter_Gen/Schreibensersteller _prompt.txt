Prompt für den Workflow zur Generierung eines behördlichen Schreibens

Eingabe:

Sie bearbeiten einen 24-seitigen Antragsvordruck eines Verkehrsunternehmens, das eine Erstattung von Fahrgeldausfällen gemäß § 233 SGB IX beantragt. Der Antrag enthält unter anderem:

Angaben zum Antragsteller (Name, Adresse, Kontaktinformationen).
Den Zeitraum, für den die Erstattung beantragt wird.
Eine detaillierte Aufschlüsselung der Fahrgeldeinnahmen nach Regionen (z. B. NRW, andere Bundesländer).
Zwischensummen und Gesamtsummen, basierend auf regionalen Einnahmen.
Aufgabe:

Dokumentenextraktion:
Automatisch extrahieren Sie die folgenden Informationen aus dem Antragsvordruck:

Name und Adresse des Antragstellers.
Den angegebenen Abrechnungszeitraum.
Die Aufschlüsselung der Fahrgeldeinnahmen, einschließlich aller Zwischensummen und Gesamtsummen.
Auffälligkeiten wie Diskrepanzen in den Berechnungen (z. B. fehlerhafte Summen für NRW oder andere Regionen).
Validierung:
Analysieren und identifizieren Sie:

Fehler in den Berechnungen, insbesondere Abweichungen zwischen Zwischensummen und der Gesamtsumme.
Fehlende oder unklare Informationen. Falls Informationen fehlen, kennzeichnen Sie diese klar und deutlich, zum Beispiel als "Nicht verfügbar" oder "Fehlende Information".
Generierung eines behördlichen Schreibens:

Bestätigung:
Bestätigen Sie den Eingang des Antrags in einer klaren, formalen Weise.
Zusammenfassung:
Fassen Sie die wichtigsten Angaben aus dem Antrag zusammen, einschließlich der korrekten und inkorrekten Summen sowie etwaiger Diskrepanzen.
Wenn Daten fehlen, verwenden Sie explizite Platzhalter wie "Informationen fehlen" oder "Nicht verfügbar".
Wenn Daten vorhanden sind, fügen Sie diese direkt in das Schreiben ein, aber lassen Sie Felder, die nicht verfügbar sind, vollständig weg. Vermeiden Sie es, Platzhalter wie "[Information fehlt]" zu verwenden, wenn keine Daten vorhanden sind.
Anforderung von Unterlagen (falls nötig):
Falls Angaben unvollständig oder fehlerhaft sind, fordern Sie die notwendigen Unterlagen an, z. B. Quittungen oder Rechnungen. Geben Sie klar an, welche Unterlagen fehlen und warum.
Erklärung der Fehler (falls vorhanden):

Erläutern Sie festgestellte Fehler professionell und klar.
Falls Informationen oder Berechnungen fehlen oder unklar sind, sagen Sie explizit: „Es fehlen bestimmte Informationen“ oder „Die Berechnungen stimmen nicht überein“.
Schlagen Sie spezifische Korrekturmaßnahmen vor, wie z. B. die erneute Einreichung von Details zu Einnahmen aus NRW oder die Korrektur von Summen.