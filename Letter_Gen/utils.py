import re
from pdf2image import convert_from_path
from pytesseract import image_to_string
import logging
import html
import os
import json
import logging
from flask import send_file, abort

import html
from docx import Document
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
from docx.shared import Pt
from io import BytesIO

logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)


def extract_text_with_ocr(pdf_path, lang="deu", pages=None):
    try:
        logging.info(f"Starting OCR for file: {pdf_path}")
        images = convert_from_path(pdf_path)
        if pages is None:
            pages = range(len(images))

        text_by_page = []

        for page_number in pages:
            if page_number < len(images):
                image = images[page_number]
                page_text = image_to_string(image, lang=lang)
                page_text_cleaned = clean_text(page_text)
                text_by_page.append(page_text_cleaned)
            else:
                logging.warning(f"Page {page_number + 1} is out of range for the PDF.")

        return text_by_page
    except Exception as e:
        logging.error(f"Error during OCR extraction: {e}")
        return []


def clean_text(text):
    text = re.sub(r"\n+", " ", text)
    text = re.sub(r"\s+", " ", text)
    text = text.strip()
    return text


def read_json_file(file_path):
    try:
        if os.path.exists(file_path):
            logging.info(f"Reading JSON file: {file_path}")
            with open(file_path, "r", encoding="utf-8") as file:
                return json.load(file)
    except Exception as e:
        logging.error(f"Error reading JSON file: {file_path}, {e}")
    return []


import html
import re


def convert_markdown_to_html(markdown_text):
    try:
        markdown_text = html.escape(markdown_text)
        markdown_text = markdown_text.replace("\n", "<br />")
        markdown_text = re.sub(r"\*\*(.*?)\*\*", r"<b>\1</b>", markdown_text)
        markdown_text = re.sub(r"_(.*?)_", r"<i>\1</i>", markdown_text)

        return markdown_text
    except Exception as e:
        logging.error(f"Error converting markdown to HTML: {e}")
        return markdown_text


def generate_document(text, output_format="pdf", filename="output_document"):
    additional_text = """<b>Fehlerhafte Berechnung:</b><br />
    Die Berechnung wurde durchgeführt.<br />
    Es wurde festgestellt, dass die Summierung der einzelnen Beträge (200,00 €, 100,00 €, 100,00 €) nicht der angegebenen Gesamtsumme von 500,00 € entspricht.<br />
    Die korrekte Summe der genannten Beträge beträgt 500,00 € und muss überprüft und korrigiert werden, da die Berechnungen nicht übereinstimmen."""

    additional_text_2 = """<b>Erforderliche Unterlagen:</b><br />
    Die Dokumente wurden erfolgreich angefordert.<br />
    1. Fahrkartenbelege: Kopien oder Scans der verwendeten Fahrkarten als Basisnachweis.<br />
    2. Rechnungen oder Quittungen: Zahlungsbelege zur Bestätigung des Ticketkaufs.<br />
    3. Wirtschaftsprüfertestat: Bestätigung, dass keine allgemeinen Zuschüsse oder Ausgleichszahlungen in den Fahrgeldeinnahmen enthalten sind.<br />
    4. Bankverbindungsnachweis: Dokument, das die korrekten Bankdaten für die Erstattung belegt.<br />
    5. Nachweis der Fahrgeldeinnahmen: Übersicht über die erhaltenen Fahrgeldeinnahmen im relevanten Zeitraum.<br />
    6. Korrigiertes Antragsformular: Falls Fehler im ursprünglichen Antrag vorliegen, wird ein neues, korrigiertes Antragsformular benötigt, das die richtigen Angaben enthält.<br />
    Bitte reichen Sie die erforderlichen Nachweise ein."""

    if output_format == "pdf":
        pdf_file = BytesIO()
        styles = getSampleStyleSheet()
        title_style = ParagraphStyle(
            "title", parent=styles["Heading1"], fontSize=14, spaceAfter=10, bold=True
        )
        normal_style = styles["Normal"]

        def handle_bold_text_pdf(paragraph):
            parts = paragraph.split("**")
            formatted_paragraph = ""
            for i, part in enumerate(parts):
                if i % 2 == 1:
                    formatted_paragraph += f"<b>{part}</b>"
                else:
                    formatted_paragraph += part
            return formatted_paragraph

        doc = SimpleDocTemplate(pdf_file, pagesize=A4)
        elements = []
        elements.append(
            Paragraph(
                "Betreff: Erstattung von Fahrgeldausfällen gemäß § 233 Sozialgesetzbuch IX",
                title_style,
            )
        )
        elements.append(Spacer(1, 12))

        for paragraph in text.split("\n\n"):
            formatted_paragraph = handle_bold_text_pdf(paragraph.strip())
            elements.append(Paragraph(formatted_paragraph, normal_style))
            elements.append(Spacer(1, 12))

        elements.append(Spacer(1, 12))
        elements.append(Paragraph(additional_text, normal_style))
        elements.append(Spacer(1, 12))
        elements.append(Paragraph(additional_text_2, normal_style))

        doc.build(elements)
        pdf_file.seek(0)
        return send_file(
            pdf_file,
            as_attachment=True,
            download_name=f"{filename}.pdf",
            mimetype="application/pdf",
        )

    elif output_format == "docx":
        docx_file = BytesIO()
        doc = Document()

        title = doc.add_paragraph()
        title_run = title.add_run(
            "Betreff: Erstattung von Fahrgeldausfällen gemäß § 233 Sozialgesetzbuch IX"
        )
        title_run.bold = True
        title.alignment = WD_PARAGRAPH_ALIGNMENT.LEFT
        doc.add_paragraph()

        paragraphs = text.split("<br />")

        for paragraph in paragraphs:
            p = doc.add_paragraph()

            bold_parts = re.findall(r"<b>(.*?)</b>", paragraph)

            if bold_parts:
                parts = re.split(r"(<b>.*?</b>)", paragraph)

                for part in parts:
                    if part.startswith("<b>") and part.endswith("</b>"):
                        clean_text = part[3:-4]
                        run = p.add_run(clean_text)
                        run.bold = True
                    else:
                        p.add_run(part)

            else:
                p.add_run(paragraph)

            p.style.font.size = Pt(11)

        doc.add_paragraph(additional_text)
        doc.add_paragraph(additional_text_2)

        doc.save(docx_file)
        docx_file.seek(0)

        return send_file(
            docx_file,
            as_attachment=True,
            download_name=f"{filename}.docx",
            mimetype="application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        )

    else:
        return abort(400, "Invalid output format. Please choose 'pdf' or 'docx'.")
