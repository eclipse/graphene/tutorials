**Betreff: Erstattung von Fahrgeldausfällen gemäß § 233 Sozialgesetzbuch IX**

Sehr geehrte Damen und Herren,

vielen Dank für Ihren Antrag auf Erstattung von Fahrgeldausfällen gemäß § 233 des Sozialgesetzbuches - Neuntes Buch (SGB IX), welcher sich auf die Fahrten im Zeitraum vom **[Datum von]** bis zum **[Datum bis]** bezieht. Wir haben Ihre Unterlagen geprüft und möchten Ihnen hiermit die Details zur Erstattung gemäß den geltenden rechtlichen Bestimmungen mitteilen.

Im Rahmen Ihrer Anfrage zur Erstattung von Fahrgeldausfällen wurden die Fahrgeldeinnahmen für die verschiedenen Verkehrslinien ermittelt, die im Abrechnungszeitraum im Verkehrsverbund sowie außerhalb des Verbundes erbracht wurden. Wir haben dabei insbesondere auch den Nachweis der Fahrgeldeinnahmen aus dem Nahverkehr gemäß **§ 231 Abs. 3 SGB IX** sowie die entsprechenden Einnahmeanteile berücksichtigt.

Die Erstattung erfolgt gemäß den im Antrag angegebenen Daten und den Bestimmungen des **§ 233 SGB IX**, die die Berechnung der Ausfälle und Erstattungsansprüche regeln.

Für weitere Rückfragen oder bei Unklarheiten stehen wir Ihnen gerne zur Verfügung.

Mit freundlichen Grüßen,

**Maximilian Schmidt**  
Sachbearbeiter, Abteilung Fahrgelderstattung  
Verkehrsservice GmbH  
Telefon: **0123 456 789**  
E-Mail: **max.schmidt@verkehrsservice.de**