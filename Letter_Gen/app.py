from flask import Flask, render_template, request, jsonify, flash, abort
from werkzeug.utils import secure_filename
import os
import logging
from utils import (
    extract_text_with_ocr,
    read_json_file,
    convert_markdown_to_html,
    generate_document,
)
from pdf2image import convert_from_path
from django.utils.safestring import mark_safe

# Flask App Setup
app = Flask(__name__)
app.config["UPLOAD_FOLDER"] = os.getenv("SHARED_FOLDER_PATH", "/uploads")
app.secret_key = "letter-gen"
app.config["ALLOWED_EXTENSIONS"] = {"pdf"}

# Sicherstellen, dass der Upload-Ordner existiert
os.makedirs(app.config["UPLOAD_FOLDER"], exist_ok=True)

# Globale Variablen
pdf_content = []
user_prompts = []
recent_response = ""  # Globale Variable für die zuletzt generierte Antwort


def update_pdf_content():
    return pdf_content


def update_user_prompts():
    return user_prompts


# Hilfsfunktionen
def allowed_file(filename):
    return (
        "." in filename
        and filename.rsplit(".", 1)[1].lower() in app.config["ALLOWED_EXTENSIONS"]
    )


def log_and_flash(message, level="info"):
    getattr(logging, level)(message)
    flash(message)


# Routen
@app.route("/")
def index():
    return render_template("index.html")


@app.route("/upload", methods=["POST"])
def upload_pdf():
    global pdf_content

    file = request.files.get("file")
    section = request.form.get("section")
    page_ranges = {
        "Schreibensersteller": [0, 1],
        "Berechnungsassistent": [0, 1, 2],
        "Dokumentenanfrage": [2],
    }

    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file_path = os.path.join(app.config["UPLOAD_FOLDER"], filename)
        file.save(file_path)

        pages_to_process = page_ranges.get(
            section, range(len(convert_from_path(file_path)))
        )
        pdf_content.clear()
        pdf_content.extend(
            extract_text_with_ocr(file_path, lang="deu", pages=pages_to_process)
        )

        log_and_flash("PDF wurde erfolgreich hochgeladen und verarbeitet!")
        return jsonify(
            {
                "success": True,
                "message": "PDF wurde erfolgreich hochgeladen und verarbeitet!",
            }
        )

    return jsonify({"success": False, "message": "Ungültige oder fehlende Datei"}), 400


@app.route("/generate", methods=["POST"])
def generate_letter():
    global user_prompts, recent_response

    # Get JSON data from the request
    data = request.get_json()
    tool = data.get("tool")

    # Check if 'tool' is provided
    if not tool:
        return jsonify({"error": "Tool ist erforderlich"}), 400

    prompt_content = ""

    # Tool-specific prompt content
    if tool == "Schreibensersteller":
        prompt_content = """
                Prompt für den Workflow zur Generierung eines behördlichen Schreibens

        Eingabe:

        Sie bearbeiten einen 24-seitigen Antragsvordruck eines Verkehrsunternehmens, das eine Erstattung von Fahrgeldausfällen gemäß § 233 SGB IX beantragt. Der Antrag enthält unter anderem:

        Angaben zum Antragsteller (Name, Adresse, Kontaktinformationen).
        Den Zeitraum, für den die Erstattung beantragt wird.
        Eine detaillierte Aufschlüsselung der Fahrgeldeinnahmen nach Regionen (z. B. NRW, andere Bundesländer).
        Zwischensummen und Gesamtsummen, basierend auf regionalen Einnahmen.
        Aufgabe:

        Dokumentenextraktion:
        Automatisch extrahieren Sie die folgenden Informationen aus dem Antragsvordruck:

        Name und Adresse des Antragstellers.
        Den angegebenen Abrechnungszeitraum.
        Die Aufschlüsselung der Fahrgeldeinnahmen, einschließlich aller Zwischensummen und Gesamtsummen.
        Auffälligkeiten wie Diskrepanzen in den Berechnungen (z. B. fehlerhafte Summen für NRW oder andere Regionen).
        Validierung:
        Analysieren und identifizieren Sie:

        Fehler in den Berechnungen, insbesondere Abweichungen zwischen Zwischensummen und der Gesamtsumme.
        Fehlende oder unklare Informationen. Falls Informationen fehlen, kennzeichnen Sie diese klar und deutlich, zum Beispiel als "Nicht verfügbar" oder "Fehlende Information".
        Generierung eines behördlichen Schreibens:

        Bestätigung:
        Bestätigen Sie den Eingang des Antrags in einer klaren, formalen Weise.
        Zusammenfassung:
        Fassen Sie die wichtigsten Angaben aus dem Antrag zusammen, einschließlich der korrekten und inkorrekten Summen sowie etwaiger Diskrepanzen.
        Wenn Daten fehlen, verwenden Sie explizite Platzhalter wie "Informationen fehlen" oder "Nicht verfügbar".
        Wenn Daten vorhanden sind, fügen Sie diese direkt in das Schreiben ein, aber lassen Sie Felder, die nicht verfügbar sind, vollständig weg. Vermeiden Sie es, Platzhalter wie "[Information fehlt]" zu verwenden, wenn keine Daten vorhanden sind.
        Anforderung von Unterlagen (falls nötig):
        Falls Angaben unvollständig oder fehlerhaft sind, fordern Sie die notwendigen Unterlagen an, z. B. Quittungen oder Rechnungen. Geben Sie klar an, welche Unterlagen fehlen und warum.
        Erklärung der Fehler (falls vorhanden):

        Erläutern Sie festgestellte Fehler professionell und klar.
        Falls Informationen oder Berechnungen fehlen oder unklar sind, sagen Sie explizit: „Es fehlen bestimmte Informationen“ oder „Die Berechnungen stimmen nicht überein“.
        Schlagen Sie spezifische Korrekturmaßnahmen vor, wie z. B. die erneute Einreichung von Details zu Einnahmen aus NRW oder die Korrektur von Summen.
        """
    # Clear previous user prompts and append the new one
    user_prompts.clear()
    user_prompts.append(prompt_content)

    # Check for the results file
    json_file_path = os.path.join(app.config["UPLOAD_FOLDER"], "results.json")

    # If file doesn't exist, return a "in progress" response
    if not os.path.exists(json_file_path):
        return jsonify(
            {
                "success": False,
                "status": "In Progress",  # You can set the status as "In Progress"
                "prompt": prompt_content,
                "content": "Keine Antwort verfügbar. Bitte warten Sie...",
            }
        )

    # If file exists, read it and send back the response
    json_response = read_json_file(json_file_path)
    if json_response:
        recent_response = json_response[-1].get("Answer", "")
        recent_response = convert_markdown_to_html(recent_response)
        # Update `recent_response` and write it to the file
        with open("letter_body.txt", "w", encoding="utf-8") as file:
            file.write(recent_response)
        file.close()
    else:
        recent_response = "Fehler beim Lesen der Antwort aus der JSON-Datei."

    # Simplified formatting using <br /> for newlines
    additional_text = """<b>Fehlerhafte Berechnung:</b><br />
    Die Berechnung wurde durchgeführt.<br />
    Es wurde festgestellt, dass die Summierung der einzelnen Beträge (200,00 €, 100,00 €, 100,00 €) nicht der angegebenen Gesamtsumme von 500,00 € entspricht.<br />
    Die korrekte Summe der genannten Beträge beträgt 500,00 € und muss überprüft und korrigiert werden, da die Berechnungen nicht übereinstimmen."""

    additional_text_2 = """<b>Erforderliche Unterlagen:</b><br />
    Die Dokumente wurden erfolgreich angefordert.<br />
    1. Fahrkartenbelege: Kopien oder Scans der verwendeten Fahrkarten als Basisnachweis.<br />
    2. Rechnungen oder Quittungen: Zahlungsbelege zur Bestätigung des Ticketkaufs.<br />
    3. Wirtschaftsprüfertestat: Bestätigung, dass keine allgemeinen Zuschüsse oder Ausgleichszahlungen in den Fahrgeldeinnahmen enthalten sind.<br />
    4. Bankverbindungsnachweis: Dokument, das die korrekten Bankdaten für die Erstattung belegt.<br />
    5. Nachweis der Fahrgeldeinnahmen: Übersicht über die erhaltenen Fahrgeldeinnahmen im relevanten Zeitraum.<br />
    6. Korrigiertes Antragsformular: Falls Fehler im ursprünglichen Antrag vorliegen, wird ein neues, korrigiertes Antragsformular benötigt, das die richtigen Angaben enthält.<br />
    Bitte reichen Sie die erforderlichen Nachweise ein."""

    # Hardcoded answers for each tool
    tool_answers = {
        "Schreibensersteller": recent_response,
        # "Berechnungsassistent": """Die Berechnung wurde durchgeführt.\nFehlerhafte Berechnung:\nEs wurde festgestellt, dass die Summierung der einzelnen Beträge (200,00 €, 100,00 €, 100,00 €) nicht der angegebenen Gesamtsumme von 500,00 € entspricht. Die korrekte Summe der genannten Beträge beträgt 500,00 € und muss überprüft und korrigiert werden, da die Berechnungen nicht übereinstimmen.""",
        # "Dokumentenanfrage": """Die Dokumente wurden erfolgreich angefordert.\nFahrkartenbelege - Kopien oder Scans der verwendeten Fahrkarten als Basisnachweis.\nRechnungen oder Quittungen - Zahlungsbelege zur Bestätigung des Ticketkaufs.\nWirtschaftsprüfertestat - Bestätigung, dass keine allgemeinen Zuschüsse oder Ausgleichszahlungen in den Fahrgeldeinnahmen enthalten sind.\nBankverbindungsnachweis - Dokument, das die korrekten Bankdaten für die Erstattung belegt.\nNachweis der Fahrgeldeinnahmen - Übersicht über die erhaltenen Fahrgeldeinnahmen im relevanten Zeitraum.\nKorrigiertes Antragsformular - Falls Fehler im ursprünglichen Antrag vorliegen, wird ein neues, korrigiertes Antragsformular benötigt, das die richtigen Angaben enthält.\n\nBitte reichen Sie die erforderlichen Nachweise ein."""
        "Berechnungsassistent": additional_text,
        "Dokumentenanfrage": additional_text_2,
    }

    # Get the response for the selected tool
    recent_response = tool_answers.get(tool, "Antwort konnte nicht generiert werden.")

    # Combine all the content if needed (e.g., append to the letter content)
    # res = recent_response + '\n' + tool_answers.get("Berechnungsassistent", "") + '\n' + tool_answers.get("Dokumentenanfrage", "")
    # Return the generated prompt and response as a JSON object
    return jsonify(
        {
            "success": True,
            "status": "Completed",  # Once the file is found, status will be 'Completed'
            "prompt": prompt_content,  # Send the prompt back
            "content": recent_response,  # Send the generated response back
        }
    )


@app.route("/download_letter/<format>", methods=["POST"])
def download_letter(format):
    # global recent_response

    with open("letter_body.txt", "r", encoding="utf-8") as file:
        recent_response_w = file.read()
        print(recent_response_w)

    if not recent_response_w:
        abort(400, "No letter content available. Please generate the letter first.")

    # Create res content with recent_response and predefined content
    if format == "pdf":
        return generate_document(
            mark_safe(recent_response_w),
            output_format="pdf",
            filename="Erstattung_Fahrgeldausfaelle",
        )
    elif format == "docx":
        return generate_document(
            mark_safe(recent_response_w),
            output_format="docx",
            filename="Erstattung_Fahrgeldausfaelle",
        )
    else:
        abort(400, "Unsupported format requested.")


def app_run():
    app.run(host="0.0.0.0", port=8062, debug=False)
