# Tutorials

This repository contains the container specification and tutorials for Graphene and AI4EU Experiments 

https://www.ai4europe.eu/development

# Limitations of the current version
* protobuf import and enum semantics not yet supported, some standard structures need to be copied

# Container Specification and Tutorials
Tutorials and examples for the AI4EU Experiments docker/grpc format for models:
* The container Specification describes how the docker container for AI4EU Experiments should be configured
* The first simple example is a predictor for house prices
* The second slightly more complex example is a classifier for sentiments in movie reviews
* Please have a look at the AI4EU Experiments playlist: https://www.youtube.com/playlist?list=PLL80pOdPsmF6s6P6i2vZNoJ2G0cccwTPa
* And the platform Manual: https://github.com/ai4eu/tutorials/blob/master/Deliverable_AI4EU_D3.3_Platfrom_Manual.pdf


## Generate README.md for Tutorials Easily

To generate README.md files for each tutorial, click the link below. This will take you to the `Readme-Gen-Pipeline` in AI-Builder, where you can quickly create them.

[![Generate README](https://img.shields.io/badge/Generate-README-brightgreen?style=for-the-badge&logo=lightning&logoColor=white)](https://aiexp-dev.ai4europe.eu/#/marketSolutions?solutionId=2c9d8c2b-d658-40d3-85b0-e0136c413af4&revisionId=25d06433-a583-4e88-9d70-052f1168260b&parentUrl=marketplace#md-model-detail-template)






