import os
import grpc
import threading
from concurrent import futures

import readme_gen_pb2
import readme_gen_pb2_grpc

from utils_folder import find_files
from utils_clone_repo import clone_repository
from utils_map_reduce_chain import summarize_protobuf_data
from tools import utils_docker_tool, utils_repo_structure_tool, utils_wikipedia_tool, utils_uml_tool
from app import app_run
from langchain_openai import OpenAI

local_directory = os.getenv("SHARED_FOLDER_PATH")
print('The SHARED_FOLDER_PATH is {}'.format(local_directory))


def initialize_llm():
    llm = OpenAI(
        model_name=os.environ["SELECTED_MODEL"],
        temperature=os.environ["TEMPERATURE"],
        max_tokens=os.environ["MAX_TOKENS"],
        top_p=os.environ["TOP_P"],
        frequency_penalty=os.environ["FREQUENCY_PENALTY"],
        presence_penalty=os.environ["PRESENCE_PENALTY"],
        openai_api_key=os.environ["API_KEY"]
    )
    return llm


class ReadmeGen(readme_gen_pb2_grpc.ReadmeGenServicer):
    def __init__(self) -> None:
        super().__init__()
        self.llm = None

    def clone_repository(self, request, context):
        repository_url = request.repository_url
        self.subdirectory_path = request.subdirectory_path
        message, cloned_path = clone_repository(repository_url, self.subdirectory_path, local_directory)

        if cloned_path:
            return readme_gen_pb2.CloneResponse(
                message=message,
                cloned_path=cloned_path
            )
        else:
            return readme_gen_pb2.CloneResponse(
                message=message
            )

    def get_llm_parameters(self, request, context):
        try:
            os.environ["API_KEY"] = request.api_key
            os.environ["ORG_ID"] = request.org_id
            os.environ["SELECTED_MODEL"] = request.selected_model
            os.environ["TEMPERATURE"] = str(round(request.temperature, 2))
            os.environ["MAX_TOKENS"] = str(request.max_tokens)
            os.environ["TOP_P"] = str(round(request.top_p, 2))
            os.environ["FREQUENCY_PENALTY"] = str(round(request.frequency_penalty, 2))
            os.environ["PRESENCE_PENALTY"] = str(round(request.presence_penalty, 2))
            print('Env variables set successfully')
        except Exception as e:
            print(e)

    def map_reduce_chain_summary(self, request, context):
        user_prompt = request.prompt
        try:

            if os.path.exists(context) and os.path.isdir(context):
                self.proto_paths = find_files(context, '*.proto')
                self.llm = initialize_llm()
                res_proto_summary, res_proto_summary_steps = summarize_protobuf_data(user_prompt, self.llm,
                                                                                      self.proto_paths)
                response = readme_gen_pb2.ResultText()
                response.message = 'success'
                response.individual_summary = res_proto_summary
                response.overall_summary = res_proto_summary_steps
                return response
            else:
                print("Error: The specified directory does not exist.")
                context.set_details("Error: The specified directory does not exist.")
                context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
                return readme_gen_pb2.ResultText(
                    message='Error: The specified directory does not exist.')

        except Exception as e:
            print('The following exception has occurred {}'.format(e))
            return readme_gen_pb2.ResultText(
                message='The following exception has occurred {}'.format(e))

    def agent_tool(self, request, context):
        tool_name = request.tool_name
        query = request.query

        try:
            if os.path.exists(context) and os.path.isdir(context):
                self.llm = initialize_llm()
                print(self.llm)
                if tool_name == "WikiAgentTool":
                    print(f"Generated output for {tool_name} with query: {query}")
                    query = query.split(", ")
                    output = utils_wikipedia_tool.run_tool(context, query)
                elif tool_name == "RepoStructureTool":
                    print(f"Generated output for {tool_name} with query: {query}")
                    output = utils_repo_structure_tool.run_tool(tool_name, context, self.llm, query)
                elif tool_name == "UMLAgentTool":
                    print(f"Generated output for {tool_name} with query: {query}")
                    output = utils_uml_tool.run_tool(tool_name, context, self.llm, query)
                elif tool_name == "DockerCommandsTool":
                    print(f"Generated output for {tool_name} with query: {query}")
                    output = utils_docker_tool.run_tool(tool_name, context, self.llm, query)
                else:
                    output = f"No tool available"

                return readme_gen_pb2.ResultTool(output=output)

            else:
                print("Error: The specified directory does not exist.")
                context.set_details("Error: The specified directory does not exist.")
                context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
                return readme_gen_pb2.ResultText(
                    message='Error: The specified directory does not exist.')

        except Exception as e:
            print('The following exception has occurred {}'.format(e))
            return readme_gen_pb2.ResultText(
                message='The following exception has occurred {}'.format(e))


def serve(port):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    readme_gen_pb2_grpc.add_ReadmeGenServicer_to_server(ReadmeGen(), server)
    server.add_insecure_port("[::]:{}".format(port))
    print("Starting server. Listening on port : " + str(port))
    server.start()
    threading.Thread(target=app_run()).start()
    server.wait_for_termination()


if __name__ == "__main__":
    port = 8061
    serve(port)
