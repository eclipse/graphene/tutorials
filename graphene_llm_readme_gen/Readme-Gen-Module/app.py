from flask import Flask, request, render_template, flash, send_file, jsonify
import os
from datetime import datetime
import markdown2
import readme_gen_pb2

app = Flask(__name__)
app.config['SECRET_KEY'] = 'readme_gen'
path = None
readme_last_modified = None
readme_ratings = {}

local_directory = os.getenv("SHARED_FOLDER_PATH")

def get_readme_last_modified():
    global readme_last_modified
    if path:
        readme_path = os.path.join(path, "README.md")
        if os.path.exists(readme_path):
            readme_last_modified = os.path.getmtime(readme_path)
    return readme_last_modified

@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')

@app.route('/clone', methods=['POST'])
def clone_repository():
    global path
    try:
        import server
        clone_service = server.ReadmeGen()
        repository_url = request.form.get('repository_url')
        subdirectory_path = request.form.get('subdirectory_path', '')
    
        clone_response = clone_service.clone_repository(
            readme_gen_pb2.CloneRequest(
                repository_url=repository_url,
                subdirectory_path=subdirectory_path
            ),
            None
        )
        if clone_response.cloned_path:
            path = clone_response.cloned_path
     
    except Exception as e:
        app.logger.error(f"An unexpected error occurred: {str(e)}")
        flash(f"An unexpected error occurred: {str(e)}", 'error')

    return render_template('index.html', message=clone_response.message, requested_repo_path=clone_response.cloned_path)


@app.route('/summary', methods=['POST'])
def map_reduce_chain_summary():
    global path
    user_prompt = request.form.get('user_prompt')  
    try:
        import server
        read_summary = server.ReadmeGen()
        summ_response = read_summary.map_reduce_chain_summary(
            readme_gen_pb2.PromptTemplate(prompt=user_prompt), path)
        if summ_response.message == 'success':
            if get_readme_last_modified() != os.path.getmtime(os.path.join(path, "README.md")):
                return send_file(os.path.join(path, "README.md"), as_attachment=True)
        else:
            return render_template('index.html', summ_message=summ_response.message)
    except Exception as e:
        app.logger.error(f"An unexpected error occurred: {str(e)}")
        flash(f"An unexpected error occurred: {str(e)}", 'error')
        return render_template('index.html', summ_message=(f"An unexpected error occurred: {str(e)}", 'error'))

    return render_template('index.html', summ_message='Please referesh the README to get the updates')


@app.route('/tools', methods=['POST'])
def agent_tool():
    global path
    tool_name = request.form.get('tool_name')  
    query = request.form.get('query')  

    try:
        import server
        tool_ = server.ReadmeGen()
        tool_response = tool_.agent_tool(
            readme_gen_pb2.UserQuery(tool_name=tool_name, query=query), path)
        
        if tool_response.message == 'success':
            if get_readme_last_modified() != os.path.getmtime(os.path.join(path, "README.md")):
                return send_file(os.path.join(path, "README.md"), as_attachment=True)
        else:
            return render_template('index.html', tool_message=tool_response.message)
    except Exception as e:
        app.logger.error(f"An unexpected error occurred: {str(e)}")
        flash(f"An unexpected error occurred: {str(e)}", 'error')
        return render_template('index.html', tool_message=(f"An unexpected error occurred: {str(e)}", 'error'))

    return render_template('index.html', tool_message='Please referesh the README to get the updates')

@app.route('/readme', methods=['GET', 'POST'])
def display_readme():
    global path
    try:
        if path:
            readme_path = os.path.join(path, "README.md")
            print('README path', readme_path)
            if os.path.exists(readme_path):
                with open(readme_path, 'r') as f:
                    readme_content = f.read()
                    readme_html = markdown2.markdown(readme_content) 
            return render_template('index.html', readme_content=readme_html)
    except Exception as e:
        app.logger.error(f"An unexpected error occurred: {str(e)}")
        flash(f"An unexpected error occurred: {str(e)}", 'error')

    return render_template('index.html', readme_message='Failed to read README')



@app.route('/rate_readme', methods=['POST'])
def rate_readme():
    global path
    
    try:
        data = request.json
        rating = data['rating']
        feedback = data.get('feedback', '')  # Get the feedback string, default to empty string if not provided
        readme_ratings.setdefault(local_directory, []).append({'rating': rating, 'feedback': feedback})
        print(readme_ratings)
        timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        try:
            ratings_filename = os.path.join(local_directory, "ratings.txt")
            with open(ratings_filename, 'a+') as file:
                print(f"Filename: {path}, Rating: {rating}, Feedback: {feedback}, Timestamp: {timestamp}\n")
                file.write(f"Filename: {path}, Rating: {rating}, Feedback: {feedback}, Timestamp: {timestamp}\n")
        except Exception as e:
            print("Error:", e)
        return jsonify({'success': True})
    except Exception as e:
        print("Exception:", e)
        return jsonify({'success': False, 'error': str(e)})



def app_run():
    app.run(host="0.0.0.0", port=8062)