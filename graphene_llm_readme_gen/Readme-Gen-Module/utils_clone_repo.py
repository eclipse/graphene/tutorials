import os
import git
import shutil
from utils_folder import remove_other_folders_files, delete_folders

def clone_repository(repository_url, subdirectory_path, local_directory):
    target_directory = os.path.join(local_directory, subdirectory_path.strip('/').split('/')[-1])

    try:
        if os.path.exists(os.path.join(local_directory, os.path.basename(subdirectory_path.rstrip('/')))):
            return f"Repository already cloned at {target_directory}", target_directory

        repo = git.Repo.clone_from(repository_url, target_directory)
        repo.git.checkout('main')

        remove_other_folders_files(target_directory, subdirectory_path)
        delete_folders(target_directory)

        if not os.listdir(target_directory):
            shutil.rmtree(target_directory)
            error_message = "Cloned repository is empty. No files found. \n Please check if it is a valid Graphene Tutorial."
            return error_message, None

        return f"Successfully cloned repository at {target_directory}", target_directory
        
    except Exception as e:
        error_message = f"Failed to clone repository. Error: {e}"
        return error_message, None
