import os
from langchain_community.document_loaders import UnstructuredMarkdownLoader
from langchain_community.callbacks import get_openai_callback
from langchain.chains.summarize import load_summarize_chain
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.prompts import PromptTemplate

from utils_folder import write_to_readme

def load_data_from_protobuf(proto_path):
    loader = UnstructuredMarkdownLoader(proto_path)
    return loader.load()

def run_summarization_chain(llm, data):
    summary_chain = load_summarize_chain(llm=llm)
    with get_openai_callback() as cb:
        result = summary_chain(data)
    return result["output_text"]

def summarize_protobuf_data(user_prompt, llm, proto_paths):
    protobuf_summary = []
    map_prompt_template = PromptTemplate(template="""Write a concise summary for the generated text. The text has the details of the\
                                         protbuf files. Please understand the text details and provide a summary "{text}" CONCISE SUMMARY:""", 
                                         input_variables=["text"])
    combine_prompt_template = PromptTemplate(template=f"{user_prompt.strip()}\n```{{text}}```\nBULLET POINT SUMMARY:", 
                                             input_variables=["text"])
    summary_chain = load_summarize_chain(llm=llm, chain_type='map_reduce', 
                                         map_prompt=map_prompt_template, 
                                         combine_prompt=combine_prompt_template)

    for proto_path in proto_paths:
        data = load_data_from_protobuf(proto_path)
        current_output_text = run_summarization_chain(llm, data)
        protobuf_summary.append(current_output_text)

    combined_protobuf_summary = ''.join(protobuf_summary)
    
    text_splitter = RecursiveCharacterTextSplitter(chunk_size=5000, chunk_overlap=50)
    chunks = text_splitter.create_documents([combined_protobuf_summary])

    with get_openai_callback():
        result = summary_chain(chunks)
    
    res_proto_summary_steps = result["output_text"]

    readme_path = os.path.join(os.path.sep.join(proto_paths[0].split(os.path.sep)[:-3]), 'README.md')
    readme_content = {
        "Protobuf files": combined_protobuf_summary,
        "Steps in client-server communication": res_proto_summary_steps,
    }
    
    write_to_readme(readme_path, readme_content)

    return combined_protobuf_summary, res_proto_summary_steps
