import os
import shutil
import fnmatch

def remove_other_folders_files(target_directory, subdirectory_path):
    for item in os.listdir(target_directory):
        item_path = os.path.join(target_directory, item)
        if item != subdirectory_path:
            if os.path.isfile(item_path):
                os.remove(item_path)
            elif os.path.isdir(item_path):
                shutil.rmtree(item_path)

def delete_folders(target_directory, folder_names=['static', 'templates']):
    for root, dirs, files in os.walk(target_directory):
        for folder in dirs:
            if folder in folder_names:
                folder_path = os.path.join(root, folder)
                shutil.rmtree(folder_path)


def find_files(directory, pattern):
    file_list = []
    for root, dirs, files in os.walk(directory):
        for filename in fnmatch.filter(files, pattern):
            file_list.append(os.path.join(root, filename))
    return file_list


def write_to_readme(readme_path, readme_content):
    if not os.path.exists(readme_path):
        with open(readme_path, 'w') as readme_file:
            pass  # Create an empty file if it doesn't exist
    
    with open(readme_path, 'r') as readme_file:
        existing_content = readme_file.read()

    with open(readme_path, 'w') as readme_file:
        for heading, content in readme_content.items():
            # Check if the heading exists in the existing content
            if f"## {heading}" in existing_content:
                # Find the start and end positions of the existing content under the heading
                start_pos = existing_content.index(f"## {heading}") + len(heading) + 3
                end_pos = existing_content.index('\n\n', start_pos)
                # Replace the existing content under the heading
                existing_content = existing_content[:start_pos] + '\n' + content + existing_content[end_pos:]
            else:
                # Append the heading and content if it doesn't exist
                readme_file.write(f"## {heading}\n{content}\n\n")
        
        # Write the updated content to the README file
        readme_file.write(existing_content)
