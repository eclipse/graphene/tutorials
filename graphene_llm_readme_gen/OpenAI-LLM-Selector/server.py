from concurrent import futures
import grpc
import openai_llms_pb2
import openai_llms_pb2_grpc
import threading
from app import app_run, get_parameters


class OpenAIService(openai_llms_pb2_grpc.OpenAIApiServiceServicer):
    def __init__(self):
        super().__init__()
        self.send_data = True

    def get_llm_parameters(self, request, context):
        try:
            params = get_parameters()
            if not params:
                print("Error: Parameters list is empty.")
                return openai_llms_pb2.ModelParameters()
            
            response = openai_llms_pb2.ModelParameters(
                api_key=params[0],
                org_id=params[1],
                selected_model=params[2],
                temperature=params[3],
                max_tokens=params[4],
                top_p=params[5],
                frequency_penalty=params[6],
                presence_penalty=params[7],
            )
       
            if not self.send_data:
                context.set_code(grpc.StatusCode.NOT_FOUND)
                context.set_details("all data has been processed")

            self.send_data = not self.send_data
            print('Received response \n Model: {}\n Temperature: {}\n Max Tokens: {}\n Top P: {}\n Frequency Penalty: {}\n Presence Penalty: {}'.format(response.selected_model,
                                                round(response.temperature, 2),
                                                response.max_tokens,
                                                round(response.top_p, 2),
                                                round(response.frequency_penalty, 2),
                                                round(response.presence_penalty, 2),
                                                ))
            return response
        except Exception as e:
            print(e)
            return None


def serve(port):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    openai_llms_pb2_grpc.add_OpenAIApiServiceServicer_to_server(OpenAIService(), server)
    server.add_insecure_port("[::]:{}".format(port))
    print("Starting server. Listening on port : " + str(port))
    server.start()
    threading.Thread(target=app_run()).start()
    server.wait_for_termination()


if __name__ == "__main__":
    port = 8061
    serve(port)
