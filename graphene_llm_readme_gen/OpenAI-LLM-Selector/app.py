from flask import Flask, render_template, request, session, redirect, url_for
from flask_wtf import FlaskForm
from flask_bootstrap import Bootstrap

from wtforms.validators import DataRequired
from wtforms import StringField, FloatField, IntegerField, SubmitField

from openai import OpenAI

app = Flask(__name__)

parameters = []

class ApiForm(FlaskForm):
    selected_model = StringField('Selected Model', validators=[DataRequired()])
    temperature = FloatField('Temperature', validators=[DataRequired()])
    max_tokens = IntegerField('Max Tokens', validators=[DataRequired()])
    top_p = FloatField('Top P', validators=[DataRequired()])
    frequency_penalty = FloatField('Frequency Penalty', validators=[DataRequired()])
    presence_penalty = FloatField('Presence Penalty', validators=[DataRequired()])
    submit_button = SubmitField('Submit')

def get_model_names(api_key=None, org_id=None):
    try:
        if api_key is None:
            api_key = session.get('api_key')
        if org_id is None:
            org_id = session.get('org_id')
        if api_key and org_id:
            client = OpenAI(organization=org_id, api_key=api_key)
            model_names_metadata = client.models.list()
            model_names = [model.id for model in model_names_metadata.data]
            return model_names
        else:
            return []
    except Exception as e:
        print(f"Error fetching model names: {e}")
        return []

@app.route('/', methods=['GET', 'POST'])
def index():
    error_message = None
    if request.method == 'POST':
        # Store keys in session
        session['api_key'] = request.form['api_key']
        session['org_id'] = request.form['org_id']
        model_names = get_model_names(session['api_key'], session['org_id'])
        if model_names:
            return redirect(url_for('select_model'))
        else:
            error_message = "Failed to retrieve model names. Please check your API key and organization ID."
            print(error_message)
            return render_template('index.html', error_message=error_message)
    return render_template('index.html')

@app.route('/select_model', methods=['GET', 'POST'])
def select_model():
    model_form = ApiForm()
    if request.method == 'POST':
        if 'clear_session' in request.form:
            session.pop('api_key', None)
            session.pop('org_id', None)
            return redirect(url_for('index'))
        else:
            parameters.clear()
            parameters.append(session['api_key'])
            parameters.append(session['org_id'])
            parameters.append(request.form['selected_model'])
            parameters.append(round(model_form.temperature.data, 2))
            parameters.append(model_form.max_tokens.data)
            parameters.append(round(model_form.top_p.data, 2))
            parameters.append(round(model_form.frequency_penalty.data, 2))
            parameters.append(round(model_form.presence_penalty.data, 2))
            
    model_names = get_model_names()
    return render_template('index.html', model_form=model_form, model_names=model_names)

def get_parameters():
    return parameters

def app_run():
    app.secret_key = "openai_llms"
    bootstrap = Bootstrap(app)
    app.run(host="0.0.0.0", port=8062, debug=False)
