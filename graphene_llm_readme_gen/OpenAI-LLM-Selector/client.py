import grpc
import logging

import openai_llms_pb2_grpc
import openai_llms_pb2
    
port = 8061

def run():
    print("Calling OpenAI_LLM_Service_Stub..")
    try:
        with grpc.insecure_channel('localhost:{}'.format(port)) as channel:
            stub = openai_llms_pb2_grpc.OpenAIApiServiceStub(channel)
            ui_request = openai_llms_pb2.Empty()
            response = stub.get_llm_parameters(ui_request)
            print(response)
    except Exception as e:
        print(e)

    
if __name__ == '__main__':
    logging.basicConfig()
    run()
