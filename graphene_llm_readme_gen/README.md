# Interactive Graphene Documentation

The primary goal of the pipeline is to create a user-friendly interface connecting the Large Language Models (LLMs) and generate a comprehensive document (README.md) for various tutorials in the Graphene platform. Additionally, it strives to explore and integrate frameworks such as LangChain to access the seamless pipeline deployment. This sample pipeline offers insights into the interplay between different components within an LLM pipeline.

Three containers are utilized within the Graphene Design Studio framework to achieve this.

![SVG Image](ReadmeGen_pipeline.svg)

| Containers |  
| ---------- | 
| **container_openai_llm_selector** : The OpenAI LLM Selector module hosts the OpenAI APIs, serving as the gateway for LLM model selection on the Graphene platform. It encapsulates the necessary API parameters, allowing users to select by inputting their chosen parameter values. Upon successful authentication using their API key, users can navigate the parameter selection process and tailor the model's output to their application requirements.These parameters include model_name, temperature, max_tokens, top_p, frequency_penalty and presence_penalty. Please refer to the following website [https://platform.openai.com/docs/api-reference/introduction] to better understand the various terms used here.  | 
| **container_readme_gen_module** : The Readme-Gen module dynamically produces README files tailored to the selected Graphene tutorials using the Large Language Models (LLMs). It utilizes the LangChain Expression Language (LCEL) for summarization and exhibits an array of Agents and Tools within LangChain to generate README.md files specific to the chosen tutorial. This module facilitates the interactive generation of a README file for a Graphene tutorial using the LLMs. Users can input prompts to obtain summaries of protobuf files and refine prompts as needed for better results. Additionally, the module utilizes four tools powered by LangChain - WikiAgentTool, RepoStructureTool, UMLAgentTool, and DockerCommandsTool - to extract specific sections of the README based on user queries. This interactive process empowers users to effectively leverage the LLMs, prompts, and queries to create a comprehensive README for the Graphene tutorial. | 
| **container_user_feedback_diagnostics** : The User-Feedback-Diagnostics module oversees user ratings and feedback on README files. It plots each tutorial's average ratings and feedback scores, helping gauge user satisfaction. Ratings range from 1 to 5, reflecting satisfaction with content or the LLM's effectiveness. It also calculates sentiment polarity scores using TextBlob. This data aids in updating and enhancing tutorials, ensuring continual improvement of the LLM pipeline.| 


## Work in Progress
Please refer to the following ticket (eclipse/graphene/tutorials#23) for a clearer understanding of the pipeline. 
For a btter understanding of the metrics, please refer to the following ticket (eclipse/graphene/tutorials#39)
This tutorial is currently in the developmental stages and may undergo frequent changes. Contributions, suggestions, and feedback are welcome to help improve this project.
