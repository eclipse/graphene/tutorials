# Graphene Container Format

## Table of Contents

### [1. Introduction](#1-introduction)
### [2. Define model.proto](#2-define-modelproto)
### [3. Create the gRPC docker container](#3-create-the-grpc-docker-container)
### [4. Status and Error Codes](#4-status-and-error-codes)
### [5. Onboarding](#5-onboarding)
### [6. First Node Parameters (e.g. for Databrokers)](#6-first-node-parameters-eg-for-databrokers)
### [7. Scalability, GPU Support and Training](#7-scalability-gpu-support-and-training)
### [8. Shared Folders for Pipeline Nodes](#8-shared-folders-for-pipeline-nodes)
### [9. Streaming Support for gRPC Services](#9-streaming-support-for-grpc-services)
### [10. Metrics Aggregation](#8-metrics-aggregation)

<hr style="border:1px solid #000">

### [1. Introduction](#1-introduction)

This document specifies the docker container format for tools and models that can be onboarded on Graphene instances so they can be used in the visual composition editor as re-usable, highly interoperable building blocks for AI pipelines.

![image](src/images/intro_pipeline.PNG)

In short, the container should define its public service methods using protobuf v3 and expose these methods via gRPC. All these technologies are open source and freely available. Here are the respective home pages for documentation and reference:

https://docs.docker.com/reference/  
https://developers.google.com/protocol-buffers/docs/overview  
https://developers.google.com/protocol-buffers/docs/proto3  
https://www.grpc.io/docs/  

Because the goal is to have re-usable building blocks to compose pipelines, the main reason to choose the above technology stack is to achieve the highest level of interoperability:

- Docker is today the defacto standard for server side software distribution including all dependencies. It is possible to onboard containers for different architectures (x86_64, GPU, ARM, HPC/Singularity)

- gRPC together with protobuf is a proven specification and implementation for remote procedure calls supporting a broad range of programming languages and it is optimized for performance and high throughput.

**Please note that the tools and models are not limited to deep learning models.** Any AI tool from any AI area like reasoning, semantic web, symbolic AI and of course deep learning can be used for piplines as long as it exposes a set of public methods via gRPC.

### [2. Define model.proto](#2-define-modelproto)

The public service methods should be defined in a file called model.proto:
- It should be self contained, thus contain the service definitions with all input and output data structures and no imports can be used

- A container can define serveral rpc methods, but all in one .proto file and in the same service { } block

- Package declarations in the context of pipelines can cause scoping issues if a message in different protobufs belongs to different packages


```proto
//Define the used version of proto
syntax = "proto3";

//Define a message to hold the features input by the client
message Features {
    float MSSubClass     = 1 ;
    float LotArea        = 2 ;
    float YearBuilt      = 3 ;
    float BedroomAbvGr   = 4 ;
    float TotRmsAbvGrd   = 5 ;
}
//Define a message to hold the predicted price
message Prediction {
    float salePrice      = 1 ;
}

//Define the service
service Predict {
    rpc predict_sale_price(Features) returns (Prediction);
}
```

**Important:** The parser for .proto-files inside AI4EU Experiments is much less flexible than the original protobuf compiler, so here are some rules. If the rules are not followed, it prohibits the model from being usable inside the visual editor AcuCompose. **Moreover, the enum keyword is not yet supported!**

![image](src/images/table_of_contents.PNG)

### [3. Create the gRPC docker container](#3-create-the-grpc-docker-container)
Based on model.proto, you can generate the necessary gRPC stubs and skeletons for the programming language of your choice using the protobuf compiler **protoc** and the respective protoc-plugins. Then create a short main executable that will read and initialize the model or tool and starts the gRPC server. This executable will be the entrypoint for the docker container.  

**The gPRC server must listen on port 8061.**  

If the model also exposes a **Web-UI** for human interaction, which is optional,  it must listen on **port 8062**.  

The filetree of the docker container should look like below. In the top level folder of the container should be the files.

- model.proto
- license.json

And also the folders for the microservice like app and data, or any supplementary folders: 

![image](src/images/docker_container.PNG)

The license file is not mandatory and can be generated after onboarding with the License Profile Editor in the AI4EU Experiments Web-UI:  

https://docs.acumos.org/en/clio/submodules/license-manager/docs/user-guide-license-profile-editor.html 

There are several detailed tutorials on how to create a dockerized model in this
repository: https://github.com/ai4eu/tutorials


#### [4. Status and Error Codes](#4-status-and-error-codes)
The models should use gRPC status codes according to the spec:
https://grpc.github.io/grpc/core/md_doc_statuscodes.html  

For example if no more data is available, the model should return status    
`5 (NOT_FOUND) or 11 (OUT_OF_RANGE)`.

### [5. Onboarding](#5-onboarding)

The final step is to onboard the model. There are several ways to onboard a model into
AI4EU Experiments but currently the only recommended way is to use **“On-boarding
dockerized model URI”:**


1. Upload your docker container to a public registry like Docker Hub
1. Start the onboarding process like in the screenshot below
1. Upload the protobuf file
1. Add license profile

![image](src/images/model_onboarding.PNG)

#### [6. First Node Parameters (e.g. for Databrokers)](#6-first-node-parameters-eg-for-databrokers)

Generally speaking, the orchestrator dispatches the output of the previous node to the following node. A special case is the first node, where obviously no output from the previous node exists. In order to be able to implement a general orchestrator, the first node must define its services with an Empty message type. Typically this concerns nodes of type Databroker as the usual starting point of a pipeline.

**Important:** If there is more than one node with an empty message at the beginning of the pipeline, ALL these nodes will be called by the orchestrator. 


```proto
syntax = "proto3";

message Empty {
}

message NewsText {
    string text = 1;
}

service NewsDatabroker {
    rpc pullData(Empty) returns(NewsText);
}
```
To indicate the end of data, a Databroker should return status code 5 or 11 (see chapter 4)


**Leveraging Databroker for enhanced metadata with dataset's attributes**  

The dataset features can be incorporated into the Databroker. The information about the dataset type, dataset name, associated description, size, DOI ID, etc., may be found in the logs generated out of this node.

When the Pipeline is launched, these initial logs are recorded. The implemented method reads and finds the logs with the meta key - **dataset_features**. The extracted logs are then added to the metadata file (execution_run.json ) as a Python dictionary. 

A sample representation of the logs for the news_training Pipeline is as follows,

`INFO:root:{'dataset_features': {'type': 'aiod-dataset/v1(TensorFlow Dataset(tfds)','datasetname': 'The Reuters Dataset', 'description': 'http://kdd.ics.uci.edu/databases/reuters21578/README.txt', 'size': '4MB', 'DOI_ID': 'Not available'}}`  

A sample representation of the output in the metadata file is as shown below,


```json
"nodes": [
        {
            "proto_uri": "org/acumos/ce1d1c94-4465-4c35-95ff-56e087aed9fd/databrokerdatasetfeatures3/1.0.0/databrokerdatasetfeatures3-1.0.0.proto",
            "image": "cicd.ai4eu-dev.eu:7444/training_pipeline/news_databroker:v2",
            "node_type": "MLModel",
            "container_name": "databrokerdatasetfeatures31",
            "operation_signature_list": [
                {
                 
                }
            ],
            "checksum": "docker-pullable://cicd.ai4eu-dev.eu:7444/training_pipeline/news_databroker@sha256:0ff4184389e7768cbf7bb8c989e91aa17e51b5cb39846a700868320386131dee",
            "dataset_features": {
                "type": "aiod-dataset/v1",
                "datasetname": "The Reuters Dataset, TensorFlow Dataset(tfds)",
                "description": "http://kdd.ics.uci.edu/databases/reuters21578/README.txt",
                "size": "4MB",
                "DOI_ID": "Not available"
            }
        },
```

The added dataset's features are shown along with the other information in the databroker's node.
In this way, expanding the container specification for the databroker's data input and output is possible. 

Note : 

1. Please be aware that the logging should be enabled in the python script (`logging.basicConfig(level=logging.INFO`)). For instance, we need to enable logging in the script containing the function/method that reads the dataset metadata.

1. Please refer the [10. Metrics Aggregation](#8-metrics-aggregation) section to understand the changes implemented in the playground-app.

<details>
<summary>Additional Information</summary>
The get_dataset_metadata() method in the databroker script can be used to accomplish the method described above. A file called dataset_features.txt may be present in each Databroker. Currently, we ensure that it complies with the message fields in the databroker.proto file. This method reads data from a text file and then sends the log data appropriately to be read by the orchestrator. To follow this example, please refer to the news-training pipeline tutorial.

```proto
syntax = "proto3";

message Empty {
}
......

message DatasetFeatues{
  string type = 1
  string datasetname = 2;
  string description = 3;
  string size = 4;
  string DOI_ID = 5;
}

service NewsDatabroker {
  ......
  rpc get_dataset_metadata(Empty) returns(DatasetFeatues);
}
```
Note : Though this functionality (get_dataset_metadata()) can be implemented independent of the grpc communication, as a future scope, it is currently implemented as a part of the Databroker's protobuf definition.
</details>

### [7. Scalability, GPU Support and Training](#7-scalability-gpu-support-and-training)

The potential execution environments range from Minikube on a Laptop over small
Kubenetes clusters to big Kubernetes clusters and even HPC and optional GPU
acceleration. **It is possible to support all those environments with a single
container image**
 taking into account some recommendations:


- Let the model be flexible with memory usage: use more memory only if
available

- Let the model be scalable if more cpu cores are available (allow for concurrency):  
https://en.wikipedia.org/wiki/Concurrency_(computer_science)

- Some AI frameworks like PyTorch or Tensorflow can be used in a way to work
with or without GPU with the same code. Here is an example with PyTorch:  
https://stackoverflow.com/a/56975325


- Even training is possible, if the model exposes the corresponding methods in
the protobuf interface

### [8. Shared Folders for Pipeline Nodes](#8-shared-folders-for-pipeline-nodes)


To be compatible with the upcoming shared folder concept of AI4EU Experiments,
please use an environment variable to pass the absolute path of the shared folder to
the processes running inside the container: so it should be the path from the inside
the container point of view. Let's assume the shared folder is mapped as
"/data/shared" into the container, then the container could be started like

`docker run --env SHARED_FOLDER_PATH=/data/shared ...`

How this shared folder is actually set up, depends your container runtime and is
different for docker, docker-compose or kubernetes. For AI4EU Experiments, the
kubernetes deployment client will take care of it.

### [9. Streaming Support for gRPC Services](#9-streaming-support-for-grpc-services)

The “stream” keyword is supported for both RPC input and output.  

That means a RPC can have the following forms, assuming Input and Output are Protobuf message types:

- Each RPC call consumes one input message and returns one output message:
`rpc call(Input) returning (Output);`

- Each RPC call gets at least one input message, decides by itself when to stop consuming the input stream, after which it returns a single output message:
`rpc call(stream Input) returning (Output);`

- Each RPC call gets at one input message and decides by itself how many output messages to produce before closing the stream:  
`rpc call(Input) returning (stream Output);`

- Each RPC call gets at least one input message, decides by itself how many output messages to produce and when to stop consuming input before closing input and output streams:  
`rpc call(stream Input) returning (stream Output);`

Special cases of the above are the following, where Empty is a message type without
any fields.
- RPC is called immediately when orchestration starts and is restarted whenever
it closes the output stream:  
`rpc call(Empty) returning (stream Output);`

    This pattern is useful for GUIs, with one RPC call for each type of User Event
    that should trigger a computation in other components.

    This pattern is useful for sensors, with one RPC call for each type of sensor
    reading that should trigger a computation in other components.

-  RPC is consuming messages only:  
`rpc call(stream Input) returning (Empty);`  
This pattern is useful for GUIs, with one RPC call for each type of input to
display.

Some notes about how streaming works:

- Connecting “stream” outputs with “non-stream” inputs and vice versa is
allowed.
- Orchestrator creates a Queue for each connection between two ports.
- Orchestrator creates a Thread for each RPC call. Essentially orchestration is
parallel.
- A RPC is started as soon as there is a message in the input queue, or
immediately for the message called Empty.
- Cycles among components are possible.

    For example a cycle from a GUI that returns User Events as streaming output to
    a computation component with a simple non-streaming RPC which feeds back
    into the GUI into a RPC that displays the result.


GUI RPC1 (Empty input, stream output)->Computation RPC->GUI RPC2 (stream input)

The Sudoku Tutorial streaming branch contains such a GUI (a webinterface), see

https://github.com/peschue/ai4eu-sudoku/blob/streaming/gui/sudoku-gui.proto

### [10. Metrics Aggregation](#8-metrics-aggregation)

We can determine information about the model's performance by using a variety of metrics, including classification, regression, clustering, anomaly detection, ranking, GNN-specific, custom metrics, etc. Obtaining and updating these metrics in the metadata is paramount to understanding the need to fine-tune the model or improve the outcomes.
As a result, by including the metrics from the nodes/containers they are contained in, the created metadata file, execution-run.json, may be constructed more concisely. 

The model provider should ensure the following additions in order to accomplish the acquisition of the metrics,

1. Have message fields for the metrics defined in the protobuf definitions.

1. Adding an initialization flag (`has_metrics = True`) indicating the presence of metrics in the particular node/module and logging a message (`logging.info('MetricsAvailable')`) accordingly. In doing so, we only output a list of nodes containing the metrics when the Pipeline is launched. 

1. Have the metrics collected and updated after the training process.
For this purpose, a gRPC routine/method,     `get_metrics_metadata(self, request, context)`, is subsequently called after the training process concludes.

1. The Python logging should be enabled in the script that contains metrics aggregation in order to see all the logs that are obtained.  

Please refer to the Additional Information section to further comprehend the topics mentioned above.

**Enhancing the playground-app repository with metadata functionality**

When the Pipeline is started, the initial logs indicated in 2. are captured, allowing merely the nodes with metrics to be retrieved. When the Pipeline is executed, these nodes are further checked for metakey - metrics. Typically, these log entries will be at the end. A Python dictionary containing the retrieved logs is then appended to the metadata file (execution_run.json).

A sample representation of the metrics logs for the news_training pipeline is as follows,

`INFO:root:{'metrics': {'date_time': '2023-09-15 08:17:22', 'type': 'classification-metrics/v1', 'more-is-better': {'accuracy': 0.9247897267341614}, 'less-is-better': {'validation_loss': 0.9067514538764954}, 'status_text': 'success'}}`

A sample representation of the output in the metadata file is as shown below,

```json
{
            "proto_uri": "org/acumos/63955d9a-f665-41a8-9fb6-605a118ea7b4/classifier34/1.0.0/classifier34-1.0.0.proto",
            "image": "cicd.ai4eu-dev.eu:7444/training_pipeline/news_classifier:v3",
            "node_type": "MLModel",
            "container_name": "classifier342",
            "operation_signature_list": [
                {
                    "connected_to": [],
                    "operation_signature": {
                        "operation_name": "startTraining",
                        "output_message_name": "TrainingStatus",
                        "input_message_name": "TrainingConfig",
                        "output_message_stream": false,
                        "input_message_stream": false
                    }
                }
            ],
            "checksum": "docker-pullable://cicd.ai4eu-dev.eu:7444/training_pipeline/news_classifier@sha256:3b4c88e571abbb0e536d8048ffb82ae4126d52dafa1e03eb13b6d8c22bf3e859",
            "metrics": {
                "date_time": "2023-09-12 06:00:09",
                "type": "classification-metrics/v1",
                "more_is_better": {
                    "accuracy": 0.9239237904548645
                },
                "less_is_better": {
                    "validation_loss": 0.9049152731895447
                }
                "status_text": "success"
            }
        }
```
<details>
<summary>Appendix : Information on metadata </summary>

In summary, we extract metadata such as dataset features and model metrics. This part of the section talks about the different types of model metrics and dataset features. 

**Model Metrics:**
The models in the playground can be broadly classified into two different types: Regression and Classification. Metrics are used for evaluating the performance of your own model and make improvements to get the desired result. 

Classification Model:

<div align="center">
<table >
    <thead>
        <tr>
            <th align="center">S.no</th>
            <th align="center"> Metrics</th>
            <th align="center">Functionality</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td align="left">1</td>
            <td align="left">Accuracy</td>
            <td align="left">Accuracy is a metric that measures the percentage of correctly predicted instances in your dataset. It's a straightforward way to evaluate how well your model is performing in terms of getting predictions right.</td>
        </tr>
        <tr>
            <td align="left">2</td>
            <td align="left">validation_loss</td>
            <td align="left">Validation loss is a metric that quantifies how well your machine learning model fits the training data.</td>
        </tr>        
    </tbody>
</table>
<p></p>
</div>


A sample:
```json
    "metrics": {
            "date_time": "2023-09-15 08:17:22", 
            "type": "classification-metrics/v1",
            "more_is_better": { 
                "accuracy": 0.9247897267341614
            },
            "less_is_better": { 
                "validation_loss": 0.9067514538764954
            } 
            "status_text": "success"
            }
```


In conclusion, accuracy can be used as a guiding metric during model development and hyperparameter tuning. validation_loss is typically used during the training process to monitor whether the model is improving or overfitting. A decreasing validation loss indicates that the model is learning and improving its ability to make predictions. However, if the validation loss starts increasing, it may be a sign of overfitting.

Regressor model:


<div align="center">
<table >
    <thead>
        <tr>
            <th align="center">S.no</th>
            <th align="center"> Metrics</th>
            <th align="center">Functionality</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td align="left">1</td>
            <td align="left">Root Mean Square Error</td>
            <td align="left">RMSE is a measure of the average deviation (error) between predicted values and actual values in a regression model. RMSE provides a sense of how much the model's predictions deviate from the true values, with lower RMSE values indicating better model performance.</td>
        </tr>
        <tr>
            <td align="left">2</td>
            <td align="left">Mean Squared Error</td>
            <td align="left">MSE is another metric that quantifies the average squared difference between predicted values and actual values. Unlike RMSE, MSE does not take the square root, so it is sensitive to large errors.</td>
        </tr>
        <tr>
            <td align="left">3</td>
            <td align="left">R2 Score</td>
            <td align="left">R2 (R-squared) is a metric that represents the proportion of the variance in the dependent variable (target) that is explained by the independent variables (features) in a regression model. It ranges between 0 and 1, with higher values indicating a better fit of the model to the data. A value of 1 indicates a perfect fit.</td>
        </tr>
        <tr>
            <td align="left">4</td>
            <td align="left">Adjusted R2 Score</td>
            <td align="left">Adjusted R2 is a modification of the R2 score that accounts for the number of independent variables (features) in the model.It penalizes the inclusion of irrelevant features and generally provides a more accurate assessment of model performance, especially in multiple regression</td>
        </tr>        
    </tbody>
</table>
<p></p>
</div>


A sample:
```json
  "metrics": {
                "date_time": "2023-09-28 13:56:21",
                "type": "regression-metrics/v1",
                "less_is_better": {
                    "mse": 0.0025680503998073007,
                    "rmse": 0.05067593511527242
                },
                "more_is_better": {
                    "r_squared": 0.831178119365146,
                    "adjusted_r_squared": 0.8270170166734419
                }
                "status_text": "success"
            }
```

In conclusion, RMSE and MSE measure the accuracy of predictions, R2 quantifies the goodness of fit, and Adjusted R2 adjusts R2 for the complexity of the model. These metrics are crucial for evaluating regression models and selecting <br /><br />
Note : Another field in the metadata is 'type,' which represents the dataset and model's metrics type. This field is critical not only for model evaluation but also for the model's performance to the precise goals and requirements of the application. Adding this feature allows one to group the metrics into a primary group or sub-group of metrics. Determining the dataset type aids in discerning the source of the dataset. 

**Dataset Features:**

<div align="center">
<table >
    <thead>
        <tr>
            <th align="center">S.no</th>
            <th align="center">Features</th>
            <th align="center">Functionality</th>
        </tr>
    </thead>
    <tbody>
       <tr>
            <td align="left">1</td>
            <td align="left">Type</td>
            <td align="left">Specifies the type or category information that the dataset belongs to.</td>
        </tr>
        <tr>
            <td align="left">2</td>
            <td align="left">Dataset name</td>
            <td align="left">This is the name or label given to a specific dataset, often indicating its content, source, or purpose.</td>
        </tr>
        <tr>
            <td align="left">3</td>
            <td align="left">Description</td>
            <td align="left">A brief or detailed explanation of what the dataset contains, its origin, format, and any other relevant information for potential users.</td>
        </tr>
        <tr>
            <td align="left">4</td>
            <td align="left">Size</td>
            <td align="left">The size of the dataset, typically measured in terms of the number of records, rows, columns, or the total file size in bytes or other appropriate units.</td>
        </tr>
        <tr>
            <td align="left">5</td>
            <td align="left">DOI (Digital Object Identifier) or ID</td>
            <td align="left">A unique and persistent identifier assigned to the dataset, often used for citation and reference purposes, ensuring its accessibility and traceability.</td>
        </tr>        
    </tbody>
</table>
<p></p>
</div>



A sample:
```json
   "dataset_features": {
                "type": "aiod-dataset/v1(Kaggle dataset)",
                "datasetname": "House Prices dataset",
                "description": "https://www.kaggle.com/datasets/lespin/house-prices-dataset",
                "size": "204 kB",
                "DOI_ID": "Not available"
            }
```

In conclusion, dataset features helps to get an overview about the dataset that the model use.
</details>

<details>

<summary>Additional Information - Future scope for the 'type' of metrics</summary>
Further, we can categorize the type of metrics into training and testing phases. This can help us assess the model performance by detecting the ML model's over/underfitting, drift, and generalization parameters. We can thus make informed preferences about the model selection, hyperparameter tuning decisions, and monitor model performance in our intended applications. The categorization of the 'type' parameter into training and testing phases can be seamlessly introduced as a future extension to the metrics metadata for comprehensive metrics tracking.<br /><br />
A sample extension of the metadata for the metrics into training and testing phases using the "type" parameter is shown below. <br />
<br />

```json
{
    "metrics": [
        {
            "type": "classification-training-metrics/v1",
            "date_time": "2023-09-07 07:28:51",
            "more_is_better": {
                "training_accuracy": 0.9257,
                "validation_accuracy": 0.8051
            },
            "less_is_better": {
                "training_loss": 0.3606,
                "validation_loss": 0.8978
            },
            "status_text": "success"
        },
        {
            "type": "classification-testing-metrics/v1",
            "date_time": "2023-09-07 07:28:51",
            "more_is_better": {
                "F1 Score": 0.8512,
                "Specificity": 0.9036,
                "ROC-AUC": 0.9205
            }
            "status_text": "success"
        }
    ]
}

```
</details>

<details>
<summary>Additional Information </summary>

Please refer to the news-training pipeline/house price prediction tutorials to understand the metrics aggregation.

##### **Changes in tutorials - news_training**

1. Initialization Flag : (has_metrics):\
   We add a flag to indicate the presence of metrics in the particular node/module and print a message accordingly. In doing so, we only output a list of nodes containing the metrics when the pipeline is started.

```python
class NewsClassifier(news_classifier_pb2_grpc.NewsClassifierServicer):

    def __init__(self):
        self.has_metrics = True # Flag to indicate the presence of metrics in this node and print a message accordingly.
        if self.has_metrics:
            logging.info('MetricsAvailable')
```

2. Next, we add the following gRPC routine to the news_classifier.proto. This method is called after the training process concludes and the metrics have been recorded.

- For instance,

```proto
syntax = "proto3";

message TrainingConfig {
  .....
}

message TrainingStatus {
  string type = 1;
  double accuracy = 2;
  double validation_loss = 3;
  string status_text = 4;
}
....

service NewsClassifier {
  .....
  rpc get_metrics_metadata(TrainingStatus) returns(TrainingStatus);
}
```

3.   Please add the function definition (`get_metrics_metadata(...)`) in the respective script that collects the final metrics. Here in the news-training pipeline, we collect the metrics after the training process, and the response is further sent to the method (`get_metrics_metadata(...)`) to display the metrics in a log format.

Please adapt all the above changes to the respective nodes to achieve the metrics aggregation in the ML model or tutorial example.

##### **Changes in playground-app - Already available**

Changes in Pipeline.py, NodeManager.py and ExecutionRun.py

Please refer to the issues eclipse/graphene/playground-app#34, eclipse/graphene/tutorials#19 to understand the implementation. 

> Pipeline.py\
> `_get_starting_nodes(path)` 
> This function scans for the entry nodes with 'Empty' as the input message and returns a list of them.
>
> `_get_metadata()` 
> This function retrieves the metadata information, such as the dataset features and metrics.
>
> NodeManager.py\
> `_get_dataset_features(entry_nodes)` 
> This function checks if the list of starting nodes is available in existing pods. Further, it reads and finds the logs with the meta key {dataset_features} from the starting nodes and returns the dataset features dictionary.
>
> `_get_metrics_metadata()` 
> This function checks for the nodes containing the metrics. Further, it finds the logs with the meta key {metrics} from the such nodes and returns the metrics dictionary.
>
> ExecutionRun.py\
>`add_dataset_features(feature_dict, start_node)`:
> This function adds the dictionary containing dataset 
> features obtained after running the pipeline to the 
> execution-run.json.
>
> `add_metrics_features(metrics_results)` 
> This function adds the dictionary containing metrics from various nodes obtained after running the pipeline to the execution-run.json.
</details>
