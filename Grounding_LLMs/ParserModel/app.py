from flask import Flask, render_template, request, flash
import grpc
import GLLM_parser_pb2
import GLLM_parser_pb2_grpc
import os
import logging


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
app = Flask(__name__)


@app.route("/", methods=['GET', 'POST'])
def index():
    results = []
    with open("results.txt", mode="r") as f:
        content = ""
        for line in f.readlines():
            content += line.strip() + ' ---- '  # Concatenate lines with hyphen

        # Split the content into two items based on '|'
        results = content.split('|')
        results = [item.strip(' ---- ') for item in results]  # Remove trailing hyphens

    try:
        structured_data=results[-2]
        KGmoduleStatus=results[-1]
    except:
        structured_data=results[:-1]
        KGmoduleStatus=results[-1]

    
    return render_template('index.html', structured_data = structured_data, KGmoduleStatus=KGmoduleStatus)


def app_run():
    app.secret_key = 'glmm' 
    app.run(host="0.0.0.0", port=8062, debug = True)


