# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: GLLM_parser.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='GLLM_parser.proto',
  package='',
  syntax='proto3',
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_pb=b'\n\x11GLLM_parser.proto\"`\n\nUserInputs\x12\x17\n\x0forganization_id\x18\x01 \x01(\t\x12\x0f\n\x07\x61pi_key\x18\x02 \x01(\t\x12\x12\n\nuser_query\x18\x03 \x01(\t\x12\x14\n\x0cusecase_data\x18\x04 \x01(\t\";\n\nResultText\x12\x15\n\rparsed_output\x18\x01 \x01(\t\x12\x16\n\x0eKGmoduleStatus\x18\x02 \x01(\t25\n\x06Parser\x12+\n\x0fget_parse_model\x12\x0b.UserInputs\x1a\x0b.ResultTextb\x06proto3'
)




_USERINPUTS = _descriptor.Descriptor(
  name='UserInputs',
  full_name='UserInputs',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='organization_id', full_name='UserInputs.organization_id', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='api_key', full_name='UserInputs.api_key', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='user_query', full_name='UserInputs.user_query', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='usecase_data', full_name='UserInputs.usecase_data', index=3,
      number=4, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=21,
  serialized_end=117,
)


_RESULTTEXT = _descriptor.Descriptor(
  name='ResultText',
  full_name='ResultText',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='parsed_output', full_name='ResultText.parsed_output', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='KGmoduleStatus', full_name='ResultText.KGmoduleStatus', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=119,
  serialized_end=178,
)

DESCRIPTOR.message_types_by_name['UserInputs'] = _USERINPUTS
DESCRIPTOR.message_types_by_name['ResultText'] = _RESULTTEXT
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

UserInputs = _reflection.GeneratedProtocolMessageType('UserInputs', (_message.Message,), {
  'DESCRIPTOR' : _USERINPUTS,
  '__module__' : 'GLLM_parser_pb2'
  # @@protoc_insertion_point(class_scope:UserInputs)
  })
_sym_db.RegisterMessage(UserInputs)

ResultText = _reflection.GeneratedProtocolMessageType('ResultText', (_message.Message,), {
  'DESCRIPTOR' : _RESULTTEXT,
  '__module__' : 'GLLM_parser_pb2'
  # @@protoc_insertion_point(class_scope:ResultText)
  })
_sym_db.RegisterMessage(ResultText)



_PARSER = _descriptor.ServiceDescriptor(
  name='Parser',
  full_name='Parser',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_start=180,
  serialized_end=233,
  methods=[
  _descriptor.MethodDescriptor(
    name='get_parse_model',
    full_name='Parser.get_parse_model',
    index=0,
    containing_service=None,
    input_type=_USERINPUTS,
    output_type=_RESULTTEXT,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
])
_sym_db.RegisterServiceDescriptor(_PARSER)

DESCRIPTOR.services_by_name['Parser'] = _PARSER

# @@protoc_insertion_point(module_scope)
