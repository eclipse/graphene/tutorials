import grpc
from timeit import default_timer as timer
import logging

# import the generated classes

import GLLM_parser_pb2_grpc
import GLLM_parser_pb2

port = 8061


def run():
    print("Calling GLLM_Stub..")
    with grpc.insecure_channel('localhost:{}'.format(port)) as channel:
        stub = GLLM_parser_pb2_grpc.ParserStub(channel)
        ui_request = GLLM_parser_pb2.UserInputs(
                    organization_id="", # enter your org id
                    api_key="",# enter your openai api key
                    user_query='your_user_query',
                    usecase_data = "DLE stands for Dry Low Emissions and its the aero-derivative version of DLN (Dry Low NOx). It includes additional fuel control valves and I think the combustors are multi-zone at least they were in the early days of development. SAC stands for Single Annular Combustor and it's usually a simple single annular combustor with one combustion zone. To achieve low NOx emissions it's usually necessary to use water or steam injection which adds somewhat to the complexity of the auxiliaries. Gas turbines with DLE technology were developed to achieve lower emissions without using water or steam to reduce combustion temperature Wet Low Emission technology. A DLE combustor uses the principle of lean premixed combustion and is similar to the Single Annular Combustor or SAC with some exceptions. A DLE combustor takes up more space than a SAC turbine and if the turbine is changed it can not be connected directly to existing equipment without considerable changes in the positioning of the equipment.The SAC turbine has one single concentric ring where the DLE turbine has two or three rings with premixers depending on gas turbine type."
                    #usecase_data="There are many fruits that were found on the recently discovered planet Goocrux. There are neoskizzles that grow there, which are purple and taste like candy. There are also loheckles, which are a grayish blue fruit and are very tart, a little bit like a lemon. Pounits are a bright green color and are more savory than sweet. There are also plenty of loopnovas which are a neon pink flavor and taste like cotton candy. Finally, there are fruits called glowls, which have a very sour and bitter taste which is acidic and caustic, and a pale orange tinge to them."
                )
        response = stub.get_parse_model(ui_request)

    print("Greeter client received: ")
    print(response)


if __name__ == '__main__':
    logging.basicConfig()
    run()


   