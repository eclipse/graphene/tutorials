from concurrent import futures
import grpc
import openai
import GLLM_parser_pb2
import GLLM_parser_pb2_grpc
import os
from app import app_run
import logging
import threading


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class Parser(GLLM_parser_pb2_grpc.ParserServicer):

    def __init__(self) -> None:
        super().__init__()
        

    def get_parse_model(self,request,context):
        
        ''' User passing usecase specific document and generating entities for KG '''

        logger.info('start of llm')
     
        openai.organization  = request.organization_id
        openai.api_key  = request.api_key
        usecase_info = request.usecase_data

        usecase_info = usecase_info.lower()

        try:
            llm = openai.ChatCompletion.create(
            model="gpt-4",
            messages=[{
                "role": "system",
                "content": "You will be provided with unstructured data, and your task is to parse it as subject, relation, object and parse it into CSV format.PLease apply this rule: if terms categorized as 'relation' contains multiple words for example 'grow on','is similar to','usually has',etc , then join it using '_' for example 'grow_on','is_similar_to','usually_has'. "},
                {"role": "user", "content": usecase_info}],
            temperature=0,
            max_tokens=256,
            )

            #Extract and print the assistant's reply
            structured_data = llm.choices[0].message.content
            KGmoduleStatus = 'Ready to Construct Knowledge graph'

            logger.info('passed data into llm and waiting ...')
            logger.info(structured_data)
            logger.info(KGmoduleStatus)   
        
        except:
            
            structured_data ='Not available'
            KGmoduleStatus = 'Not ready to construct Knowledge graph'

        response = GLLM_parser_pb2.ResultText(parsed_output=str(structured_data), KGmoduleStatus=str(KGmoduleStatus))

        with open("results.txt", mode="a+") as f:
            # for e0, e1, e2, e3, e4, e5 in result:
            f.write("|" + str(response.parsed_output) + "|" + str(response.KGmoduleStatus) + "\n")
            f.close()

        logger.info(f'response: {response}')

        return response


def serve(port):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    GLLM_parser_pb2_grpc.add_ParserServicer_to_server(Parser(), server)
    server.add_insecure_port("[::]:{}".format(port))
    print("Starting server. Listening on port : " + str(port))
    server.start()
    threading.Thread(target=app_run()).start()
    server.wait_for_termination()

if __name__ == "__main__":
    logging.basicConfig()
    open('results.txt', 'w').close()
    port = 8061
    serve(port)
   

