from concurrent import futures
import grpc
import openai
import GLLMmodel_pb2
import GLLMmodel_pb2_grpc
import os
from app import app_run
import logging
import threading


from langchain_community.chat_models import ChatOpenAI
from langchain.chains import GraphCypherQAChain
from langchain_community.graphs import Neo4jGraph
from langchain.prompts.prompt import PromptTemplate

from neo4j import GraphDatabase
import pandas as pd
import time
import sys
from openai import OpenAI

from datasets import Dataset 
from ragas.metrics import faithfulness, answer_relevancy
from ragas import evaluate

import warnings
warnings.filterwarnings("ignore")

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class Grounding(GLLMmodel_pb2_grpc.GroundingServicer):

    def __init__(self) -> None:
        super().__init__()
        self.user_data = " "

    def create_nodes_and_relationships(self,tx, source, target, edge):
        constant_value = 'related_to'
        query = (
            "MERGE (s:Node {name: $source}) "
            "SET s += {`" + edge + "`: $target} "
            "MERGE (t:Node {name: $target}) "
            "SET t += {`" + constant_value + "`: $source} "
            "MERGE (s)-[r:`" + edge + "`]->(t)"
        )
    
        tx.run(query, source=source, target=target, edge=edge)


    def wait_for_neo4j(self,driver):
        while True:
            try:
                with driver.session() as session:
                    session.run("RETURN 1")
                break
            except Exception as e:
                print(f"Waiting for Neo4j to start: {str(e)}")
                logger.info(f"Waiting for Neo4j to start: {str(e)}")
                time.sleep(2)
            
        

    def get_keys_data(self,request,context):
        
        ''' User passing user data and respetive keys'''

        logger.info('Get keys')
     
        os.environ['OPENAI_ORGANIZATION']  = request.organization_id
        os.environ['OPENAI_API_KEY']  = request.api_key

        openai.organization  = request.organization_id
        openai.api_key  = request.api_key

        self.user_data = request.user_query
        self.user_data= self.user_data.lower()
           
        out ='True'
        response = GLLMmodel_pb2.keysOutput(note=str(out))
        logger.info(response)
     
        return response


    def get_grounding_model(self,request,context):

        ''' Grounding LLM by building KG on neo4j using use case specific data and access the graph for relevant information using Langchain. 
        Finally passing relevant info + user's query into a LLM'''

        entity_pairs = []
        parse_model_output= request.parsed_output
        formatted_list = parse_model_output.split("\n")

        # create dataframe
        for i in formatted_list:
            split_list = i.split(',')
            source = split_list[0]
            edge = split_list[1]
            target = split_list[2]  

            entity_pairs.append({'source':source, 'target':target, 'edge':edge})

        KG_ent_df= pd.DataFrame(entity_pairs)

        # Connect to the Neo4j database
        uri = "bolt://localhost:7687"
        username = None
        password = None

        driver = GraphDatabase.driver(uri, auth=(username, password))

        # Wait for Neo4j to become available
        self.wait_for_neo4j(driver)

        logger.info('going to create queries')

       # Creatin Knowledge graph
        with driver.session() as session:
            for _, row in KG_ent_df.iterrows():
                session.write_transaction(self.create_nodes_and_relationships, row['source'], row['target'], row['edge'])    
    
        driver.close()

        logger.info('KG created')
        logger.info('Enter Langchain Component')

        # Entering Langchain
        graph = Neo4jGraph(
        url="bolt://localhost:7687", 
        username= "neo4j", 
        password= "password")

        CYPHER_GENERATION_TEMPLATE =  """
        Task:Generate Cypher statement to query a graph database.
        Instructions:
        Use only the provided relationship types and properties in the schema.
        Do not use any other relationship types or properties that are not provided.
        Schema:
        {schema}
        Note: Do not include any explanations or apologies in your responses.
        Do not respond to any questions that might ask anything else than for you to construct a Cypher statement.
        Do not include any text except the generated Cypher statement.
        Examples: Here are a few examples of generated Cypher statements for particular questions:
        # How many people played in Top Gun?
        MATCH (m:Movie {{name:'Top Gun'}})<-[:ACTED_IN]-() 
        RETURN count(*) AS numberOfActors

        Use below queries for types of generic questions :
        # How is the movie Top Gun ? 
        # Can you describe about Top Gun?
        # Have you heard about Top Gun? Can you describe it ?
        # what is Top Gun ?
        MATCH (m:Movie) WHERE m.name = 'Top Gun'
        MATCH (m)-[r]-(connectedNode) RETURN m, TYPE(r) AS relationshipType, connectedNode

        The question is:
        {question}"""

        CYPHER_GENERATION_PROMPT = PromptTemplate(
            input_variables=["schema", "question"], template=CYPHER_GENERATION_TEMPLATE
        )

       
        cypher_chain = GraphCypherQAChain.from_llm(
        cypher_llm = ChatOpenAI(temperature=0, model_name='gpt-4-turbo-preview'),
        qa_llm = ChatOpenAI(temperature=0, model_name='gpt-4'), graph=graph, verbose=True,
        cypher_prompt=CYPHER_GENERATION_PROMPT)

        print(f'user question:{self.user_data}')
        logger.info('Extracting relevant information from KG')
        relevant_info= cypher_chain.run(self.user_data)


        # Combine the articles and user's question into a single user message
        user_message = " ''' " + relevant_info + " ''' " + "\n\nQuestion: " + self.user_data
        logger.info('Now Grounding LLMs response by user query and relevant information from KG')
        print(user_message)


        # Finally, sending it to an LLM prompt
        client = OpenAI(organization=os.environ['OPENAI_ORGANIZATION'], api_key= os.environ['OPENAI_API_KEY'])
        LLM_G_response = client.chat.completions.create(
        model="gpt-4",
        messages=[
            {"role": "system", "content": "Use the provided articles delimited by triple quotes as an additional help to the question and provide relevant answers in detail."},
            {"role": "user", "content": user_message}
        ],
        temperature=0,
        max_tokens=256
        )
       
        grounded_output = LLM_G_response.choices[0].message.content.strip()
        response = GLLMmodel_pb2.GroundedOutput(grounded_output=str(grounded_output))

        with open("results.txt", mode="a+") as f:
            # for e0, e1, e2, e3, e4, e5 in result:
            f.write("|" + str(user_message) + "|" + str(response.grounded_output) + "\n")
            f.close()

        faithfulness, relevancy = self.calculate_metrics(relevant_info, grounded_output) 
        logger.info(f'faithfulness, relevancy: {faithfulness}, {relevancy}')
          
        with open("metrics.txt", mode="a+") as f:
            # for e0, e1, e2, e3, e4, e5 in result:
            f.write("|" + str(round(faithfulness,3)) + "|" + str(round(relevancy,3)) + "\n")
            f.close()


        logger.info(f'response: {response}')

        return response  

    def calculate_metrics(self, relevant_info, grounded_output):

        data_samples = {
            'question': [str(self.user_data)],
            'answer': [str(grounded_output)],
            'contexts' : [[str(relevant_info)]]
        }

        dataset = Dataset.from_dict(data_samples)
        faithfulness_score = evaluate(dataset,metrics=[faithfulness])
        answer_relevancy_score = evaluate(dataset,metrics=[answer_relevancy])
        
        return faithfulness_score["faithfulness"], answer_relevancy_score["answer_relevancy"]


def serve(port):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    GLLMmodel_pb2_grpc.add_GroundingServicer_to_server(Grounding(), server)
    server.add_insecure_port("[::]:{}".format(port))
    print("Starting server. Listening on port : " + str(port))
    server.start()
    threading.Thread(target=app_run()).start()
    server.wait_for_termination()

if __name__ == "__main__":
    logging.basicConfig()
    open('results.txt', 'w').close()
    open('metrics.txt', 'w').close()
    port = 8061
    serve(port)
   

