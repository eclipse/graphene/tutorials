from flask import Flask, render_template, request, flash, jsonify
import grpc
import GLLMmodel_pb2
import GLLMmodel_pb2_grpc
import os
import logging
from datetime import datetime

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
app = Flask(__name__)

readme_ratings = {}
local_directory = os.getenv("SHARED_FOLDER_PATH")
faithfulness = None
relevancy= None

@app.route("/", methods=['GET', 'POST'])
def index():
    global faithfulness
    global relevancy
    results = []
    metrics = []
    with open("results.txt", mode="r") as f:
        content = ""
        for line in f.readlines():
            content += line.strip('')  # Concatenate lines with hyphen
            

        # Split the content into two items based on '|'
        results = content.split('|')
        results = [item.strip('') for item in results]  # Remove trailing hyphens
        
    try:
        LLM_prompt=results[-2]
        grounded_output=results[-1]
    except:
        LLM_prompt=results[:-1]
        grounded_output=results[-1]


    with open("metrics.txt", mode="r") as f:        
        content = ""
        for line in f.readlines():
            content += line.strip('')  # Concatenate lines with hyphen
            
            

        # Split the content into two items based on '|'
        metrics_list = content.split('|')
        metrics_list = [item.strip('') for item in metrics_list]  # Remove trailing hyphen

        
    try:
        faithfulness=metrics_list[-2]
        relevancy=metrics_list[-1].rstrip('\n')
    
    except:
        faithfulness=metrics_list[:-1]
        relevancy=metrics_list[-1].rstrip('\n')
    
    logger.info(f'faithfulness, relevancy {faithfulness}, {relevancy}')


    return render_template('index.html', LLM_prompt=LLM_prompt, grounded_output=grounded_output, faithfulness= faithfulness , relevancy = relevancy)


@app.route('/rate_readme', methods=['POST'])
def rate_readme():
    global faithfulness
    global relevancy
    
    try:
        data = request.json
        rating = data['rating']
        feedback = data.get('feedback', '')  # Get the feedback string, default to empty string if not provided
        readme_ratings.setdefault(local_directory, []).append({'rating': rating, 'feedback': feedback, 'faithfulness' : faithfulness, 'answer_Relevancy' : relevancy})
        logger.info(readme_ratings)
        timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        try:
            ratings_filename = os.path.join(local_directory, "ratings.txt")
            with open(ratings_filename, 'a+') as file:
                logger.info(f"Rating: {rating}, Feedback: {feedback}, Faithfulness: {faithfulness}, Answer_Relevancy: {relevancy}, Timestamp: {timestamp}\n")
                file.write(f"Rating: {rating}, Feedback: {feedback}, Faithfulness: {faithfulness}, Answer_Relevancy: {relevancy}, Timestamp: {timestamp}\n")
        except Exception as e:
            logger.info("Error:", e)
        return jsonify({'success': True})
    except Exception as e:
        logger.info("Exception:", e)
        return jsonify({'success': False, 'error': str(e)})




def app_run():
    app.secret_key = 'glmm' 
    app.run(host="0.0.0.0", port=8062, debug = True)


