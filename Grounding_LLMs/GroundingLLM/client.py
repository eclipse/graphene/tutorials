import grpc
from timeit import default_timer as timer
import logging

# import the generated classes

import GLLMmodel_pb2_grpc
import GLLMmodel_pb2

port = 8061




def run():
    print("Calling GLLM_Stub..")
    with grpc.insecure_channel('localhost:{}'.format(port)) as channel:
        stub = GLLMmodel_pb2_grpc.GroundingStub(channel)
        
        keys_request = GLLMmodel_pb2.UserInputs(
                    organization_id=" ", # enter org id
                    api_key=" ", # enter openai-key
                    user_query="Can you tell me about Single annular combustor ? ",
                    usecase_data="DLE stands for Dry Low Emissions and its the aero-derivative version of DLN (Dry Low NOx). It includes additional fuel control valves and I think the combustors are multi-zone at least they were in the early days of development. SAC stands for Single Annular Combustor and it's usually a simple single annular combustor with one combustion zone. To achieve low NOx emissions it's usually necessary to use water or steam injection which adds somewhat to the complexity of the auxiliaries. Gas turbines with DLE technology were developed to achieve lower emissions without using water or steam to reduce combustion temperature Wet Low Emission technology. A DLE combustor uses the principle of lean premixed combustion and is similar to the Single Annular Combustor or SAC with some exceptions. A DLE combustor takes up more space than a SAC turbine and if the turbine is changed it can not be connected directly to existing equipment without considerable changes in the positioning of the equipment.The SAC turbine has one single concentric ring where the DLE turbine has two or three rings with premixers depending on gas turbine type."
                    #usecase_data='There are many fruits that were found on the recently discovered planet Goocrux. There are neoskizzles that grow there, which are purple and taste like candy. There are also loheckles, which are a grayish blue fruit and are very tart, a little bit like a lemon. Pounits are a bright green color and are more savory than sweet.'
                )
        keys_response = stub.get_keys_data(keys_request)

        grounding_request = GLLMmodel_pb2.ResultText(
                    #parsed_output = "goocrux,has_fruit,neoskizzles\nneoskizzles,has_color,purple\nneoskizzles,has_taste,candy\ngoocrux,has_fruit,loheckles\nloheckles,has_color,grayish blue\nloheckles,has_taste,tart\ngoocrux,has_fruit,pounits\npounits,has_color,bright green\npounits,has_taste,savory\ngoocrux,has_fruit,loopnovas\nloopnovas,has_color,neon pink\nloopnovas,has_taste,cotton candy\ngoocrux,has_fruit,glowls\nglowls,has_color,pale orange\nglowls,has_taste,sour and bitter",
                    parsed_output = "subject,relation,object\ndle,stands_for,dry low emissions\ndle,is,the aero-derivative version of dln\ndle,includes,additional fuel control valves\ncombustors,are,multi-zone\nsac,stands_for,single annular combustor\nsac,is,usually a simple single annular combustor with one combustion zone\nlow nox emissions,achieve_by,use water or steam injection\ngas turbines with dle technology,were_developed_to,achieve lower emissions\ndle combustor,uses,the principle of lean premixed combustion\ndle combustor,is_similar_to,single annular combustor or sac\ndle combustor,takes_up,more space than a sac turbine\nturbine,can_not_be,connected directly to existing equipment without considerable changes in the positioning of the equipment\nsac turbine,has,one single concentric ring\ndle turbine,has,two or three rings with premixers",
                    KGmoduleStatus = "Ready to construct KG"
                )
        response_grounded_output = stub.get_grounding_model(grounding_request)



    print("Greeter client received: ")
    print(keys_response)
    print(response_grounded_output)

if __name__ == '__main__':
    logging.basicConfig()
    run()


   