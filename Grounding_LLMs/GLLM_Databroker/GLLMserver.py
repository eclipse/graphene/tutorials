from concurrent import futures
import grpc
from app import app_run, get_parameters
import sys
import threading
import GLLM_databroker_pb2
import GLLM_databroker_pb2_grpc
import openai
from openai import OpenAI
import os
import logging


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class DatabrokerServicer(GLLM_databroker_pb2_grpc.DatabrokerServicer):
    
    def __init__(self):
        super().__init__()
        self.send_data=True
        
    def GLMMdatabroker(self, request, context):
       
        """
        Collect the parameters.
        """
        parameters = get_parameters()
        print('parameters from server side:', parameters)
        logger.info(f'parameters from server side:: {parameters}')
        response = GLLM_databroker_pb2.UserInputs(organization_id=str(parameters[0]), api_key=str(parameters[1]),user_query=str(parameters[2]),usecase_data=str(parameters[3]))

        print('Response')
        print(response)
        logger.info(f'response: {response}')

        logger.debug(response)
        if not self.send_data:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details("all data has been processed")

        self.send_data= not self.send_data


        return response
        

def serve(port):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    GLLM_databroker_pb2_grpc.add_DatabrokerServicer_to_server(DatabrokerServicer(), server)
    server.add_insecure_port("[::]:{}".format(port))
    print("Starting server. Listening on port : " + str(port))    
    server.start() 
    threading.Thread(target=app_run()).start()
    server.wait_for_termination()
    



if __name__ == "__main__":

    logging.basicConfig()
    port = 8061
    serve(port)