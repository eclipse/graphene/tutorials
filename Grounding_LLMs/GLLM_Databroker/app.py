from flask import Flask, render_template, redirect, url_for
from flask_bootstrap import Bootstrap


from flask_wtf import FlaskForm
from wtforms.fields import StringField, SubmitField, PasswordField
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
app = Flask(__name__)

parameters = []
port = 8062



class GllmInputForm(FlaskForm):
    organization_id= PasswordField('Organisation ID')
    api_key = PasswordField('Your API-Key')
    user_query = StringField('Enter your question for the GPT model')
    usecase_data = StringField('Submit the usecase specific data')

    submit_button = SubmitField('Submit')


#GLLM_databroker_Page
@app.route('/', methods=['GET', 'POST'])
def hello():
    form = GllmInputForm()
    
 
    if form.validate_on_submit():
        parameters.clear()
        parameters.append(form.organization_id.data)
        parameters.append(form.api_key.data)
        parameters.append(form.user_query.data)
        parameters.append(form.usecase_data.data)
        logger.info('Parameters')
        print(parameters)
        logger.info(f'parameters from app.py: {parameters}')
        
        return render_template("display_prediction.html")

    return render_template("index.html", example_form=form)
        
    

def get_parameters():
    logger.debug("Return databroker parameters")
    return parameters


def app_run():
    app.secret_key = 'gllm'
    bootstrap = Bootstrap(app)
    app.run(host="0.0.0.0", port=8062)
