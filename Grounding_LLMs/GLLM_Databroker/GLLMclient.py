import grpc
from timeit import default_timer as timer
import logging

# import the generated classes

import GLLM_databroker_pb2_grpc
import GLLM_databroker_pb2

port = 8061


def run():
    print("Calling GLLM_Stub..")
  
    with grpc.insecure_channel('localhost:{}'.format(port)) as channel:
        stub = GLLM_databroker_pb2_grpc.DatabrokerStub(channel)
        ui_request = GLLM_databroker_pb2.Empty()
        response = stub.GLMMdatabroker(ui_request)
    
    print("Greeter client received: ")
    print(response)


if __name__ == '__main__':
    logging.basicConfig()
    run()
